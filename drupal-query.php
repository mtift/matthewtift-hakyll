<?php

// Use an HTML to Markdown converter. Requires this command:
// `composer require league/html-to-markdown`
use League\HTMLToMarkdown\HtmlConverter;
$converter = new HtmlConverter();

// Get the node IDs.
$query = \Drupal::entityQuery('node');
$query->condition('type', 'article');
$entity_ids = $query->execute();

// Load the nodes.
$nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
$articles = [];

foreach ($nodes as $node) {

  // Get the URL (alias).
  $nid = $node->id();
  $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$nid);
  // String the first slash.
  $alias = str_replace('/', '', $alias);

  // Get the taxonomy terms for the article.
  $terms = $node->field_tags->referencedEntities();
  $term_names = [];
  foreach ($terms as $term) {
    if (!empty($term)) {
      $term_name = $term->getName();
      if ($term_name == 'drupal') {
        $term_name = 'Drupal';
      }
      elseif ($term_name == 'backdrop') {
        $term_name = 'Backdrop';
      }
      elseif ($term_name == 'drupal association') {
        $term_name = 'Drupal Association';
      }
      elseif ($term_name == 'beethoven') {
        $term_name = 'Beethoven';
      }
      elseif ($term_name == 'prx') {
        $term_name = 'PRX';
      }
      elseif ($term_name == 'red hat') {
        $term_name = 'Red Hat';
      }
      elseif ($term_name == 'wordpress') {
        $term_name = 'Wordpress';
      }
      elseif ($term_name == 'wpr') {
        $term_name = 'WPR';
      }
      $term_names[] = $term_name;
    }
  }
  
  // Get the image file, if it exists.
  $image = '';
  $image_file = $node->field_image->referencedEntities();
  if (is_object($image_file[0])) {
    $image = $image_file[0]->getFilename();
  }

  $body = $node->get('body')->value;
  $body_format = $node->get('body')->format;
  // Convert html content, but not markdown.
  if ($body_format == 'full_html') {
    $body_md = $converter->convert($body);
  }
  // Remove line endings.
  elseif ($body_format == 'markdown') {
    $body_md = str_replace("\r", "", $body);
  }

  $published_date = $node->get('created')->value;

  $file_name = date("Y-m-d", $published_date) . '-' . $alias . '.md';
  $output_location = 'public://posts/' . $file_name;
  $data = "---\n";
  $data .= 'title: "' . $node->getTitle() . '"' . "\n";
  $data .= "date: " . date("F j, Y, g:i a", $published_date) . "\n";
  $data .= "author: Matthew Tift\n";
  if (!empty($image)) {
    $data .= "image: " . $image . "\n";
  }
  if (!empty($term_names)) {
    $data .= "tags: " . implode(', ', $term_names) . "\n";
  }
  $data .= "path: " . $alias . "\n";
  $data .= "---\n\n";
  $data .= $body_md;

  // Save the data.
  $file = file_save_data($data, $output_location);

}
