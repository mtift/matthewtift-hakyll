---
title: AMPing up Drupal
author: Matthew Tift, Marc Drummond, Karen Stevenson
s5-url: ../../s5/ui/default
revealjs-url: ../../reveal.js
---

## We work at Lullabot
![](images/lullabot.png)

<!--
pandoc -t revealjs --css tift.css --standalone -V theme=night -V transition='none' -V controls='false' -V progress='false' -so index.html drupalcon-amp.md
-->

# Accelerated Mobile Pages
Instant. Everywhere.

![](images/amp-logo.jpg)

# Mobile ONLY

![Image: [Gilles Lambert](https://unsplash.com/photos/pb_lF8VWaPU)](images/mobile.jpg)

# Mobile web
Slow, clunky, frustrating

![Image: [Gratisography.com](http://www.gratisography.com/)](images/frustrating.jpg)

# Revenue over UX
Business drives decisions

![Image: [Circe Denyer](http://www.publicdomainpictures.net/view-image.php?image=108032)](images/national-cash-register.jpg)

# Bouncy loading

# Visitors abandon sites that do not load in 3 seconds

# Goal

Pages load instantly everywhere

![Image: [Unsplash](https://unsplash.com/photos/iR8m2RRo-z4)](images/lights.jpg)

# Walled Gardens
- Facebook Instant Articles
- Apple News
- Native Apps

# October 7, 2015
Google unveiled

![](images/amp.png)

# February 24, 2016
AMP content [Google search results](https://amphtml.wordpress.com/2016/02/24/amping-up-in-google-search/)

# April 20, 2016
AMP content in [Google News](http://googlenewsblog.blogspot.com/2016/04/amplifying-news.html)

# Google News
Up to 14 headlines in carousel

----

![](images/amp-results.gif)

# Open Source Project

![](images/github.jpg)

# Apache 2 license

![](images/apache.png)

# 130+ contributors

# 100s of Partners
![](images/twitter.jpg)
![](images/vine.jpg)
![](images/bbc.jpg)
![](images/medium.jpg)
![](images/linkedin.jpg)
![](images/nyt.jpg)
![](images/pinterest.jpg)
![](images/vimeo.jpg)
![](images/wordpress.jpg)
![](images/usa-today.jpg)
![](images/youtube.jpg)
![](images/cnn.png)
![](images/wsj.jpg)
![](images/time.png)

# 3 parts of AMP
1. AMP HTML
1. AMP JS (Runtime)
1. Google AMP Cache

# AMP HTML
[Loads in any browser](https://support.google.com/analytics/answer/6343176)

# AMP HTML

## NODE
`<link rel="amphtml" href="/node/1?amp" />`

## AMP NODE
`<link rel="canonical" href="/node/1" />`

# AMP Tags
Most tags in an AMP HTML page are regular HTML tags

# AMP Tags
Some HTML tags are replaced with AMP-specific tags

# AMP Tags
- `<img>` replaced with `<amp-img>`
- `<video>` replaced with `<amp-video>`
- `<audio>` replaced with `<amp-audio>`
- `<iframe>` replaced with `<amp-iframe>`

# AMP CSS
Declared in an inline stylesheet

# AMP CSS
Bound to a maximum size of 50 kb

# AMP JS
AMP HTML documents no not include any author-written JavaScript, nor any third-party scripts

----

## What? No JavaScript!?!

![Image: [George Hodan](http://www.publicdomainpictures.net/view-image.php?image=19999)](images/sad-man-and-rain.jpg)

# AMP Cache
Free CDN

# AMP Cache
Very fast

# AMP Cache
Built-in validation

# Fast
AMP content loads an average of

## four times faster

# 10 times less data
than non-AMP pages

# Benefit
Positive effect on SEO

# Reaction
["Most publishers are moving forward with Google as their main focus right now."](https://www.searchenginejournal.com/how-will-googles-amp-pages-affect-seo/159741/) -- Search Engine Journal

# Reaction
["It's only a matter of time before AMP formatting becomes synonymous with publishing content for mobile devices."](https://premium.wpmudev.org/blog/facebook-instant-articles-vs-amp-vs-apple-news-for-wordpress/) -- WPMU DEV

# Want that?
`<html ⚡>`

----

Talk given at DrupalCon New Orleans, May 11, 2016. Presentation and slides are Copyright © 2016 Matthew Tift, and are licensed under the [Creative Commons Attribution-Share Alike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

