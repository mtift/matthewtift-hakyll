<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - Drupal as a Political Act</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>Drupal as a Political Act</h1>
    <div class="post">

  <div class="info">
    Posted on December  9, 2016
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/Drupal/">Drupal</a>, <a href="../tags/community/">community</a>, <a href="../tags/activism/">activism</a>, <a href="../tags/solidarity/">solidarity</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p><em>(This article was cross-posted on <a href="https://www.lullabot.com/articles/drupal-as-a-political-act">lullabot.com</a>.)</em></p>
<p>Like so many of you, both here in the United States and elsewhere, I am deeply troubled by the <a href="https://www.splcenter.org/20161129/ten-days-after-harassment-and-intimidation-aftermath-election">incidents of hateful harassment</a> and the threats to democracy that have spiked since November 8. My thoughts have been routinely consumed with the task of analyzing my work and motivations, trying to detect any positive impact that my contributions have on the world. Because of my involvement in the Drupal community, my thoughts are as much about Drupal as they are about me. I am a proud member of our community and I cannot help but reflect on how the organization of our community brings people together, discourages hate, and promotes democracy.</p>
<!--more-->
<p>What it would mean to “make the world better” is up for debate. We cannot be experts in all subjects, and a group of Drupal developers might not fully understand, for example, the policies that allow <a href="https://www.theguardian.com/world/2016/apr/14/us-corporations-14-trillion-hidden-tax-havens-oxfam">tax havens</a>, the economic implications of a <a href="http://fightfor15.org">$15 minimum wage</a>, how to combat <a href="https://en.wikipedia.org/wiki/Predatory_lending">predatory lending</a>, or the solutions to <a href="http://www.economist.com/blogs/graphicdetail/2016/11/global-warming?fsrc=scn/tw/te/bl/ed/globalwarmingthestateoftheclimatein2016">climate change</a>. Perhaps we have strong opinions on these topics, but many of us would begrudgingly admit that we know more about <a href="https://en.wikipedia.org/wiki/Dependency_injection">dependency injection</a>, <a href="https://www.drupal.org/patch/reroll">re-rolling patches</a>, or even the <a href="https://www.drupal.org/docs/7/creating-custom-modules/understanding-the-hook-system-for-drupal-modules">hook system</a>. That is, we know how to build Drupal websites. More importantly, to succeed in the Drupal community <a href="https://www.drupal.org/dcoc">we are required</a> to be considerate, respectful, and collaborative. We, as a community, vigorously reject bigotry, racism, sexism, homophobia, and xenophobia. This, in my view, makes the world better.</p>
<p>What is more, I would argue that Drupal blurs traditional boundaries. While certainly there are market forces that determine how Drupal is constructed, powerful <a href="https://en.wikipedia.org/wiki/GNU_General_Public_License">legal</a> and <a href="https://www.drupal.org/dcoc">cultural</a> <a href="https://en.m.wikipedia.org/wiki/Nonmarket_forces">nonmarket forces</a> push back. Some Drupal agencies exist to turn a profit, but do so working primarily with <a href="https://www.drupal.org/civicactions">public sector</a> or <a href="https://www.jacksonriver.com">non-profit</a> organizations. Drupal agencies can be seen as capitalist in the sense that they accumulate surplus value by “<a href="https://en.wikipedia.org/wiki/Exploitation_of_labour">exploiting the working class</a>,” but socialist in the sense that they produce goods that are owned collectively. Some have stated goals to <a href="http://www.md-systems.ch/en/blog/md-systems/2016/09/28/we-are-striving-invest-20-every-projects-value-back-community">invest value back into the community</a> and others are “<a href="https://www.drupal.org/thinkshout">benefit corporations</a>,” required to make the world a better place. While I am tempted to place new labels on the Drupal community, such as “post-capitalist,” I find such terms to be of limited use, and I am far more interested in finding common ground that unites our community.</p>
<p>Drupal code has only limited value without the community, and our community stands for values that transcend our code. I participate in the Drupal community because I believe it represents ideals that are consistent with my own. One of the beliefs that we hold in high regard is “doing good.” It would be difficult to convince me that people, such as <a href="https://www.palantir.net/people">George DeMet and Tiffany Farriss</a>, <a href="https://www.fourkitchens.com/team/todd-ross-nienkerk">Todd Ross Nienkerk</a>, or <a href="https://thinkshout.com/team/lev/">Lev Tsypin</a>, have anything but the best intentions in the way they run their businesses. More importantly, these individuals, like so many others in our community, actually do make the world a better place through their work, compassion, and advocacy.</p>
<p>In some respects, the well-intentioned subset of our community exemplifies what Luc Boltanski and Eve Chiapello describe as “the new spirit of capitalism.” In <a href="https://www.versobooks.com/books/259-the-new-spirit-of-capitalism">their study</a> of management textbooks, they find this “new spirit” is characterized by, among other things, a “high moral tone” (58), a belief that workers should “control themselves” (80), structures where managers are essentially replaced with customers (82), and where bureaucracy represents a kind of “totalitarianism and arbitrariness” that should be avoided (85). While Boltanski and Chiapello find many faults with this “new spirit,” generally, I would suggest that is has become more important than ever to acknowledge the many benefits that the people and organizations in our community have for the world. While critique and criticism will surely be needed, we should also continue to celebrate the impact that our software and colleagues plays in efforts towards ending poverty, empowering independent journalists, defending the free and open Internet, and educating people. Even though Drupal has been used for nefarious purposes, and there are many reasons to <a href="https://www.lullabot.com/articles/why-paid-drupal-modules-fail-drupal-as-art">critique</a> the <a href="https://www.lullabot.com/articles/better-then-bigger-cultivating-the-drupal-community">Drupal</a> <a href="https://www.lullabot.com/articles/the-cultural-construction-of-drupal">community</a>, I feel emboldened knowing that when people came together to build websites for <a href="https://www.drupalwatchdog.com/volume-3/issue-1/short-history-drupal-distributions">DeanSpace</a>, the <a href="http://www.un.org/en/index.html">United Nations</a>, <a href="http://www.drupalshowcase.com/drupal-showcase/amnesty-international">Amnesty International</a>, <a href="https://www.drupal.com/case-study/greenpeace-greenwire">Greenpeace</a>, <a href="http://oxfam.org/">Oxfam</a>, the <a href="https://www.aclu.org">ACLU</a>, the <a href="https://www.eff.org/">Electronic Frontier Foundation</a>, <a href="http://digitalservices.npr.org/topic/core-publisher">National Public Radio</a>, <a href="https://www.freepress.net">Free Press</a>, and the <a href="https://www.whitehouse.gov/">White House</a>, they choose Drupal.</p>
<p>More than just software, part of the reason we “stay for the community” is because we place such a high premium on human interaction. Drupal contributors create <em>public goods</em> (free software) that can be used by one person without reducing the availability to others. If the public relations departments of mega-corporations extol the value of business and markets, while criticizing government and fair labor, the Drupal community takes an alternative approach that values solidarity. In this sense, our democratic practices threaten unjust power. Throughout history people in power have pushed back against the democratizing effects of solidarity to defend their positions of power. In his 1776 magnum opus on political economy, <em>Wealth of Nations</em>, Adam Smith <a href="http://www.econlib.org/library/Smith/smWN11.html">famously observed</a>, “All for ourselves and nothing for other people, seems, in every age of the world, to have been the vile maxim of the masters of mankind.” With every Drupal Camp, DrupalCon, code sprint, community summit, and user group meeting we gather together in solidarity. Let us not forget all we do to encourage hope and camaraderie.</p>
<p>If you are discouraged by a world that turns workers against each other and treats citizens as consumers, pushing them to the malls rather than the public library, remember that we as a Drupal community are pushing back against the “masters of mankind.” In the 1970s, <a href="https://en.wikipedia.org/wiki/Buckley_v._Valeo">Buckley v. Valeo</a> may have determined that money is a form of speech, but because we work together, Drupal becomes another kind of speech. Most of us (the working class) must sell our labor in return for a wage or salary. So what I am arguing is not for our community to become noncommercial or anti-commercial, but instead that we consider expanding our <a href="https://en.wikipedia.org/wiki/Horizons_of_Expectation">horizon of expectations</a> to allow for a conception of Drupal as a political act. I want us to celebrate our community and stand up against hate, inequality, corruption, and depoliticization. If that idea makes you uncomfortable, then perhaps consider the words of the historian Howard Zinn and <a href="http://www.huffingtonpost.com/michael-berkowitz/requiem-for-the-american-_b_9203578.html">his suggestion</a> that what matters are “the countless deeds of unknown people who lay the basis for the events of human history.” I hope that we can find common ground, build on what we have accomplished, and organize against the forces that seek to divide us against ourselves.</p>
  
  <div class="prev-next">
  
  <a href=".././who-sponsors-drupal-development/" rel="previous">← Previous Post</a>
  
  
  <a href=".././install-tidal-debian-stretch/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
