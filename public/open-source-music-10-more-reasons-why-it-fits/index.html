<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - Open-Source Music: 10 More Reasons Why It Fits</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>Open-Source Music: 10 More Reasons Why It Fits</h1>
    <div class="post">

  <div class="info">
    Posted on June  5, 2012
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/open-source-music/">open source music</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p>A few days ago I offered my version of an <a href="public-music-public-media-introduction">introduction to open-source music</a> and suggested a few reasons why I think public media organizations should be proponents of open-source recordings and scores. Here are 10 more reasons why this would be a good idea for public media:</p>
<ol type="1">
<li><strong>It’s a digital world.</strong> Free and open access to digital music can greatly help expand music audiences. Especially for some <a href="http://en.wikipedia.org/wiki/Digital_native">digital natives</a>, classical music scores might as well not exist at all if they are not online and searchable. For these folks, a <a href="http://amzn.com/B0000025PM">great CD</a> and <a href="http://www.sheetmusicplus.com/title/Goldberg-Variations/3186397">printed sheet music</a> is a world apart from an instantaneous <a href="http://musescore.com/node/16091">great recording + moving score + video</a> all mixed together.</li>
<li><strong>Think of the children.</strong> The score-following technology that is used on sites like <a href="http://musescore.com/">MuseScore.com</a> allows kids (or anyone, really) to learn about music by watching the notes they hear, much like underlined words in a children’s program on public television can show children how words look. And although watching the notes go by on the screen can be a learning tool, it is especially awesome when <a href="http://musescore.com/node/580">video is added to the mix</a>.</li>
<li><strong>Music lovers become marketers.</strong> Additional free and open-source scores would help public media’s biggest classical music lovers more easily share the music they love.</li>
<li><strong><a href="http://blog.mediaengage.org/?p=3848">Engagement</a> is all the rage.</strong> Having listeners request their favorite music is a common practice. With a public media station that supports open-source scores, audiences might get a chance to vote for the piece they want their station to sponsor. Other listeners might find joy in helping to edit the next piece sponsored by their station. Public-media sponsored recordings could provide talented – especially local – performers more opportunities to record music and those same performers could help in creating the scores they would be playing.</li>
<li><strong>Capital campaigns for music</strong>. Donors often like to be involved in capital campaigns because the product of those campaigns is frequently long lasting, such as a new or renovated building. Music scores that can be used by all would have that same long-lasting quality. Open-source software is free, but time is money. Along those same lines, performers creating open-source music deserve to be compensated and it doesn’t take much to envision donors wanting to contribute to a recording by their favorite local musician, hand picked by their favorite classical music station.</li>
<li><strong>Expand audiences.</strong> Ever heard someone mention aging classical music (or public radio) audiences? Hip new forms of interaction might encourage younger audiences to get involved with their local station. There is likely a subset of our audiences that would not only enjoy watching the scores online while listening to a broadcast, but also to see the music host make notes on the score in real time, much like is <a href="http://www.youtube.com/watch?v=UNSYYQFMsFY">demonstrated here</a>.</li>
<li><strong>Appeal to shorter attention spans</strong>. By creating a new kind of engagement, someone who might not want to sit through a concert, or even listen to an entire symphony, might be interested in helping to create new scores. Free software and a welcoming community could invite these people to participate online or even at station-sponsored, score-creating events.</li>
<li><strong>New or better partnerships.</strong> By contributing to online catalogs of open-source recordings and scores, public media would create new reasons for establishing partnerships with music teachers, researchers, music camps, and other organizations. I think the appropriate buzzword here is “<a href="http://en.wikipedia.org/wiki/Synergy">synergy</a>.”</li>
<li><strong>Not for everyone.</strong> Broadcasting open-source recordings and linking to open-source scores might not change how many people experience their public media, and it does not have to interfere with those who would rather focus their attention on listening. These activities are meant to augment the broadcast and are relevant for only a select subset of our audiences.</li>
<li><strong>The great unknown.</strong> When the U.S. government created data.gov they didn’t necessarily know what people would do with the data. The same could be said of efforts to expand access to music data, in that we don’t know how people will use it, but we know it creates the opportunity for new and innovative applications. When search is added to this technology, it could help to solve the age-old problem of remembering the source of the melody or chord progression that is stuck in your head.</li>
</ol>
<p>This is by no means a comprehensive list, but it should demonstrate some of the many reasons why a public media organization might want to get involved in the open-source music movement.</p>
  
  <div class="prev-next">
  
  <a href=".././public-music-public-media-introduction/" rel="previous">← Previous Post</a>
  
  
  <a href=".././role-open-source-music-scores/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
