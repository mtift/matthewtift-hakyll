<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - Public Music for Public Media: An Introduction</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>Public Music for Public Media: An Introduction</h1>
    <div class="post">

  <div class="info">
    Posted on June  1, 2012
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/public-media/">public media</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p>Open-source music is a <a href="http://boingboing.net/2012/05/28/opengoldberg.html">hot</a> <a href="http://entertainment.slashdot.org/story/12/05/29/0015231/open-source-bach-project-completed-score-and-recording-now-online">topic</a> right now, and it is in the best interest of public media stations to help promote it. This post is meant to serve as an overview of open-source music, and in the coming weeks – leading up to a <a href="http://opengoldberg.wpr.org/">special broadcast at Wisconsin Public Radio on June 24</a> – I will explore this topic in more depth.</p>
<h3 id="consider-the-history-of-music-distribution">Consider the history of music distribution</h3>
<p>In 1498, the Venetian government granted Ottaviano Petrucci the exclusive rights to print and sell music, for 20 years, in the Venetian Republic. In his application Petrucci said that he had “at great expense and with most watchful care, executed what many, not only in Italy but also outside of Italy, have long attempted in vain, which is, with the utmost convenience, to print Figured Music.”<sup>1 </sup></p>
<p>Half a century after Gutenberg’s press, Petrucci’s editions would rather suddenly expose music to a much larger section of the population, as musical scores were previously an expensive undertaking, with each piece of music copied by hand, and mostly the purview of the nobility and church. Petrucci’s editions signaled a monumental change in the history and distribution of music.</p>
<h3 id="the-work-has-already-begun">The work has already begun</h3>
<p>More than 500 years later, and much in the manner of Petrucci, there are numerous attempts to expose classical music to new audiences. For example, the <a href="http://imslp.org/">International Music Score Library Project</a> (IMSLP) – also known as the Petrucci Music Library – is building a sizeable online library of public domain music scores. The <a href="http://www.mutopiaproject.org/">Mutopia Project</a>, whose slogan is “free sheet music for everyone,” has similar goals. Another related example is <a href="http://musopen.org/">Musopen</a>, which works to improve “access and exposure to music by creating free resources and educational materials.”</p>
<p>Like Petrucci, the people working on these projects are creating new audiences for music by making music more available. Before Petrucci, music scores were essentially unavailable to the public. And before projects like IMSLP, music scores cost money.</p>
<p>If the above projects are the equivalent to giving away the sheets printed by Petrucci, then there are other efforts that are more akin to giving away Petrucci’s press, as well as his templates. Sheet music on <a href="http://MuseScore.com">MuseScore.com</a> is available not only as a PDF, but also in various open-source formats. This approach allows visitors to do much more than look at scores – they can, for example, download the scores in the open <a href="http://en.wikipedia.org/wiki/MusicXML">MusicXML</a> form and change them using the free and open-source <a href="http://musescore.org/">MuseScore</a> music composition and notion program.</p>
<p>These open formats – especially when combined – allow listeners to create new relationships with the music, to see as well as hear, to create as well as consume. These technologies allow listeners to become more than listeners by enabling wholly new manners of interaction. These are new kinds of “<a href="http://books.google.com/books/about/Musicking.html?id=1lOx9nr0aHkC">musicking</a>.”</p>
<h3 id="so-what-does-this-mean-for-public-media">So what does this mean for public media?</h3>
<p>These technologies could benefit our listeners in numerous ways. For example, many classical music stations publish their playlists online. What if we could take this a step further? Imagine that you are listening to your favorite classical radio station and you hear a string quartet playing something especially beautiful. It is one thing to find out the name of the quartet and composer of the piece. It would be quite another thing if you could go to that station’s website, download or stream the world-class recording you hear (for free) and download or view the score (for free). That is already starting to sound rather special.</p>
<p>Better yet, let’s say you knew that the particularly beautiful moment occurred a few minutes near the end of the work, so you don’t just find the name of the piece, you listen to the recording online and you watch as the score follows along (a bit like <a href="http://musescore.com/opengoldberg/goldberg-variations">this</a>). So you jump near the end of the piece, and you not only hear that beautiful moment again, but you get to see the notes. Even if you do not read music, you would suddenly be able to see the music like never before. And as a bonus you would have a copyright-free soundtrack to download for your next cat video on YouTube.</p>
<p>And yet, that is only the beginning. These technologies would allow public media stations to form new (or strengthen existing) partnerships. What music teacher would not love to have a free catalog of scores and recordings available to their students? The scores their students downloaded would not merely be for study because they could open the file and make changes to the score in the music notion program of their choice, such as <a href="http://www.finalemusic.com/finale/features/sharingyourmusic/musicxml.aspx">Finale</a>, <a href="http://www.us.sibelius.com/products/sibeliusedu/faq/index.html#8">Sibelius</a>, or <a href="http://musescore.org/en/handbook/file-format">MuseScore</a>.</p>
<p>Because the scores are available in MusicXML, there is nothing that would prevent someone writing software that allows real-time, group editing, a la <a href="http://en.wikipedia.org/wiki/Apache_Wave">Apache Wave</a> or <a href="http://etherpad.org/">Etherpad</a>. The open-source nature of these music-related projects imbues them with special powers that public media can amplify.</p>
<p>The influential American historian Lawrence Levine wrote of a “shared public culture that once characterized the United States.”<sup>2</sup> Promoting open-source music scores and recordings, and creating new sites of collaboration, might help bring together people that would not otherwise wake up in the morning and decide they want to contribute to an open-source project.</p>
<h3 id="please-listen-on-june-24">Please Listen on June 24</h3>
<p>Here at Wisconsin Public Radio, in partnership with the <a href="http://www.opengoldbergvariations.org/">Open Goldberg project</a> and <a href="http://musescore.com">MuseScore</a>, we will be conducting a bit of an experiment later this month on June 24. We will be <a href="http://opengoldberg.wpr.org/">broadcasting the Open Goldberg Variations</a> and we think it will be the first time in history that radio listeners will be able to follow the digital score of a broadcast recording as it is being played on air.</p>
<p>If it is a success, I will make the case that WPR should do even more, such as sponsor another open-source recording or bring our listeners together to help create a new open-source score. I have so many ideas I can hardly contain my excitement. But first this broadcast needs to demonstrate interest.</p>
<p>I invite you to please tune in – on air or <a href="http://opengoldberg.wpr.org/">online</a> – on June 24 and take part in radio history.</p>
<p><strong>Notes</strong><br />
1. Gustave Reese, “The First Printed Collection of Part-Music,” <em>The Musical Quarterly</em> 20 (1934): 40.<br />
2. Lawrence Levine, <em>Highbrow/Lowbrow: The Emergence of Cultural Hierarchy in America</em> (Cambridge: Harvard University Press, 1988), 9.</p>
  
  <div class="prev-next">
  
  <a href=".././what-means-public-radio/" rel="previous">← Previous Post</a>
  
  
  <a href=".././open-source-music-10-more-reasons-why-it-fits/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
