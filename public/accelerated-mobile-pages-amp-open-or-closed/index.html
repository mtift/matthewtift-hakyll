<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift -   Accelerated Mobile Pages (AMP): Open or closed?</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>  Accelerated Mobile Pages (AMP): Open or closed?</h1>
    <div class="post">

  <div class="info">
    Posted on April  6, 2016
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/AMP/">AMP</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p><em>(This article was cross-posted on <a href="https://opensource.com/life/16/4/accelerated-mobile-pages-amp-open-or-closed">opensource.com</a>.)</em></p>
<p>The AMP project has occupied my mind quite a bit lately: not only am I one of the developers of the <a href="https://amphtml.wordpress.com/2016/03/02/amping-up-drupal/" title="_blank">new AMP module for Drupal</a>, I am also an <a href="http://matthewtift.com/about#writing" title="_blank">outspoken</a> free software advocate and host of <a href="https://www.lullabot.com/podcasts/hacking-culture" title="_blank">Hacking Culture</a>, which features in-depth interviews with open source and free software advocates. I have had to decide whether or not I think AMP represents a positive change for the open web. Although I think it is still rather early to assess the full impact of AMP, I have concluded that it is a move in the right direction.</p>
<p>One fact that cannot be disputed is that AMP content loads quickly. AMP content loads an average of <a href="https://engineering.pinterest.com/blog/building-faster-mobile-web-experience-amp" title="_blank">four times faster</a> and uses <a href="https://amphtml.wordpress.com/2016/03/18/the-google-webmaster-help-forum-provides-publishers-with-support-on-all-things-amp/" title="_blank">10 times less data</a> than non-AMP pages. We all desire to see statistics like that when loading content on our devices.</p>
<p>We should also admit that this is no longer just “Google’s Accelerated Mobile Pages project.” Google created AMP, but the company has convinced many other publishers and platforms to get involved with the project. Current <a href="https://www.ampproject.org/who/" title="_blank">partners</a> include the likes of LinkedIn, Medium, Brightcove, Vine, Pinterest, The New York Times, Vox Media, The Washington Post, The Guardian, BBC, and The Wall Street Journal.</p>
<p>Unlike other solutions to the problem of a slow mobile web, AMP is an <a href="https://www.ampproject.org/" title="_blank">open source project</a>. The code for the project <a href="https://github.com/ampproject/amphtml" title="_blank">resides on GitHub</a>. It’s an active community with lots of <a href="https://github.com/ampproject/amphtml/issues" title="_blank">open issues</a> and thus far includes contributions from <a href="https://github.com/ampproject/amphtml/graphs/contributors" title="_blank">well over 100 people</a>. The project provides clear information about <a href="https://github.com/ampproject/amphtml/blob/master/GOVERNANCE.md" title="_blank">governance</a>, as well as a <a href="https://github.com/ampproject/amphtml/blob/master/CODE_OF_CONDUCT.md" title="_blank">code of conduct</a> (based on the <a href="http://hood.ie/code-of-conduct/" title="_blank">Hoodie Community Code</a>) that describes the project as a “positive, growing project and community” that aims to provide a “safe environment for everyone.”</p>
<p>More than just open source, the project seeks to be an open standard. Whereas previous attempts to build websites for mobile devices (“m.” sites, anyone?) had similar goals, they did not provide the kind of strict rules that AMP dictates. Anyone can implement the AMP standard, and it is already being used by <a href="https://engineering.pinterest.com/blog/building-faster-mobile-web-experience-amp" title="_blank">Pinterest</a>, <a href="http://techcrunch.com/2016/03/09/twitter-updates-moments-for-better-mobile-browsing-by-sending-users-to-amp-powered-pages/" title="_blank">Twitter</a>, <a href="https://googleblog.blogspot.com/2016/02/amping-up-in-mobile-search.html" title="_blank">Google</a>, and others. Because it is a standard, AMP HTML could potentially work in any browser or app, making it a much more “open” solution than any other proposed plans, especially those suggested by Apple and Facebook.</p>
<p>While AMP focuses primarily on maximizing speed, it provides publishers with flexibility that other proprietary solutions do not. Unlike solutions such as RSS or Apple News, where 3rd parties (e.g. designers at Facebook or Apple) control the appearance of content, publishers dictate the look and feel of their AMP content and those publishers have complete control over the elements on the page. In <a href="http://www.poynter.org/2015/inside-googles-plan-to-speed-up-the-mobile-web/390046/" title="_blank">Poynter’s analysis of AMP</a>, it is the publisher that is “in control of their product, they’re in control of serving the product. They’re in control of the business model of the product. Effectively, they’re in control of their own destinies.” Consequently, I disagree with the characterization of AMP as another <em>closed silo</em>.</p>
<p>The fact that Google’s AMP Cache servers store cached versions of AMP pages and serves them in search results is not altogether different from other CDNs caching those pages. Companies such as Akamai charge a lot of money for use of their CDN, and Google provides AMP caching at no cost. Not only does Google’s AMP Cache speed up the loading time of search results, it allows visitors to <a href="https://2.bp.blogspot.com/-v4HwU0Bb2H4/Vs0mLBtKsqI/AAAAAAAAR3c/JfjaZcVl3hc/s640/AMP_v6_phone_cropped.mov.gif" title="_blank">page through the AMP results in the carousel</a> with a flick of a finger. Google’s search results carousel is simply one implementation of the standard, and the publisher is still in control. The <a href="http://www.wired.com/2016/02/googles-amp-speeding-web-changing-works/" title="_blank">charge in Wired magazine</a> that “AMP is speeding up the web by changing how it works” feels hollow, unless the author equates Google’s search results with the web.</p>
<p>What might AMP mean for a site such as Opensource.com, which happens to be a Drupal site? With the <a href="https://www.drupal.org/project/amp" title="_blank">new Drupal AMP module</a>, the barriers to serving AMP for Opensource.com are greatly reduced. Adding AMP would provide the opportunity for Opensource.com articles (like this one) to appear at the <a href="https://googleblog.blogspot.com/2016/02/amping-up-in-mobile-search.html" title="_blank">top of Google’s mobile search results</a> and allow for AMP content to load more quickly on <a href="https://engineering.pinterest.com/blog/building-faster-mobile-web-experience-amp" title="_blank">social</a> <a href="http://techcrunch.com/2016/03/09/twitter-updates-moments-for-better-mobile-browsing-by-sending-users-to-amp-powered-pages/" title="_blank">media</a>. The articles would look the same and load faster if other platforms implemented the AMP standard. None of this would make Opensource.com dependent on AMP, as AMP would simply be served in addition to non-AMP content from the same server. In that respect, AMP content reminds me of <a href="http://www.hyperorg.com/blogger/" title="_blank">David Weinberger’s blog</a>, which he offers in three styles (“Style 1,” “Style 2,” and “Default”) and allows visitors to change it.</p>
<p>I am not ready to christen AMP the savior of the web, but I appreciate the philosophy behind it. AMP is not trying to feed the hungry or provide shelter to the homeless—it makes web pages load faster on phones. Short of teaching the world how to build fast web pages that do not bounce around as they are loading, I do not see any better proposals to speed up the mobile web. In essence, AMP <a href="https://www.ampproject.org/docs/get_started/technical_overview.html" title="_blank">forces people to build fast-loading websites</a>. AMP may not be the ideal solution for strong advocates of copyleft licenses (AMP is <a href="https://github.com/ampproject/amphtml/blob/master/LICENSE" title="_blank">Apache License 2.0</a>) or those who mistrust Google (the only company mentioned in the <a href="https://github.com/ampproject/amphtml/blob/master/GOVERNANCE.md" title="_blank">AMP project governance document</a>), but AMP feels like the most open solution.</p>
<p>I remain open to other opinions. If not AMP, then what?</p>
  
  <div class="prev-next">
  
  <a href=".././amping-drupal/" rel="previous">← Previous Post</a>
  
  
  <a href=".././why-paid-drupal-modules-fail-drupal-art/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
