<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - What &lt;audio&gt; means for public radio</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>What &lt;audio&gt; means for public radio</h1>
    <div class="post">

  <div class="info">
    Posted on May 28, 2012
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/public-radio/">public radio</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p>On Wednesday this week (May 30), I will be taking part in a “<a href="http://mediaengage.org/webinars/webinar_details.cfm?wbid=159955">first of a kind webinar</a>” about the state of audio and video on the web. I consider myself very lucky to be speaking beside some esteemed leaders from the public media technology community.</p>
<p>Although the webinar will address some of the technical issues regarding audio (and video, but here I am focusing on audio) on the web, it’s a rather complex topic, and on Wednesday we will focus on some specific implementations by showcasing new audio players from <a href="http://blog.prx.org/2012/03/new-player-new-ways-to-listen/">PRX</a>, <a href="http://www.npr.org/blogs/inside/2011/11/14/142303990/introducing-the-infinite-player">NPR</a>, as well as the new HTML5 audio player that we use on <a href="http://ttbook.org">ttbook.org</a>.</p>
<p>The underlying goal with audio players these days is to move away from proprietary plugins like Flash, Silverlight, or QuickTime, and make playing audio with the HTML5 <a href="http://www.w3.org/TR/html-markup/audio.html">&lt;audio&gt; tag</a> as easy as displaying an image with the <a href="http://www.w3.org/TR/html-markup/img.html">&lt;img&gt; tag</a>. Among other benefits, this gives browsers a more consistent way to stream audio without needing any of the aforementioned plugins and makes it easier for designers to create a consistent look and feel on their sites.</p>
<p>While the &lt;audio&gt; tag offered a good start, it only provided limited functionality and was not nearly as sophisticated as something like Flash, so the <a href="https://wiki.mozilla.org/Audio_Data_API">Audio Data API</a> and the <a href="http://www.w3.org/TR/webaudio/">Web Audio API</a> are supposed to help bring the standard more on par with other commercial plugins.</p>
<p>A common phrase used to describe a modern audio player on the web is “HTML5 player with Flash fallback.” The “HTML5 player” part of this description refers to using the &lt;audio&gt; tag, and if the browser does not support &lt;audio&gt;, it tries to play the audio with Flash (or Silverlight, depending on the player). One of the first, and most popular, players to offer this functionality was <a href="http://www.jplayer.org/about">jPlayer</a>, which launched in 2009. Other capable player projects that meet this general characterization include <a href="http://mediaelementjs.com/">MediaElement.js</a>, <a href="http://www.schillmania.com/projects/soundmanager2/">SoundManager 2</a>, <a href="http://www.speakker.com/">Speakker</a>, and <a href="http://kolber.github.com/audiojs/">audio.js</a>.</p>
<p>One of the fundamental impediments to using the &lt;audio&gt; tag on the web concerns the browsers used by our listeners. The website <a href="http://caniuse.com">caniuse.com</a> provides up-to-date information about what <a href="http://caniuse.com/#feat=audio">browsers support the &lt;audio&gt; tag</a>. This highlights a problem at the station where I work because nearly a quarter of our visitors are using IE8, which does NOT support the &lt;audio&gt; tag. So we’re hoping they have Flash installed. Thankfully, the majority of our other visitors use browsers that support the &lt;audio&gt; tag.</p>
<p>This is just a taste of the technical issues concerning &lt;audio&gt; on the web.</p>
<p>In the webinar, we’ll address issues that reflect how users consume the audio. For example, NPR differentiates between “<a href="http://www.npr.org/blogs/inside/2011/11/14/142303990/introducing-the-infinite-player">engaged listening&quot; and &quot;distracted listening</a>” to describe why they created their <a href="http://www.npr.org/sandbox/conplay/">Infinite Player</a>, which targets the latter type of interaction. The approach we used when building <a href="http://ttbook.org">ttbook.org</a> targeted the “engaged listeners,” requiring somewhat more effort for our users while allowing them more control. On <a href="http://core.digitalservices.npr.org/">Core Publisher</a> sites, such as <a href="http://wprnews.org">wprnews.org</a>, NPR advises a more “traditional” approach in that audio players do little more than reliably play audio files related to the story that is currently being viewed.</p>
<p>There exists, to be sure, a wide array of competing technologies in this space, and the knowledgable presenters on the webinar will have useful information to help us all provide the best possible experience for our users and listeners. I hope you will <a href="http://mediaengage.org/webinars/webinar_details.cfm?wbid=159955">join us on Wednesday</a>.</p>
  
  <div class="prev-next">
  
  <a href=".././wpr-partners-open-goldberg/" rel="previous">← Previous Post</a>
  
  
  <a href=".././public-music-public-media-introduction/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
