<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - Creating Daily Reminders with Free Software</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>Creating Daily Reminders with Free Software</h1>
    <div class="post">

  <div class="info">
    Posted on June 25, 2015
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/zenity/">zenity</a>, <a href="../tags/free-software/">free software</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p>Many of us who build websites for a living need to log the hours we work each day. It can be easy to occasionally forget a day. There are some interesting solutions to this problem, such as time trackers like <a href="https://toggl.com/">Toggl</a> or <a href="https://letsfreckle.com/">Freckle</a> or email reminder applications such as <a href="http://www.boomeranggmail.com">Boomerang for Gmail</a>.</p>
<p>But what about a free/libre solution? There are a variety of tools that could be used for such a purpose, such as <a href="https://packages.debian.org/jessie/remind">remind</a> or <a href="https://packages.debian.org/jessie/gxmessage">gxmessage</a>, or even simply setting up daily emails with <a href="http://en.wikipedia.org/wiki/Sendmail">Sendmail</a>.</p>
<p>My preferred solution to this problem is to use a program called <a href="http://en.wikipedia.org/wiki/Zenity">Zenity</a>. Zenity is a <a href="https://wiki.gnome.org/action/show/Projects/Zenity">GNOME program</a> that enables a shell script to create GTK+ dialog boxes. A Zenity Hello, World! program, run from the command line, might look something like this:</p>
<pre><code>zenity --info --text &quot;Hello, World!&quot;</code></pre>
<p>In this example, the <code>--info</code> flag is a signal to display an info dialog and <code>--text</code> sets the dialog text to “Hello, World!”.</p>
<div class="figure">
<img src="../images/hello-world.png" />
</div>
<p>A gentle reminder about logging hours might look like this:</p>
<pre><code>zenity --question --text=&quot;Did you enter your hours?&quot; --ok-label=&quot;Yes&quot; --cancel-label=&quot;No&quot;</code></pre>
<p>Here the <code>--question</code> flag creates a question dialog with the <code>--text</code> value. The other two flags change the labels of “OK” and “Cancel” to “Yes” and “No”.</p>
<div class="figure">
<img src="../images/enter-hours.png" />
</div>
<p>Combining Zenity with cron and bash allows for all kinds of possibilities. To create a daily reminder, the steps might go something like this:</p>
<p><strong>1. Create a bash script somewhere in your $PATH (I like to keep my personal scripts in ~/bin):</strong></p>
<pre><code>#!/bin/bash

CONTINUE=1

while [ $CONTINUE -eq 1 ]; do

 # Wait 10 minutes.
 sleep 10m

 # The response to a zenity question is in the $? variable.
 CONTINUE=`zenity --question --text=&quot;Did you enter your hours?&quot; --ok-label=&quot;Yes&quot; --cancel-label=&quot;No&quot; --display=:0.0; echo $?`;

done</code></pre>
<p>This script will display this message, and then display it again 10 minutes later until “Yes” is clicked. This starts by setting a variable called “CONTINUE” to 1, and as long as CONTINUE equals (<code>-eq</code>) 1, it will wait 10 minutes (<code>10m</code>) and then display the question. If “Yes” is clicked, CONTINUE will be set to zero and the program will complete. If “No” is click, CONTINUE will still be 1 and the program will wait ten minutes before displaying the question again. When using <code>--question</code> zenity stores the response in a <code>$?</code> variable, which is why we assign the value of <code>$?</code> to CONTINUE (<code>echo $?</code>).</p>
<p><strong>2. Add line to .bashrc:</strong></p>
<pre><code># Allow use of zenity in cron jobs (xhost controls user access to the X server)
xhost local:USERNAME &gt; /dev/null</code></pre>
<p>There are some <a href="http://promberger.info/linux/2009/01/02/running-x-apps-like-zenity-from-crontab-solving-cannot-open-display-problem">issues with Zenity and cron</a>, so this line is needed for the cron job to work. Replace USERNAME with your username.</p>
<p><strong>2. Create a cron job to run the script twice a day:</strong></p>
<pre><code>40 8,16 * * 1-5 /home/USERNAME/bin/daily-reminder.sh</code></pre>
<p>For most Linux (and Mac) systems, you can just type <code>crontab -e</code> and then enter this line. This will display the reminder every week day (<code>1-5</code>) at 8:50am and 4:50pm (<code>40 8,16</code>). Remember that the bash script starts by waiting 10 minutes, so in this case set the time for 8:40 and 16:40.</p>
<p>I have been using this system for quite a while, and it works very well for me. It could also be modified to be used as a simple replacement for programs like <a href="https://userbase.kde.org/RSIBreak">RSIBreak</a>, for example displaying a popup message every 20 minutes for 20 seconds reminding yourself to take a break, look away from your screen, and move around a little bit.</p>
  
  <div class="prev-next">
  
  <a href=".././cultural-construction-drupal/" rel="previous">← Previous Post</a>
  
  
  <a href=".././better-then-bigger-cultivating-drupal-community/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
