TITLE
Teaching Kids to Code

PROPOSAL
The Drupal community, like other free software communities, tends to be very focused on the current version of its software, or perhaps building the next version. However, when we are thinking about the next generation of Drupalers, it can be helpful to broaden our mindset and consider not just how to teach Drupal, but how we can teach young kids the skills required for to them learn Drupal. One way to do this is to teach our kids to code.

The Minnetonka school district is leading the state of Minnesota by including coding as part of its curriculum. Although I do not work for the district, I have been involved in these efforts as a parent, "hour of code" volunteer, elementary school "coding club" mentor, Finch robot club teacher, high school CoderDojo mentor, member of the district's "Tonka Code Design Team," and as a relentless free software advocate. Through these experiences, and related research, I have learned a great deal about how kids learn to code, and how we can help them on their journey.

Some key components of the Minnetonka school district's approach include:

 * Holding community listening sessions and administering surveys about priorities
 * Creating an advisory committee consisting of administrators, teachers, parents, and community members
 * Building a coding program that starts with elementary students
 * Targeting curricular (in school), co-curricular (e.g., lunch groups), and extra-curricular (after-school coding clubs)
 * Integrating coding lessons across disciplines (science, math, art, music, etc.)
 * Focusing on free software

I have spoken with a wide variety of administrators, teachers, mentors, reporters, parents, and students. Combining their insights with mine, this talk will offer practical advice for anyone who would like to see (more) coding taught in their local community. It is my goal to help enable others to teach kids the basics of problem-solving, thinking, and coding in order to empower the next generation to join our Drupal community. The Drupal community has improved my life dramatically, and I feel a deep desire to pass on the skills and values that I have learned.



ADDITIONAL INFO
KSTP: "Minnetonka Schools Lead the Way with Computer Coding" (https://www.youtube.com/watch?v=ZuttEM5y4UU)
Star Tribune: "Minnesota schools scramble to catch up on computer coding classes" (http://www.startribune.com/local/west/279732482.html)
Sun Sailor: "Cracking the code: Tonka CoderDojo" (http://sailor.mnsun.com/2014/09/05/cracking-the-code-tonka-coderdojo)
Promo video: "Minnetonka Elementary Coding" https://www.youtube.com/watch?v=ivbXhzdUL4g








