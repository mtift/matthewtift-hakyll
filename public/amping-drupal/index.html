<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - AMPing up Drupal</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>AMPing up Drupal</h1>
    <div class="post">

  <div class="info">
    Posted on February 26, 2016
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/AMP/">AMP</a>, <a href="../tags/Drupal/">Drupal</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p><em>(This article was cross-posted on <a href="https://www.lullabot.com/articles/amping-up-drupal">lullabot.com</a> and the <a href="https://amphtml.wordpress.com/2016/03/02/amping-up-drupal/">AMP project blog</a>).</em></p>
<p>Today we are proud to announce a <a href="https://www.drupal.org/project/amp">new Drupal 8 module</a> that provides support for the Accelerated Mobile Pages (AMP) Project. The AMP Project is a new open source initiative which drastically improves the performance of the mobile web. In January 2016 Lullabot and Google started working together to create the Drupal <a href="https://www.drupal.org/project/amp">AMP module</a>. A beta version of AMP module for Drupal 8 is available immediately, and we are starting work on a Drupal 7 version of the module that will be available in mid-March.</p>
<p>In many cases, the mobile web is a slow and frustrating experience. The AMP Project is an open source initiative that embodies the vision that publishers can create mobile optimized content once and have it load instantly everywhere. When AMP was first introduced last October, many commentators immediately compared it to Facebook’s Instant Articles and Apple’s News app. One of the biggest differentiators between AMP and other solutions is the fact that AMP is open source.</p>
<p><a href="https://github.com/ampproject/amphtml">AMP HTML</a> is, essentially, a subset of HTML. And it really <a href="http://adage.com/article/digital/google-amp-faster-mobile-web/302837/">makes the web fast</a>. AMP HTML is designed to support smart caching, predictable performance, and modern, beautiful mobile content. Since AMP HTML is built on existing web technologies, publishers continue to host their own content, craft their own user experiences, and flexibly integrate their advertising and business models – all using familiar tools, which will now include Drupal!</p>
<p>One of the most touted features of Drupal is its flexibility, so making Drupal produce AMP HTML has required a lot of careful consideration of the design approach. To make Drupal output AMP HTML, we have created an <a href="https://www.drupal.org/project/amp">AMP module</a>, <a href="https://www.drupal.org/project/amptheme">AMP theme</a>, and a <a href="https://github.com/Lullabot/amp-library">PHP Library</a>.</p>
<p>When the <a href="https://www.drupal.org/project/amp">AMP module</a> is installed, AMP can be enabled for any content type. At that point, a new AMP view mode is created for that content type, and AMP content becomes available on URLs such as node/1/amp or node/article-title/amp. We also created special AMP formatters for text, image, and video fields.</p>
<p>The <a href="https://www.drupal.org/project/amptheme">AMP theme</a> is designed to produce the very specific markup that the <a href="https://www.ampproject.org/">AMP HTML standard</a> requires. The AMP theme is triggered for any node delivered on an /amp path. As with any Drupal theme, the AMP theme can be extended using a subtheme, allowing publishers as much flexibility as they need to customize how AMP pages are displayed. This also makes it possible to do things like place AMP ad blocks on the AMP page using Drupal’s block system.</p>
<p>The PHP Library analyzes HTML entered by users into rich text fields and reports issues that might make the HTML non-compliant with the AMP standard. The library does its best to make corrections to the HTML, where possible, and automatically converts images and iframes into their AMP HTML equivalents. More automatic conversions will be available in the future. The PHP Library is CMS agnostic, designed so that it can be used by both the Drupal 8 and Drupal 7 versions of the Drupal module, as well as by non-Drupal PHP projects.</p>
<p>We have done our best to make this solution as turnkey as possible, but the module, in its current state, is not feature complete. At this point only node pages can be converted to AMP HTML. The initial module supports AMP HTML tags such as <a href="https://www.ampproject.org/docs/reference/amp-ad.html">amp-ad</a>, <a href="https://www.ampproject.org/docs/reference/amp-pixel.html">amp-pixel</a>, <a href="https://www.ampproject.org/docs/reference/amp-img.html">amp-img</a>, <a href="https://www.ampproject.org/docs/reference/amp-video.html">amp-video</a>, <a href="https://www.ampproject.org/docs/reference/extended/amp-analytics.html">amp-analytics</a>, and <a href="https://www.ampproject.org/docs/reference/extended/amp-iframe.html">amp-iframe</a>, but we plan to add support for more of the <a href="https://www.ampproject.org/docs/reference/extended.html">extended components</a> in the near future. For now the module supports Google Analytics, AdSense, and DoubleClick for Publisher ad networks, but additional network support is forthcoming.</p>
<p>While AMP HTML is already being served by some of the biggest publishers and platforms in the world — such as The New York Times, The Economist, The Guardian, BBC, Vox Media, LinkedIn, Pinterest, and many more! — you don’t have to be a large publisher to take advantage of AMP. Today, any Drupal 8 site can output AMP HTML using the <a href="https://www.drupal.org/project/amp">AMP module</a>. We invite you to try it out and let us know what you think!</p>
<p>Finally, if you are interested in this topic and want to learn more about publishing AMP with Drupal, please leave a comment on our <a href="https://events.drupal.org/neworleans2016/sessions/amping-drupal">DrupalCon New Orleans proposal</a>.</p>
  
  <div class="prev-next">
  
  <a href=".././configuration-management-drupal-8-key-concepts/" rel="previous">← Previous Post</a>
  
  
  <a href=".././accelerated-mobile-pages-amp-open-or-closed/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
