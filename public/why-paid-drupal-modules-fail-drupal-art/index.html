<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - Why Paid Drupal Modules Fail: Drupal as Art</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>Why Paid Drupal Modules Fail: Drupal as Art</h1>
    <div class="post">

  <div class="info">
    Posted on April  7, 2016
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/Drupal/">Drupal</a>, <a href="../tags/free-software/">free software</a>, <a href="../tags/Wordpress/">Wordpress</a>, <a href="../tags/art/">art</a>, <a href="../tags/culture/">culture</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p><em>(This article was cross-posted on <a href="https://www.lullabot.com/articles/why-paid-drupal-modules-fail-drupal-as-art">lullabot.com</a>.)</em></p>
<p>Recently the Drupal Association announced accepted sessions for <a href="https://events.drupal.org/neworleans2016/sessions/accepted">DrupalCon New Orleans</a>. While it looks like we can expect another great DrupalCon (this will be my 7th straight North American DrupalCon), one particular session on the program about the sale of Drupal modules caught my attention. Although I have tried to stay focused on preparing my <a href="https://events.drupal.org/neworleans2016/sessions/configuration-management-developers-drupal-8">own</a> <a href="https://events.drupal.org/neworleans2016/sessions/amping-drupal">sessions</a>, I have had conversations with other people in the Drupal community about “paid modules” that have led me to the conclusion that confusion about this topic persists. So here I would like to offer a perspective on why these kinds of plans consistently fail. Specifically, I hope to expand the scope of this frequently discussed issue and suggest <em>why</em> so many paid module initiatives fail: the Drupal community protects its free software with the same vigor that other communities protect artistic freedom.</p>
<h2 id="the-gpl-protects-software-freedoms">The GPL Protects Software Freedoms</h2>
<p>Before offering my analysis, I should start by acknowledging the obvious reason why paid Drupal modules fail: the <a href="http://www.gnu.org/licenses/gpl-2.0.en.html">General Public License (GPL)</a>. The Drupal Association <a href="https://www.drupal.org/about/licensing#q7">clearly states</a> that “Drupal modules and themes are a derivative work of Drupal. If you distribute them, you must do so under the terms of the GPL version 2 or later.” Consequently, anyone who wishes to sell a Drupal module must provide the source code.</p>
<p>Drupal community members are standing by with their credit cards ready to purchase those modules – and then post the code to Drupal.org so that everyone else can use it. These principled individuals believe that copyleft provides a powerful tool for ensuring software freedom and will exercise their right guaranteed by the GPL that allows the recipient (buyer) “to copy, distribute or modify” the module or theme purchased. The seller “may not impose any further restrictions on the recipients’ exercise of the rights.” The GPL greatly curtails most plans to sell Drupal modules, and these would-be capitalists might have more success selling popsicles during a blizzard.</p>
<p>Yet the GPL does not close the debate on this subject. I have heard many reasons provided to justify revisiting this conversation, and most frequently they come down to money. Let me share some direct quotes used to justify the pursuit of a “Drupal Marketplace,” “paid Drupal apps,” or “paid modules”:</p>
<ul>
<li>“<a href="https://www.phase2technology.com/blog/the-open-app-standard-better-usability-drupalwide/">Greater usability for Drupal</a>”</li>
<li>“<a href="https://www.youtube.com/watch?v=NwUxi34rAqg#t=13m30s">Lack of products is a risk to the platform</a>”</li>
<li>“<a href="https://www.cmscritic.com/subhub-launches-worlds-first-drupal-powered-app-store/">Give non-technical people the opportunity to build an outstanding website using Drupal</a>”</li>
<li>Drupal should learn from “<a href="https://events.drupal.org/neworleans2016/sessions/selling-drupal-modules">the success of WordPress</a>” and its paid plugins</li>
<li>“<a href="https://www.youtube.com/watch?v=NwUxi34rAqg#t=12m11s">Quality products boost the whole platform</a>”</li>
<li>“<a href="https://events.drupal.org/neworleans2016/sessions/selling-drupal-modules">Paid modules and distros will lead to more money for innovation in Drupal</a>”</li>
<li>“<a href="https://www.youtube.com/watch?v=NwUxi34rAqg#t=41m20s">It’s a foregone conclusion that there are going to be app stores for Drupal</a>”</li>
<li>“<a href="https://www.youtube.com/watch?v=NwUxi34rAqg#t=40m50s">There will be people drawn to the community who were not drawn to it before</a>”</li>
</ul>
<p>I have read and listened to a great deal of arguments defending these commercial endeavors, and I remain unconvinced that the potential upsides outweigh the considerable drawbacks.</p>
<h2 id="a-history-of-failure">A History of Failure</h2>
<p>The words of the American literary critic Fredric Jameson influence my thinking on this topic: “always historicize!” A look at attempts to sell Drupal modules reveals a distinct lack of success, and yet people continue to claim they have found a solution. Consider SubHub, who announced in 2011 to great fanfare the “<a href="https://www.cmscritic.com/subhub-launches-worlds-first-drupal-powered-app-store/">World’s First Drupal-Powered App Store</a>.” They hoped to offer some of their “apps” at no cost, while other “apps” would require a small recurring charge. This plan failed and SubHub <a href="http://www.memberdigital.com/blog/finding-the-perfect-cms/">abandoned their initiative in 2013</a>, lamenting the fact that the Drupal community “simply didn’t share the same motivation to make Drupal a highly commercial, competitive alternative to WordPress.” My apologies to anyone who built a Drupal site with integral SubHub functionality.</p>
<p>Also in 2011 – a big year for “apps” in the Drupal community – Phase2 announced the <a href="https://www.phase2technology.com/blog/the-open-app-standard-better-usability-drupalwide/">Open App Standard initiative</a>. The title contains the word “open” so surely this plan would find traction with Drupal people, right? While Phase2 found <a href="https://www.phase2technology.com/blog/introducing-openpublics-app-strategy-reimagining-content-management-for-public-sector/">some success with OpenPublic</a>, which uses apps, I don’t see evidence that “apps” ever found traction in the Drupal community, and certainly not adoption with alacrity.</p>
<p>Keep in mind that many people make money selling Drupal services and that the community generally supports such efforts. I work at a company filled with people who make money building Drupal websites. Rather, I think this evidence shows that <a href="https://www.drupal.org/node/2110635">paid module schemes tend to fail</a>, that others have found Drupal to be “<a href="http://blog.varunarora.com/why-we-stopped-using-drupal-for-our-platform/">actually a horrible solution to build apps</a>,” and that when people ask the question, “Is a Drupal App Store a good idea?” the community generally responds with a decisive “no” (<a href="http://www.bmannconsulting.com/archive/buying-ponies-from-a-drupal-app-store/">unless it features ponies</a>). We absolutely want people to succeed in the community, just not by selling modules.</p>
<p>Certainly exceptions exist. For instance, some companies have found success selling Drupal themes. A case could be made that Acquia, a company founded by Drupal’s creator, peddles its own variety of “paid apps.” The <a href="https://www.drupal.org/project/acquia_connector">Acquia Connector module</a> “enhances the Drupal experience by providing the support and network services to operate a trouble-free Drupal website.” However, the module does little without an Acquia Subscription, which requires regular payments.</p>
<p>Acquia, and other similar services, get around the restrictions of the GPL by taking advantage of something known as the “application service provider (ASP) loophole.” When the Free Software Foundation published <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html">GPL version 2</a> (all Drupal code must be GPL version 2 or later) in 1991 we did not use web services like we do today. Because providing a subscription service (such as <a href="https://www.drupal.org/project/mollom">Mollom</a> or <a href="https://www.drupal.org/project/new_relic_rpm">New Relic</a>) does not involve distributing software, Acquia need not provide any additional code than that which connects the site to the service.</p>
<p>The Drupal community could adopt a stronger copyleft license that closes the ASP loophole, such as the <a href="http://www.affero.org/oagpl.html">Affero General Public License (AGPL)</a>. Just do not expect this to happen anytime soon. Dries Buytaert, the founder and project lead of Drupal, built a business that takes advantage of this loophole and he made his opinion clear a decade ago in a blog post titled, “<a href="http://buytaert.net/long-live-the-web-services-loophole">Long Live the Web Services Loophole</a>.”</p>
<p>Consequently, the focus of the discussion around paid modules often revolves more around the merits and problems of what Richard Stallman calls “<a href="https://www.gnu.org/philosophy/who-does-that-server-really-serve.en.html">Service as a Software Substitute (SaaSS)</a>” than on actually selling Drupal modules that address common challenges. While some companies have found success with the SaaSS model, why do so many others fail? To answer this question, we must look beyond code and licenses to the Drupal community.</p>
<h2 id="products-and-practices">Products and Practices</h2>
<p>I wrote at length about the Drupal community in previous articles such as “<a href="https://www.lullabot.com/articles/the-cultural-construction-of-drupal">The Cultural Construction of Drupal</a>” and “<a href="https://www.lullabot.com/articles/better-then-bigger-cultivating-the-drupal-community">Better, then Bigger: Cultivating the Drupal Community</a>,” and I do not intend to re-hash those same ideas here. Rather, I would like to examine why “paid module” schemes have flopped.</p>
<p>As I see it, one fundamental explanation for failure has to do with when business people misunderstand Drupal as a <em>product</em> rather than an organic accumulation of <em>practices</em>, or more broadly, when they ignore the differences between machines and humans. I deliberately add the qualifier “business” to describe these people because all efforts to create paid products are intended to generate revenue. Efforts to sell Drupal modules, in the Marxist sense, epitomize efforts to create capital, to accumulate money – a goal that comes off as at odds with a community that values participation.</p>
<p>All modules that come with a fee are intended to generate revenue, but we must not forget that “free” modules (that are also “<a href="http://www.gnu.org/philosophy/free-sw.en.html">Free</a>”) have the potential to do the same. Many people, <a href="https://www.drupal.org/project/amp">including me</a>, enjoy getting paid to write Drupal code. For the paid module argument to work, its defendant must demonstrate that existing models are somehow lacking. Defending a new commercial approach necessitates, in my view, demonstrating the unsatisfactory nature of a free software project with more than <a href="https://www.drupal.org/project/project_module">33,000+ shared modules</a> that has existed for more than 15 years and has a vibrant <a href="https://www.drupal.org/drupal-services">marketplace of vendors offering Drupal services</a>. Such an argument not only requires a high level of mental gymnastics, it, at least tacitly, represents an affront to years of successful collaboration.</p>
<p>Because some of those recommending that Drupal re-evaluate its commercial ecosystem are contributing members of the community, I open myself up to the critique of constructing inaccurate divisions, pitting Drupal and its community against business people who desire to convert Drupal into a revenue-generating product. But we know too well that many of us in the Drupal community could represent either side. We aim to balance our need to support our families with our desire to defend Drupal and fight for its freedom. Luckily, we can often choose both.</p>
<h2 id="drupal-as-art">Drupal as Art</h2>
<p>Moving away from discussions of licenses and capitalist motives, I would now like to venture beyond the typical boundaries of the “paid module” discussion to explore why people feel so connected to Drupal. I get the sense that many people in the Drupal community do not actually understand the intricacies of the GPL and how it differs from other free software licenses such as the AGPL. I believe that the community is protecting something more ephemeral. Consequently, this part of my argument ventures into much more abstract topics and requires thinking about Drupal differently, less as a “technology” and more as an artistic practice.</p>
<p>I am not the first person to notice Drupal’s artistic side. For example, a DrupalCon session had the title “<a href="https://archive.org/details/DrupalconBoston2008-ZenAndTheArtOfDrupal">Zen and the Art of Drupal</a>.” Someone else even created a new word, “<a href="https://drupalwatchdog.com/volume-2/issue-1/druplart">Drupl’Art</a>,” to describe “Drupal code that is both beautiful and artistic.” The conception of “Drupal as art” is not new, but I am going to be using it here in order to offer new insights into how the Drupal community works.</p>
<p>More than just a thought experiment, the idea of Drupal as art becomes useful when we position it within another long-running debate, namely the effect of technology on art. For instance, when proponents position technological advances as something that improves the lives of many, critics will sometimes note that those advances also threaten the purity of art – for example, photographs (the “new technology”) were seen as a threat to portraiture (“pure art”). Ironically, Drupal, as I construct it here plays the role of art, even as we cannot deny that most people would label Drupal “technology” well before they would ever call it “art.” And yet when members of the Drupal community react negatively to the suggestion of paid modules, it is not simply because of the GPL, it is because the community is defending its perceived “purity.”</p>
<p>Drupal and art have both been understood as pure expressions, deeply tied to their predecessors, consisting of ever changing practices, and driven by community. Yet it would be idle to suggest that concerns about the threat of technology to art are exactly the same as concerns about the effect of paid modules on Drupal’s ecosystem. Nonetheless, I believe that we can better understand the Drupal community’s typical allergic reaction to “paid modules” by interrogating previous debates about technology and art.</p>
<p>This line of reasoning follows a long history of thinkers who have conflated art and technology that goes at least as far back as Ancient Greece. The Greeks did not enjoy art for aesthetic reasons. The word “technology” refers to a “treatise on a practical art or craft” and its Greek root is <a href="https://en.wikipedia.org/wiki/Techne"><em>techne</em></a>. For the Greeks, <em>techne</em> also referred to art. <a href="http://plato.stanford.edu/entries/episteme-techne/">Aristotle described <em>techne</em></a> as both practical knowledge and practical art. And what Drupal enthusiast would not admit that most websites convey practical knowledge?</p>
<p>In his 1954 essay, “<a href="https://en.wikipedia.org/wiki/The_Question_Concerning_Technology">The Question Concerning Technology</a>,” the German philosopher Martin Heidegger variously described both art and technology as a “mode of revealing,” a way of “bringing forth.” A painting reveals another person’s point-of-view and a website “reveals” information. As uncomfortable as it may seem to some among us to describe Drupal as art, it helps explain why people feel so connected to Drupal and vigorously defend its purity. The community wants to work together and reveal its solutions rather than hide them behind a web service. Developers treasure not just revealing websites, but revealing (sharing) the code in the modules that enables their functionality.</p>
<p>Another well-regarded German philosopher, Walter Benjamin, provided valuable insights that are useful for our purposes in his 1936 essay, “The Work of Art in the Age of Its Technological Reproducibility.” Therein Benjamin explores when inventions such as photography and film (both being kinds of “mechanical reproduction”) “separated art from its basis in cult, the semblance of its autonomy disappeared forever.” Not only did these products threaten the group, they also threatened the group’s output (its art). He believed that “<em>the unique value of the ‘authentic’ work of art always has its basis in ritual</em>.” Likewise in the Drupal community we value ritual: we discuss issues, post patches, review the work of others, attend camps and cons, celebrate the accomplishments of new members, and create code with unique value – code that lives with other Drupal code on Drupal.org. Hiding code threatens our rituals.</p>
<p>Paid modules or services revoke our access, our autonomy, and our beloved practices. Drupal is something people do, and we cannot learn by doing when we cannot see the code. Products are purchased, while practices are lived. Drupal, like other forms of <em>techne</em>, is communication, and we cannot communicate in the same manners with black boxes as we can in the issue queue.</p>
<p>In addition to affecting what we do, the existence of paid modules could negatively affect <em>perceptions</em> of the Drupal community. Benjamin writes of a “decay of the aura,” which sounds much like what the Drupal community works against. While some may argue, as one Slate writer did, simply that “<a href="http://www.slate.com/articles/news_and_politics/politics/2009/10/message_error.html">Drupal hates change</a>,” many in the Drupal community still believe that Drupal exudes a sort of magical aura. We hold the community close to our hearts and defend its name. We do not half-heartedly <a href="https://assoc.drupal.org/support-project-you-love">promote our community</a>, we instead speak of our “unique, diverse Drupal community,” say that “Drupal has made a big difference in my life,” and that “I’m truly proud to be part of this community.” We protect our people, practices, and reputation by following the <a href="https://www.drupal.org/dcoc">Drupal Code of Conduct</a>, in the hope that “we preserve the things that got us here.”</p>
<p>Our current system for creating modules on Drupal.org values the work of humans. Benjamin observes, “What matters is the way the orientation and aims of that technology differ from those of ours. Whereas the former made the maximum possible use of human beings, the latter reduces their use to the minimum.” Drupal is not so much the thing that is built but the way it is built by people. In fact, one of Lullabot’s most frequently quoted core values is “<a href="https://www.lullabot.com/values">be human</a>.”</p>
<p>The Drupal software will never be complete, and we hope the sharing continues indefinitely. In the same spirit, Benjamin believes, “It has always been one of the primary tasks of art to create a demand whose hour of full satisfaction has not yet come.” The Drupal community wants to continue building, to keep moving forward, never content that we have finally arrived at an “end” where our interactions have been replaced by web services.</p>
<p>Fredric Jameson once warned that “machines are indeed machines of reproduction rather than of production.” The Drupal community has produced a great deal of code that powers much of the web. Few among us would want to shift from production to reproduction, from working together to solve problems to purchasing solutions to problems. Repeatedly and continuously solving the same problems – building the same websites – breeds boredom. We like to create new and shiny things, producing not reproducing. ur rituals, our process, and our freedom to tinker allows us to continuously move forward.</p>
<h2 id="the-contrasting-case-of-wordpress">The Contrasting Case of WordPress</h2>
<p>Even if these theories accurately describe the Drupal community, they do not fully account for the success of paid WordPress plugins. Do paid plugins make WordPress <em>unpure</em>, inhabited by a community of dispassionate contributors? Absolutely not. Does the availability of paid plugins somehow cheapen the WordPress community or imply that WordPress lacks meaningful rituals? Certainly no. WordPress powers a huge chunk of the web with GPL-licensed code. It would be fruitless to deny the overwhelming success of a project with a vibrant community. Rather, I hope to convey how the WordPress community’s embrace of paid plugins informs my argument that the Drupal community understands its practices as a kind of artistic expression.</p>
<p>Simply suggesting that WordPress and Drupal are different helps about as much as arguing that Drupal and WordPress are words that contain an incommensurable number of letters. We could go a step further, as <a href="http://buytaert.net/why-woonattic-is-big-news-for-small-businesses">Dries Buytaert often does</a> and argue not only are they different, but that WordPress and Drupal target <em>different markets</em>, with “WordPress dominating the small business segment” and Drupal geared toward the “larger organizations with more complex requirements” (an idea <a href="https://www.lullabot.com/articles/the-cultural-construction-of-drupal">I dispute</a>). If that were true, one would think the community that caters to “large organizations” would have the customers with the funds to purchase modules rather than get them at no cost. Then again, the reverse argument seems equally defensible, and that small businesses would rather pay for plugins than for those <a href="https://wpml.org/2013/11/drupal-developers-make-x10-wordpress-developers/">reportedly expensive Drupal developers</a>. None of these avenues feel satisfactory.</p>
<p>One might expect that WordPress and Drupal espouse contrasting ideas about paid add-ons and the GPL. Quite the contrary, the Wordpress.org <a href="https://wordpress.org/about/license">licensing page</a> provides clear guidance about how plugins should be licensed: “derivatives of WordPress,” including plugins and themes, “inherit the GPL license.” While they admit to “some legal grey area regarding what is considered a derivative work,” they “feel strongly that plugins and themes are derivative work and thus inherit the GPL license.” The community leaders back up their assertions with <a href="https://markjaquith.wordpress.com/2010/07/17/why-wordpress-themes-are-derivative-of-wordpress/">thoughtful discussion</a> as well as expert analysis from the <a href="https://wordpress.org/news/2009/07/themes-are-gpl-too/">Software Freedom Law Center</a>. The WordPress licensing page even provides a link to Drupal’s “excellent page on licensing as it applies to themes and modules (their word for plugins).” Thus, WordPress and Drupal provide almost exactly the same guidelines.</p>
<p>Remarkably, certain members of the WordPress community completely ignore the advice on the licensing page. They claim “<a href="http://hackadelic.com/the-gpl-faq-has-no-legal-validity">the GPL FAQ has no legal validity</a>.” Some sellers proudly declare, “<a href="http://wprocks.com/2009/07/15/my-wordpress-themes-are-not-gpl-got-my-own-licensing-terms/">got my own licensing terms</a>” and others offer code with <a href="http://docs.easydigitaldownloads.com/article/942-terms-and-conditions">terms and conditions</a> that make no mention of the GPL. <a href="https://ivycat.com/a-newbies-guide-to-the-gpl-and-wordpress-licensing/">Some explain the norm thusly</a>: “Premium WordPress plugin and theme developers usually sell you a license key, which allows you access to support and automatic upgrades.” One store believes it takes advantage of GPL loopholes and sells plugins under a <a href="https://help.market.envato.com/hc/en-us/articles/202501064-What-is-Split-Licensing-and-the-GPL-">default split license</a> ostensibly to “protect authors.” In other words, the WordPress.org <a href="https://wordpress.org/plugins/">plugin directory</a> that lists over 40,000 plugins is not a comprehensive directory of WordPress plugins. If this sounds a bit like the Wild West, then you may be a Drupal developer.</p>
<p>If Drupal modules are about process, then WordPress plugins – at least a portion of them – are products. Members of the WordPress community write at length about the benefits of “<a href="https://chrislema.com/gpl-themes-plugins/">premium products</a>.” <a href="https://premium.wpmudev.org/projects/category/plugins/">Some stores</a> offer plugins that are “guaranteed to work, always updated, top quality plugins. Beautifully coded, packed with features, and easy to use.” They offer “free trials.” <a href="https://proplugindirectory.com/about/">Another store</a> proudly trumpets its “professionally developed and supported options not found on WordPress.org.” Ventures of this nature are justified with such syllogisms as “<a href="https://yoast.com/on-the-gpl-themes-plugins-free/">‘free’ doesn’t make me rich</a>.” Plugins like these are products, plain and simple, and clearly the WordPress community (“<a href="http://www.elegantthemes.com/plugins/">happy customers</a>,” as one site put it) willingly pays.</p>
<p>In comparison, Drupal developers get modules from Drupal.org. More than just a “best practice,” this is the norm in the Drupal community. It keeps things simple, and has a practical benefit: acquiring a new module requires typing <code>drush dl module-name</code>.</p>
<p>Incidentally, another kind of software facilitates an analogous workflow: GNU/Linux. For example, to install software on my operating system, Debian, I type <code>apt install package_name</code>. I would never consider downloading software to my computer from some random website, let alone software that I could not inspect. For me, at least, these two processes (<code>drush dl</code> and <code>apt get</code>) feel nearly identical, and I could make a good argument that the Drupal community is more like the Debian community (i.e., that many of the commitments outlined the <a href="https://www.debian.org/social_contract">Debian Social Contract</a> are lived in the Drupal community) than the WordPress community.</p>
<p>Once again, the words of Benjamin feel relevant: “It might be stated as a general formula that the technology of reproduction detaches the reproduced object from the sphere of tradition. By replicating the work many times over, it substitutes a mass existence for a unique existence.” App stores for paid plugins fragment traditions established on WordPress.org. The Drupal community, on the other hand, has decided not to purchase paid Drupal modules, not to depart from its tradition of keeping Drupal as close to 100% free as possible. Whenever this issue arises, the Drupal community votes with its voices and its wallets to favor practices over products, to <em>reveal</em> the modules it creates rather than conceal them behind paywalls, to work with – rather than sell to – each other. The fact that WordPress customers have chosen a different path reveals contrasting, not misplaced, priorities.</p>
<p>While it is difficult to generalize a community with more than a million users (or at least user accounts), Holly Ross, Executive Director of the Drupal Association, <a href="https://assoc.drupal.org/blog/holly.ross.drupal/measuring-drupals-health">believes</a> “the one thing we all have in common is that we care about the fate of Drupal.” It might be fate that someday paid modules will find success in the Drupal community, and that would not necessarily be wrong. It would mean, however that the <em>essence</em> of Drupal has changed. That may even signal the end of Drupal. But history suggests that day will not soon come. Until then, the Drupal community will continue to defend its practices. It will band together to resist paid module schemes, treat its software with a reverence that others reserve for works of art, share code, and encourage others to do the same.</p>
<p><strong>Works Cited</strong></p>
<p>Benjamin, Walter. <em>The Work of Art in the Age of Its Technological Reproducibility, and Other Writings on Media</em>. Edited by Michael W. Jennings, Brigid Doherty, and Thomas Y. Levin. Cambridge, Mass: Belknap Press, 2008.</p>
<p>Heidegger, Martin. <em>The Question Concerning Technology and Other Essays</em>. New York: Harper Perennial, 1977.</p>
<p>Jameson, Fredric. <em>The Political Unconscious: Narrative as a Socially Symbolic Act</em>. Cornell University Press, 1982.</p>
<p>Jameson, Fredric. <em>Postmodernism, or, The Cultural Logic of Late Capitalism</em>. Duke University Press, 1992.</p>
  
  <div class="prev-next">
  
  <a href=".././accelerated-mobile-pages-amp-open-or-closed/" rel="previous">← Previous Post</a>
  
  
  <a href=".././who-sponsors-drupal-development/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
