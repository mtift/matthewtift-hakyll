<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - Drupal Core Development and Fundraising</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>Drupal Core Development and Fundraising</h1>
    <div class="post">

  <div class="info">
    Posted on February 17, 2014
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/Drupal/">Drupal</a>, <a href="../tags/fundraising/">fundraising</a>, <a href="../tags/drupal-planet/">drupal planet</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p>Over the past couple of years, the Drupal community has been hard at work trying to confront a variety of challenges concerning <a href="https://drupal.org/contribute/development">Drupal core development</a>, especially issues like <a href="http://london2011.drupal.org/coreconversation/1700-1730-burnout">burnout</a> and <a href="https://www.lullabot.com/blog/podcasts/drupalizeme-podcast/17-funding-drupal-core-development">funding</a>. People like <a href="http://heyrocker.com/funding-cmi">heyrocker</a>, <a href="http://yesct.net/targeting">YesCT</a>, and <a href="http://alex.vit-al.com/node/16">Alex Pott</a> have been especially outspoken. I have been paying close attention to these discussions as well as engaging in conversations with some of the key individuals involved.</p>
<p><a href="broadcasting-bot">Until last week</a>, I worked in the nonprofit sector and I have been struck by the similarities between code contributors to Drupal core and the monetary contributions to nonprofit organizations. I do not think it is a coincidence that that nonprofit fundraising departments are also called <em>development</em> departments or that both Drupal and nonprofits require <em>contributions</em>.</p>
<p>What follows is a broad discussion of Drupal core development through the lens of current fundraising practices. It will unfortunately gloss over the important contributions of countless individuals and organizations, especially in the contributed module space. It is my hope that through this rubric we might develop an increased appreciation of the current state of <a href="https://drupal.org/node/717162">Drupal core development</a>.</p>
<h2 id="membership">Membership</h2>
<p>For many nonprofits, individual gifts are the lifeblood of the organization and collectively are often the largest single source of revenue.</p>
<p>Drupal’s “membership department” (my phrase, not theirs) consists of individuals like <a href="https://drupal.org/user/258568">YesCT</a>, <a href="https://drupal.org/user/1167326">Cottser</a>, <a href="https://drupal.org/user/65776">xjm</a>, and <a href="https://drupal.org/user/226976">ZenDoodles</a>. These people take it upon themselves to help other people contribute to Drupal core by organizing frequent code sprints, helping folks climb the <a href="http://drupalladder.org/">Drupal ladder</a>, answering questions and offering assistance during <a href="https://drupal.org/core-office-hours">core office hours</a>, and much more. Like many of the people I know that work in nonprofit membership, these people often project a seemingly endless supply of patience.</p>
<p>Much like many nonprofits that are shifting their strategy from getting one-time contributors to attracting sustainers – that contribute monthly donations – the concerted efforts to grow the Drupal community of core contributors is succeeding. Not only is the number of <a href="http://ericduran.github.io/drupalcores/index.html">contributors to Drupal 8</a> already more than twice the number that <a href="http://www.knaddison.com/drupal/contributors-drupal-7-final-numbers">contributed code to Drupal 7</a>, the number of people who have contributed more than one patch to Drupal 8 has also grown considerably.</p>
<h2 id="corporate-support">Corporate Support</h2>
<p>For nonprofits, corporate support can sometimes mean getting nice things, such a new building or an endowed professorship. Corporations like to be recognized for their contributions.</p>
<p>In Drupal, it would seem that we are rather healthy in this area, too. We have created a situation in which companies want to contribute back. In the contributed module space, for example, we have nice things like <a href="https://drupal.org/project/commerce">Drupal Commerce</a>, <a href="https://drupal.org/project/workbench">Workbench</a>, <a href="https://drupal.org/project/backup_migrate">Backup and Migrate</a>, and so much more. We also have numerous companies that encourage their employees to work on Drupal for a certain percentage of their time each week. Further, we have a somewhat unique situation with Acquia’s <a href="http://www.acquia.com/blog/next-steps-how-acquia-plans-give-back-drupal-%208-q4">Office of the CTO (OCTO)</a>, which, with the involvement of <a href="http://buytaert.net">Drupal’s project lead</a>, helps tremendously in pushing Drupal core development forward.</p>
<p>I would guess that much of this corporate support in code comes from the Drupal Association’s excellent work in <a href="https://association.drupal.org/advertising">securing financial contributions</a>. However, because the Drupal Association <a href="https://association.drupal.org/about">has no authority over the planning, functionality and development of the Drupal software</a>, their involvement in this area is necessarily limited.</p>
<h2 id="major-giving">Major Giving</h2>
<p>Nowadays, nonprofits are increasingly involved in cultivating relationships with major donors. They are called “major gifts” because they have a major influence on an organization’s ability to succeed (or not). For example, when I worked for the Minnesota Orchestra, I was shocked to find out that a single donor would pay for all of the costs to send the orchestra on a tour of Europe.</p>
<p>In Drupal, we have people like <a href="https://drupal.org/user/99340">Daniel Wehner</a>, <a href="https://drupal.org/user/241634">Tim Plunkett</a>, and <a href="http://ericduran.github.io/drupalcores">many others</a> that devote significant time and code to Drupal core. We have <a href="https://drupal.org/user/157725">Alex Pott</a>, who will forever hold a place in Drupal lore as the guy who <a href="http://buytaert.net/alex-pott">quit his job to work full time on Drupal</a>.</p>
<p>In many nonprofits the role of the top executive is increasingly becoming that of chief fundraiser. Presidents, Executive Directors, and General Managers are just as likely, if not more likely, to be experts in finance or fundraising as they are experts in their organizational subject matter.</p>
<p>This is one of the areas where my analytical tool begins to break down. For Drupal, the equivalent would be for Dries to be spending his time flying around the world, taking top contributors out to lunch to talk about the future of the Drupal project. While that model proves successful for nonprofits, it hardly seems like a good use of Dries’s time. Alex Pott, for example, is a visionary leader in his own right and does not need to be cultivated. Alex needs people to help him reach his <a href="https://www.gittip.com/alexpott/">goal on Gitttip</a>.</p>
<p>Overall, it seems like the Drupal community recognizes the contributions of its major givers and does its best to take care of them. When <a href="https://drupal.org/user/39567">yched’s</a> laptop started melting, the community <a href="https://www.drupalfund.us/project/funding-yched-complete-d8-entity-field-api">bought him a new one</a>. DrupalCons and numerous DrupalCamps, such as BADCamp and <a href="http://tcdrupal.org">Twin Cities Drupal Camp</a> (TCDC), bring core contributors together in real life through scholarships and other support. (I like to brag that Views in Drupal Core was <a href="http://xjm.drupalgardens.com/blog/drupalcamp-twin-cities-views-drupal-core">born at TCDC</a>.) We are also starting to see some success with <a href="https://www.drupalfund.us/">Drupalfund</a> and the <a href="https://www.gittip.com/for/drupal">Drupal community on Gittip</a>.</p>
<h2 id="parting-thoughts">Parting Thoughts</h2>
<p>While my goal here has been to re-imagine Drupal contributions rather than to solve some of the very real problems we face, I cannot help but wonder if some folks have already figured out the magic formula: patronage. This formula allows organizations to get what they need while supporting Drupal core development. While not perfect, the patronage system worked for the <a href="http://en.wikipedia.org/wiki/House_of_Esterh%C3%A1zy">Esterházy family</a> and <a href="http://en.wikipedia.org/wiki/Joseph_Haydn">Haydn</a>, the <a href="http://en.wikipedia.org/wiki/House_of_Medici">Medici’s</a> and <a href="http://en.wikipedia.org/wiki/Davinci">Da Vinci</a>, and countless others. Nowadays, we have Stanford Univeristy and Tim Plunckett. Tim’s employer requires him to work on Drupal sites, while also giving him a generous amount of time to work on Drupal core. His employer benefits, Drupal benefits, and Tim benefits.</p>
<p>Even if patronage is not the answer, then I think we will benefit by thinking about Drupal core like nonprofits approach fundraising. We need to grow contributors and do whatever it takes to keep folks coming back for more. We need to make it beneficial for businesses to support work on Drupal core and recognize those contributions. And we need to keep our major givers happy.</p>
<p><em>P.S. If you found this article helpful, please consider looking at these two issues that <a href="https://drupal.org/node/2187339">I’m responsible for</a> that will help get CMI to beta: <a href="https://drupal.org/node/2108813" class="uri">https://drupal.org/node/2108813</a> and <a href="https://drupal.org/node/2148199" class="uri">https://drupal.org/node/2148199</a>.</em></p>
  
  <div class="prev-next">
  
  <a href=".././broadcasting-bot/" rel="previous">← Previous Post</a>
  
  
  <a href=".././introducing-hacking-culture/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
