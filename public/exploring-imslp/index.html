<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - Exploring IMSLP</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>Exploring IMSLP</h1>
    <div class="post">

  <div class="info">
    Posted on August 21, 2012
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/open-source-music/">open source music</a>, <a href="../tags/IMSLP/">IMSLP</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p>Over the weekend, I was sitting in my office watching an episode about <a href="http://video.pbs.org/video/1295282213">Beethoven’s 3rd (“Eroica”) symphony</a> from the excellent PBS series <a href="http://www.keepingscore.org/">Keeping Score</a>, when Michael Tilson Thomas said something about the symphony that intrigued me. I could not easily locate my copy of the orchestral score, so rather than continue my search in another room, I did something that I find myself doing more and more these days – searching on the International Music Score Library Project (<a href="http://imslp.org/">IMSLP</a>) website. As often happens on IMSLP, which is also known as the Petrucci Library, I found <a href="http://imslp.org/wiki/Symphony_No.3,_Op.55_(Beethoven,_Ludwig_van)">exactly what I wanted</a>.</p>
<p>Don’t get me wrong. The Keeping Score project is excellent. The site has some fancy tools for exploring excerpts from the score of the Eroica, complete with audio and visual guides. It’s quite impressive.</p>
<p>But if you want to listen to a complete recording of the Eroica and follow along with a score, IMSLP has all that you need. Not only does IMSLP have a score of the Eroica, it has <a href="http://imslp.org/wiki/Symphony_No.3,_Op.55_(Beethoven,_Ludwig_van)">seven different versions</a> of the full score (from as early as 1809), as well as various parts, arrangements, and transcriptions. It also has a number of recordings, not from Leonard Bernstein conducting the New York Philharmonic, but from the Czech National Symphony Orchestra.</p>
<p>The scores and recordings on IMSLP are free and legal. This Wikipedia-inspired site allows users to upload <a href="http://imslp.org/wiki/Public_domain">public domain scores</a>, which can be freely distributed. While copyright laws differ from country to country, in the United States works published before 1923 are public domain. In other words, IMSLP hosts more than 100,000 scores by composers such as <a href="http://imslp.org/wiki/Category:Bach,_Johann_Sebastian">Bach</a>, <a href="http://imslp.org/wiki/Category:Beethoven,_Ludwig_van">Beethoven</a>, and <a href="http://imslp.org/wiki/Category:Brahms,_Johannes">Brahms</a>, rather than copyrighted works by contemporary composers. If you are looking for more information about the legality of this project, the New York Times published <a href="http://www.nytimes.com/2011/02/22/arts/music/22music-imslp.html">a helpful article</a> that explains more.</p>
<p>I can think of at least five different uses for IMSLP that might not be readily apparent:</p>
<ol type="1">
<li><strong>Find a score</strong>. In addition to searching for a score, IMSLP also provides a <a href="http://imslpforums.org/">forum</a> where users can request a score be added to the collection, request new features on the IMSLP website, and much more. One person (self-described as “not a musician”) shared <a href="http://imslpforums.org/viewtopic.php?f=35&amp;t=6332">on the forum</a> how seeing the scores from IMSLP facilitated a new appreciation of Mozart’s music.</li>
<li><strong>Remix a recording</strong>: For those who were inspired by TTBOOK’s excellent episode on <a href="http://ttbook.org/book/remix-culture">remix culture</a> or Radiolab’s <a href="http://www.indabamusic.com/opportunities/wnyc-radiolab-remix-contest">remix contest</a> that would like to try remixing, but would prefer to use legal recordings, IMSLP is a great place to find all kinds of sounds.</li>
<li><strong>Remix a score</strong>: If you want to be like Jad Abumrad, and do your own <a href="http://www.in-c-remixed.com/">remix of Terry Riley’s <em>In C</em></a>, you can find a copy of the score for <a href="http://imslp.org/wiki/In_C_(Riley,_Terry)"><em>In C</em> on IMSLP</a> to get started.</li>
<li><strong>Learn</strong>. In addition to hosting scores, recordings, and a forum, there is also a free <a href="http://imslpjournal.org/">IMSLP Journal</a>, which contains articles intended for a general audience, and without too much music jargon.</li>
<li><strong>Be romantic</strong>: For those less musical, a sweet way to say “I love you” is to print out an original score of someone’s favorite music and use it as wrapping paper for a small gift.</li>
</ol>
<p>In the final decades of the 20th century, music scholars grew increasingly incredulous toward grand narratives that featured “great works by great men.” With the advent of the Web – and tools such as IMSLP – the work of disrupting these grand narratives can be opened up to those without the academic credentials of a music scholar. Access to online <a href="http://imslp.org/wiki">music libraries</a>, <a href="http://www.doaj.org/doaj?func=subject&amp;cpid=6">journals</a>, <a href="http://www.digitalculture.org/hacking-the-academy/">books</a>, and other information is increasing. While it was possible in the past, websites such as IMSLP make it a lot easier and invite everyone to participate.</p>
<p>If you do happen to find something interesting on IMSLP, and you create a free blog on a site such as <a href="http://www.drupalgardens.com/">Drupal Gardens</a> or <a href="http://wordpress.org/">Wordpress</a>, IMSLP <a href="http://imslpjournal.org/are-you-a-classical-music-or-culture-blogger/">wants to know about it</a> and might even feature it <a href="http://imslpjournal.org/">on their website</a>.</p>
  
  <div class="prev-next">
  
  <a href=".././bach-meets-high-tech-wisconsin-public-radio-broadcast-experiment/" rel="previous">← Previous Post</a>
  
  
  <a href=".././drupal-development-osx-mamp-and-solr/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
