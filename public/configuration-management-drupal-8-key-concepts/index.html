<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - Configuration Management in Drupal 8: The Key Concepts</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>Configuration Management in Drupal 8: The Key Concepts</h1>
    <div class="post">

  <div class="info">
    Posted on November  4, 2015
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/Drupal/">Drupal</a>, <a href="../tags/configuration-system/">configuration system</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p><em>(This article was cross-posted on <a href="https://www.lullabot.com/articles/configuration-management-in-drupal-8-the-key-concepts">lullabot.com</a>.)</em></p>
<p>While the <a href="https://api.drupal.org/api/drupal/core%21core.api.php/group/config_api/8">new configuration system in Drupal 8</a> strives to make the process of exporting and importing site configuration feel almost effortless, immensely complex logic facilitates this process. Over the past five years, the entire configuration system code was written and rewritten multiple times, and we think we got much of it right in its present form. As a result of this work, it is now possible to store configuration data in a consistent manner and to manage changes to configuration. Although we made every attempt to document how and why decisions were made – and to always update <a href="https://www.drupal.org/project/issues/search/drupal?text=&amp;assigned=&amp;submitted=&amp;project_issue_followers=&amp;status%5B0%5D=Open&amp;status%5B1%5D=1&amp;status%5B2%5D=13&amp;status%5B3%5D=8&amp;status%5B4%5D=14&amp;status%5B5%5D=15&amp;status%5B6%5D=2&amp;status%5B7%5D=4&amp;status%5B8%5D=16&amp;status%5B9%5D=3&amp;status%5B10%5D=5&amp;status%5B11%5D=6&amp;status%5B12%5D=18&amp;status%5B13%5D=7&amp;&amp;&amp;version%5B0%5D=8.x&amp;component%5B0%5D=configuration%20entity%20system&amp;component%5B1%5D=configuration%20system&amp;issue_tags_op=%3D&amp;issue_tags=">issue queues</a>, <a href="https://www.drupal.org/node/1667894">documentation</a>, and <a href="https://www.drupal.org/list-changes/published?keywords_description=configuration&amp;to_branch=&amp;version=&amp;created_op=%3E%3D&amp;created%5Bvalue%5D=&amp;created%5Bmin%5D=&amp;created%5Bmax%5D=">change notices</a> – it is not reasonable to expect everyone to read all of this material. But I did, and in this post I try to distill years of thinking, discussions, issue summaries, code sprints, and code to ease your transition to Drupal 8.</p>
<p>In this article I highlight nine concepts that are key to understanding the configuration system. This article is light on details and heavy on links to additional resources.</p>
<ol type="1">
<li><strong>It is called the “configuration system.”</strong> The Configuration Management Initiative (CMI) is, by most reasonable measures, feature complete. The number of <a href="https://www.drupal.org/project/issues/search/drupal?project_issue_followers=&amp;status%5B%5D=Open&amp;priorities%5B%5D=400&amp;version%5B%5D=8.x&amp;component%5B%5D=configuration+entity+system&amp;component%5B%5D=configuration+system&amp;issue_tags_op=%3D">CMI critical issues</a> was reduced to zero back in the Spring and the <a>#drupal-cmi</a> IRC channel has been very quiet over the past few months. Drupal now has a functional configuration management system, but we only should call the former a CMS. While it is tempting to think of “CMI” as an <a href="https://en.wikipedia.org/wiki/Acronym#Pseudo-acronyms">orphaned initialism</a>, like AARP or SAT, we aspire to avoid confusion. Our preferred phrase to describe the <em>result</em> of CMI is “configuration system.” This is the phrase we <a href="https://www.drupal.org/project/issues/search/drupal?project_issue_followers=&amp;status%5B%5D=Open&amp;version%5B%5D=8.x&amp;component%5B%5D=configuration+system&amp;issue_tags_op=%3D">use in the issue queue</a> and <a href="https://www.drupal.org/documentation/administer/config">the configuration system documentation</a>.</li>
<li><strong>DEV ➞ PROD.</strong> Perhaps the most important concept to understand is that the configuration system is designed to optimize the process of moving configuration <em>between instances of the same site</em>. It is not intended to allow exporting the configuration from one site to another. In order to move configuration data, the site and import files must have matching values for UUID in the system.site configuration item. In other words, additional environments should initially be set up as clones of the site. We did not, for instance, hope to facilitate exporting configuration from <a href="http://whitehouse.gov">whitehouse.gov</a> and importing it into <a href="http://harvard.edu">harvard.edu</a>.</li>
<li><strong>The configuration system is highly configurable.</strong> Out of the box the configuration system stores configuration data in the database. However, it allows websites to easily switch to <a href="https://www.drupal.org/node/2416555">file-based storage</a>, MongoDB, Redis, or another favorite key/value store. In fact, there is a growing ecosystem of modules related to the configuration system, such as <a href="https://www.drupal.org/project/config_update">Configuration Update</a>, <a href="https://www.drupal.org/project/config_tools">Configuration Tools</a>, <a href="https://www.drupal.org/project/config_sync">Configuration Synchronizer</a>, and <a href="https://www.drupal.org/project/config_devel">Configuration Development</a>.</li>
<li><strong>There is no “recommended” workflow.</strong> The configuration system is quite flexible and we can imagine multiple workflows. On one end of the spectrum, we expect some small sites will not ever use the configuration manager module to import and export configuration. For the sites that utilize the full capabilities of the configuration system, one key question they will need to answer regards the role that site administrators will play in managing configuration. I suspect many sites will disable configuration forms on their production sites – perhaps using modules like <a href="https://www.drupal.org/project/config_readonly">Configuration Read-Only Mode</a> – and make all configuration changes in their version control system.</li>
<li><strong>Sites, not modules, own configuration.</strong> When a module is installed, and the configuration system imports the configuration data from the module’s <a href="https://www.drupal.org/node/2234799">config/install</a> directory (and perhaps the <a href="https://www.drupal.org/node/2453919">config/optional</a> directory), the configuration system assumes the site owner is now in control of the configuration data. This is a contentious point to some developers because module maintainers will need to use <a href="https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Extension!module.api.php/function/hook_update_N/8">update hooks</a> rather than making simple changes to their configuration. Changing the files in a module’s config/install directory after the module has been installed will have no effect on the site.</li>
<li><strong>Developers will still use Features.</strong> The <a href="https://www.drupal.org/project/features">Features module in Drupal 8</a> changes how the configuration system works to allow modules to control their configuration. Mike Potter, Nedjo Rogers, and others have been making Features in Drupal 8 do the kinds of things Features was originally intended to do, which is to bundle functionality, such as a “photo gallery feature.” The configuration system makes the work of the Features module maintainers exponentially easier and as a result, we all expect using Features to be more enjoyable in Drupal 8 than it was in Drupal 7.</li>
<li><strong>There are two kinds of configuration in Drupal 8: simple configuration and configuration entities.</strong> <a href="https://www.drupal.org/node/1809490">Simple configuration</a> stores basic configuration, such as boolean values, integers, or texts. Simple configuration has exactly one copy or version, and is somewhat similar to using <a href="https://api.drupal.org/api/drupal/includes%21bootstrap.inc/function/variable_get/7">variable_get()</a> and <a href="https://api.drupal.org/api/drupal/includes%21bootstrap.inc/function/variable_get/7">variable_set()</a> in Drupal 7. <a href="https://www.drupal.org/node/1809494">Configuration entities</a>, like <a href="https://www.drupal.org/node/2192175">content types</a>, enable the creation of zero or more items and are far more complex. Examples of configuration entities include <a href="https://www.drupal.org/node/1912118">views</a>, <a href="https://www.drupal.org/node/2192175">content types</a>, and <a href="https://api.drupal.org/api/drupal/core%21modules%21image%21src%21Entity%21ImageStyle.php/class/ImageStyle/8">image styles</a>.</li>
<li><strong>Multilingual needs drove many decisions.</strong> Many of the features of the configuration system exist to support multilingual sites. For example, the primary reason <a href="https://www.drupal.org/node/1905070">schema files</a> were introduced was for multilingual support. And many of the benefits to enabling multilingual functionality resulted in enhancements that had much wider benefits. The multilingual initiative was perhaps the best organized and documented Drupal 8 initiative and their <a href="http://www.drupal8multilingual.org/">initiative website</a> contains extensive information and documentation.</li>
<li><strong>Configuration can still be overridden in settings.php.</strong> The $config variable in the <a href="https://api.drupal.org/api/drupal/sites!default!default.settings.php/8">settings.php</a> file provides a mechanism for overriding configuration data. This is called the <a href="https://www.drupal.org/node/1928898">configuration override system</a>. Overrides in settings.php take precedence over values provided by modules. This is a good method for storing sensitive data that should not be stored in the database. Note, however, that the values in the active configuration – not the values from settings.php – are displayed on configuration forms. Of course, this behavior can be modified to match expected workflows. For example, some site administrators will want the configuration forms to indicate when form values are overridden in settings.php.</li>
</ol>
<p>If you want more information about the configuration system, the best place to start is the <a href="https://api.drupal.org/api/drupal/core%21core.api.php/group/config_api/8">Configuration API</a> page on <a href="https://api.drupal.org/api/drupal/8">api.drupal.org</a>. It contains numerous links to additional documentation. Additionally, <a href="https://www.drupal.org/u/alexpott">Alex Pott</a>, my fellow configuration system co-maintainer, wrote a <a href="https://www.chapterthree.com/blog/principles-of-configuration-management-part-one">series</a> of <a href="https://www.chapterthree.com/blog/principles-of-configuration-management-part-two">blog</a> <a href="https://www.chapterthree.com/blog/principles-of-configuration-management-part-three">posts</a> concerning the “Principles of Configuration Management” that I enthusiastically recommend.</p>
<p>I hope you will agree that the configuration system is one of the more exciting features of Drupal 8.</p>
<p><em>This article benefitted from helpful conversations and reviews by <a href="https://www.drupal.org/u/tim.plunkett">Tim Plunkett</a>, <a href="https://www.drupal.org/u/jhodgdon">Jennifer Hodgdon</a>, <a href="https://www.drupal.org/u/deviantintegral">Andrew Berry</a>, and <a href="https://www.drupal.org/u/juampynr">Juampy NR</a>.</em></p>
  
  <div class="prev-next">
  
  <a href=".././better-then-bigger-cultivating-drupal-community/" rel="previous">← Previous Post</a>
  
  
  <a href=".././amping-drupal/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
