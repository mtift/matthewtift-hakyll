<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - What I Learned about Drupal at a Go/Rust User Group</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>What I Learned about Drupal at a Go/Rust User Group</h1>
    <div class="post">

  <div class="info">
    Posted on April 30, 2015
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/Drupal/">Drupal</a>, <a href="../tags/drupal-planet/">drupal planet</a>, <a href="../tags/go/">go</a>, <a href="../tags/rust/">rust</a>, <a href="../tags/community/">community</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p>Last night I had the pleasure of attending the first joint meeting of <a href="http://www.meetup.com/golangmn/events/221703765">Minnesota Go and Rust users</a>, and arrived home inspired – not just by Go and Rust, but also about Drupal and its community.</p>
<p>Attending a wide variety of local meetups is something I really enjoy. Since I moved back to the Twin Cities area nearly seven years ago, I have attended meetings related to <a href="http://www.meetup.com/mn-php">PHP</a>, <a href="http://www.mn-linux.org">Linux</a>, <a href="http://www.meetup.com/Concrete5-Minnesota">Concrete5</a>, <a href="https://www.joyent.com/noderoad/cities/minneapolis-6-17-2014">NodeJS</a>, <a href="http://www.meetup.com/tcdnug">.NET</a>, <a href="http://www.meetup.com/JavaScriptMN">JavaScript</a>, <a href="processing-processing">Flash</a>, and more. Each of these groups has a unique community and way of doing things, although just about every one of them involves pizza, beer, and predominantly white, male participants.</p>
<p>Given the fact that Rust and Go are both C-like low-level languages, it might not come as a surprise that this Go/Rust user group meeting was quite technical. There were three presentations and lots of terminals on the big screen. The first speaker introduced Rust by illustrating the process for generating the Fibonacci sequence. The end result looked something like this:</p>
<pre><code>struct Fibonacci {
   curr: u32,
   next: u32,
}

impl Iterator for Fibonacci {
   type Item = u32;
   fn next(&amp;mut self) -&gt; Option {
       let new_next = self.curr + self.next;

       self.curr = self.next;
       self.next = new_next;

       Some(self.curr)
   }
}

fn fibonacci() -&gt; Fibonacci {
   Fibonacci { curr: 1, next: 1 }
}

fn main() {
   for i in fibonacci().take(30) {
       println!(&quot;{}&quot;, i);
   }
}</code></pre>
<p>At other local meetups I have attended, this could have made for a dry, boring talk. However, at this Go/Rust meetup, each aspect of this Fibonacci program was followed by remarkably sophisticated audience questions and answers about language architecture, Rust’s underlying assumptions, and frequent references to how Rust compared to C, Python, Go, and other languages. I learned a lot and found it very entertaining. (Incidentally, while I personally prefer Go, I think this is a great article comparing <a href="http://jaredly.github.io/2014/03/22/rust-vs-go/index.html">Go and Rust</a> – the author likes Rust).</p>
<p>I have attended a lot of DrupalCon and DrupalCamp sessions over the past five years or so, and I don’t recall any of them feeling like this Go/Rust meetup. Perhaps there are a lot of more technical sessions and I just avoided them. Perhaps sessions like the (Drupal) Core Conversations are very technical, but I just don’t notice it as much. Whatever the case, these Rust and Go talks got me thinking about Drupal and its community.</p>
<p>Drupal meetings, especially in the <a href="https://groups.drupal.org/twin-cities">Twin Cities</a>, tend to be focused on bringing in new users and not leaving anyone out of the conversations. That often means less technical presentations. Furthermore, every year at the Twin Cities DrupalCamp we have made it an explicit goal to welcome new users into our community.</p>
<p><a href="http://www.garfieldtech.com/blog/off-the-island-2013">Getting off the Drupal island</a> to go to this Go/Rust group was a nice reminder of just how much Drupal lets me do without ever having to think about these low-level issues. For comparison, in PHP (Drupal is written in PHP), we might do something more simpler looking to create the Fibonacci series:</p>
<pre><code>$fib = [1,0];
for ($i = 0; $i &lt; 30; $i++) {
 $next = array_sum($fib);
 array_shift($fib);
 array_push($fib,$next);
 echo $next.', ';
}</code></pre>
<p>Or perhaps more relevant is the fact that I can just spin up a Drupal blog for one of my friends or one of my kids, easily download themes to radically change the appearance, and quickly add sophisticated integrations without writing code. And at that point get on with my hacking, or not. My code, my data, my content. Sometimes I lose track of that when I’m working on a project with teams of people I’ve never met, testing my code in ways I never would have anticipated, making spectacularly complex, cool websites.</p>
<p>The Go/Rust meetup had the unexpected effect of reminding me that Drupal can be very laid-back and non-technical, and that the web can be the same. Not all websites will need the sophistication of Drupal 8’s <a href="http://drupal8cmi.org">fancy new configuration system</a>, and its ability to facilitate development, testing, and production environments. Drupal allows me to come home, emboldened by a meetup, and quickly jot down my unfiltered, half-baked thoughts for all the world to see, without having to think about the complexities of immutable references or garbage collection. This Drupal site lets me spill out these ideas in short order.</p>
<p>As Drupal becomes an increasingly capable and accommodating entrance onto the wonderfully diverse information superhighway, it’s nice to be reminded once in a while of the fact that Drupal also can do a darn good job of hiding complexity and letting us get on with creating and managing our content of all shapes, sizes, and qualities.</p>
  
  <div class="prev-next">
  
  <a href=".././introducing-hacking-culture/" rel="previous">← Previous Post</a>
  
  
  <a href=".././cultural-construction-drupal/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
