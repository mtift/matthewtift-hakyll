<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - A Security Checklist for Drupal 8 Sites with Private Data</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>A Security Checklist for Drupal 8 Sites with Private Data</h1>
    <div class="post">

  <div class="info">
    Posted on February 21, 2019
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/Drupal/">Drupal</a>, <a href="../tags/security/">security</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p><em>(This article was cross-posted on <a href="https://www.lullabot.com/articles/security-checklist-drupal-8-sites-private-data">lullabot.com</a>.)</em></p>
<p>Drupal has a great reputation as a CMS with excellent security standards and a 30+ member <a href="https://security.drupal.org/team-members">security team</a> to back it up. For some Drupal sites, we must do more than just keep up-to-date with each and every security release. A Drupal site with private and confidential data brings with it some unique risks. Not only do you want to keep your site accessible to you and the site’s users, but you also cannot afford to have private data stolen. This article provides a checklist to ensure the sensitive data on your site is secure.</p>
<!--more-->
<p>Keeping a Drupal 8 site secure requires balancing various needs, such as performance, convenience, accessibility, and resources. No matter how many precautions we take, we can never guarantee a 100% secure site. For instance, this article does not cover vulnerabilities beyond Drupal, such as physical vulnerabilities or team members using <a href="https://www.lullabot.com/articles/eight-reasons-why-security-matters-for-distributed-agencies">unsafe hardware, software, or networks</a>. These suggestions also do not ensure <a href="https://www.drupal.org/project/drupal_gdpr_team">General Data Protection Regulation (GDPR) compliance</a>, a complex subject that is both important to consider and beyond the scope of this checklist.</p>
<p>Rather than systems administrators well-versed in the minutia of securing a web server, the list below targets Drupal developers who want to advise their clients or employers with a reasonable list of recommendations and implement common-sense precautions. The checklist below should be understood as a thorough, though not exhaustive, guide to securing your Drupal 8 site that contains private data, from development to launch and after.</p>
<p>The information below comes from various sources, including the security recommendations on Drupal.org, articles from <a href="https://www.drupal.org/planet/">Drupal Planet</a> that discuss security, a Drupal security class I took with Gregg Knaddison and his 2009 book <a href="http://crackingdrupal.com/"><em>Cracking Drupal</em></a>, a review a numerous internal Lullabot documents regarding security, and other sources. This list was reviewed by Lullabot’s security team. That said, for more comprehensive content and considerations, I recommend learning more about <a href="https://www.drupal.org/security/secure-configuration">securing your site</a> on Drupal.org and other security sources.</p>
<h2 id="infrastructure">Infrastructure</h2>
<ul>
<li>Configure all servers and environments to use <a href="https://www.lullabot.com/articles/https-everywhere-security-is-not-just-for-banks">HTTPS</a>.</li>
<li>Install the site following the Drupal guidelines to <a href="https://www.drupal.org/node/244924">secure file permissions and ownership</a> (using the <code>fix-permissions.sh</code> included with the guidelines is a quick way to do this).</li>
<li>Hide important core files that may allow attackers to identify the installed version (the point release) of Drupal, install new sites, update existing sites, or perform maintenance tasks. For example, when using Apache add something like this to your .htaccess configuration file:</li>
</ul>
<div class="sourceCode" id="cb1"><pre class="sourceCode bash"><code class="sourceCode bash"><a class="sourceLine" id="cb1-1" title="1"><span class="op">&lt;</span><span class="ex">FilesMatch</span> <span class="st">&quot;(MAINTAINERS|INSTALL|INSTALL.mysql|CHANGELOG).txt&quot;</span><span class="op">&gt;</span></a>
<a class="sourceLine" id="cb1-2" title="2">  <span class="ex">Order</span> deny,allow</a>
<a class="sourceLine" id="cb1-3" title="3">  <span class="ex">deny</span> from all</a>
<a class="sourceLine" id="cb1-4" title="4">  <span class="ex">Allow</span> from 127.0.0.1  <span class="op">&lt;</span>-- your domain goes here</a>
<a class="sourceLine" id="cb1-5" title="5"><span class="op">&lt;</span>/<span class="ex">FilesMatch</span><span class="op">&gt;</span></a>
<a class="sourceLine" id="cb1-6" title="6"><span class="op">&lt;</span><span class="ex">FilesMatch</span> <span class="st">&quot;(authorize|cron|install|update).php&quot;</span><span class="op">&gt;</span></a>
<a class="sourceLine" id="cb1-7" title="7">  <span class="ex">Order</span> deny,allow</a>
<a class="sourceLine" id="cb1-8" title="8">  <span class="ex">deny</span> from all</a>
<a class="sourceLine" id="cb1-9" title="9">  <span class="ex">Allow</span> from 127.0.0.1  <span class="op">&lt;</span>-- your domain goes here</a>
<a class="sourceLine" id="cb1-10" title="10"><span class="op">&lt;</span>/<span class="ex">FilesMatch</span><span class="op">&gt;</span></a></code></pre></div>
<ul>
<li>In <code>settings.php</code>:
<ul>
<li>Configure a hard-to-guess table name prefix (e.g. <code>5d3R_</code>) in the <code>$databases</code> array to deter SQL injections (this suggestion comes from <a href="https://www.keycdn.com/blog/drupal-security">KeyCDN</a>).</li>
<li>Check that <code>$update_free_access = FALSE;</code> to further prevent using the <code>update.php</code> script.</li>
<li>Configure trusted host patterns (e.g. <code>$settings['trusted_host_patterns'] = ['^www\.example\.com$'];</code>).</li>
<li>In each environment (development, staging, and production) configure a <a href="https://www.drupal.org/docs/8/core/modules/file/overview#content-accessing-private-files">private files directory</a> located outside of the web root.</li>
</ul></li>
<li>If you are running MariaDB 10.1.4 or greater with a XtraDB or InnoDB storage engine on your own hardware (as opposed to a hosting provider such as Linode or AWS), enable <a href="https://mariadb.com/kb/en/library/encryption/">data-at-rest encryption</a>.</li>
<li>Establish a regular process for code, files, and database off site backup using an encryption tool such as <a href="https://gnupg.org">GPG</a> or a service such as <a href="http://www.nodesquirrel.com">NodeSquirrel</a>.</li>
<li>If a VPN is available, restrict access to the administrative parts of the site to only local network/VPN users by blocking access to any pages with “admin” in the URL.</li>
<li>Note that these recommendations also apply to Docker containers, which often run everything as root out of the box. Please be aware that the Docker Community – not the Drupal Community or the Drupal Security Team – maintains the <a href="https://github.com/docker-library/drupal">official Docker images for Drupal</a>.</li>
</ul>
<h2 id="development-pre-launch">Development: Pre-Launch</h2>
<ul>
<li>Before launching the site, calibrate security expectations and effort. Consider questions such as, who are the past attackers? Who are the most vulnerable users of the site? Where are the weaknesses in the system design that are “by design”? What are the costs of a breach? This discussion might be a good project kickoff topic.</li>
<li>Establish a policy to stay up-to-date with <a href="https://www.drupal.org/security">Drupal security advisories and announcements</a>. Subscribe via <a href="https://groups.drupal.org/node/15254/feed">RSS</a> or email, follow <span class="citation" data-cites="drupalsecurity">[@drupalsecurity]</span>(https://twitter.com/drupalsecurity) on Twitter, or create an <a href="https://en.wikipedia.org/wiki/IRC_bot">IRC bot</a> that automatically posts security updates. Use whatever method works best for your team.</li>
<li>Create a process for keeping modules up to date, how and when security updates are applied, and considers the <a href="https://www.drupal.org/drupal-security-team/security-risk-levels-defined">security risk level</a>. <a href="https://www.mydropwizard.com/blog/understanding-drupal-security-advisories-risk-calculator">Drupal security advisories</a> are based on the <a href="http://www.nist.gov/itl/csd/cmss-072512.cfm">NIST Common Misuse Scoring System (NISTIR 7864)</a>, so advisories marked “Confidentiality impact” (CI) are especially crucial for sites with private and confidential data.</li>
<li>Establish a process for handling <a href="https://en.wikipedia.org/wiki/Zero-day_(computing)">zero-day vulnerabilities</a>.</li>
<li>Review content types to ensure that all sensitive data, such as user photos and other sensitive files, are uploaded as private files.</li>
<li>Implement one or more of the <a href="https://www.drupal.org/node/598562">Drupal modules for password management</a>. The United States National Institute of Standards and Technology (NIST) offers <a href="https://pages.nist.gov/800-63-3/sp800-63b.html">detailed guidelines about passwords</a>. Here are some policies that are easy to implement using submodules of the <a href="https://www.drupal.org/project/password_policy">Password Policy</a> module:
<ul>
<li>At least 8 characters (Password Character Length Policy module)</li>
<li>Maximum of 2 consecutive identical characters (Password Consecutive Characters Policy module)</li>
<li>Password cannot match username (Password Username Policy)</li>
<li>Password cannot be reused (Password Policy History)</li>
</ul></li>
<li>Consider having your organization <a href="https://www.troyhunt.com/only-secure-password-is-one-you-cant/">standardize on a password manager</a> and implement training for all employees.</li>
<li>Consider using the <a href="https://www.drupal.org/project/tfa">Two-Factor Authentication (TFA) module</a>.</li>
<li>Change the admin username to something other than admin.</li>
<li>Enable the <a href="https://www.drupal.org/project/login_security">Login Security</a> module, which detects password-guessing and brute-force attacks performed against the Drupal login form.</li>
<li>Enable <a href="https://www.drupal.org/project/security_review">Security Review</a> module, run the Security Review checklist, and make sure all tests pass. Disable this module before site launch, but use this module to perform security reviews each time core or contributed modules are updated.</li>
<li>Carefully review Drupal user permissions and lock down restricted areas of the site.</li>
</ul>
<h2 id="development-ongoing">Development: Ongoing</h2>
<ul>
<li>Perform a thorough and recurring analysis of your site using online tools such as <a href="https://www.ssllabs.com/ssltest/">Qualys SSL Server Test</a>, <a href="https://sitecheck.sucuri.net/">Sucuri SiteCheck scanner</a>, <a href="http://www.unmaskparasites.com">Unmask Parasites</a>, and <a href="https://securityheaders.com">securityheaders.com</a>.</li>
<li>Carefully review contributed modules to ensure they contain secure code. Prioritize stable releases of modules that are covered by Drupal’s <a href="https://www.drupal.org/node/475848">security advisory policy</a>.</li>
<li>For custom modules, follow advice for <a href="https://www.drupal.org/docs/8/security/writing-secure-code-for-drupal-8">writing secure code</a>.</li>
<li>Follow guidelines to <a href="https://www.drupal.org/docs/develop/security/why-does-drupal-filter-on-output">preserve user input</a> and ensure that all <a href="https://www.drupal.org/docs/8/security/drupal-8-sanitizing-output">output is sanitized</a>.</li>
<li>Build forms using Drupal’s <a href="https://www.drupal.org/docs/8/api/form-api">Form API</a> to help <a href="https://www.drupal.org/node/178896">protect against cross-site request forgeries (CSRF)</a>.</li>
<li>Check code integrity before each deployment with the <a href="https://www.drupal.org/project/coder">Coder module</a>.</li>
<li>Use <a href="https://eslint.org/">ESLint</a> to scan JavaScript for obvious errors.</li>
<li>Use the <a href="http://www.drush.org./">drush</a> tools provided by <a href="https://www.drupal.org/project/database_sanitize">Database Sanitize</a> to sanitize database data for local development.</li>
</ul>
  
  <div class="prev-next">
  
  <a href=".././upcoming-live-coding-events-in-minnesota-and-wisconsin/" rel="previous">← Previous Post</a>
  
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
