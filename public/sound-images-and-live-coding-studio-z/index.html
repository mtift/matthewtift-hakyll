<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - Sound, Images, and Live Coding at Studio Z</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>Sound, Images, and Live Coding at Studio Z</h1>
    <div class="post">

  <div class="info">
    Posted on June 11, 2018
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/live-coding/">live coding</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  
  <div class="main-image">
    <img alt="Empty stage at Studio Z" src="../images/studio-z-stage.jpg" />
  </div>
  

  <p>On Friday (June 8) I attended my first live coding event, announced as “<a href="http://www.studiozstpaul.com/blog/nada-presents-spednar-rew-and-local-artists">Nada presents Spednar, Rew, and Local Artists</a>.” This concert felt unlike any music concert I had ever attended. I greatly enjoyed the event and I’ve been investigating why. What follows recounts my experiences at one live coding event, and does not attempt to characterize all live coding events.</p>
<!--more-->
<h2 id="venue">Venue</h2>
<p>Just finding the venue set the stage for a night of surprises.</p>
<p>I had read Alex McLean’s <a href="https://dl.acm.org/citation.cfm?doid=2633638.2633647">descriptions of live coding performances</a>: they take place “late at night with a dancing nightclub audience” or “during the day to a seated concert hall audience” or “in more collaborative, long form performance.” I think I may have been expecting a dancing nightclub audience.</p>
<p>This was my first event at <a href="http://www.studiozstpaul.com/">Studio Z</a> and I had some trouble finding the place. A quick glance at my phone told me the general location, essentially across the street from <a href="http://saintsbaseball.com/chs-field1/parking">CHS field</a> where a <a href="http://saintsbaseball.com/">St. Paul Saints</a> baseball game was in progress. I’ve attended Saints games and I know the area, but when I walked down to the appropriate corner I couldn’t find Studio Z. At this point, I’m literally standing on a corner, seeing a familiar and conventional entertainment venue, inhabited by thousands of people, but I’m instead seeking out an unknown venue, filled with strangers participating in a more unconventional performance.</p>
<p>I probably should have read the <a href="http://www.studiozstpaul.com/directions--parking.html">directions on the website</a>, which suggest, “Look for the neon Z in the window!” Indeed, I eventually looked up and saw the glowing Z in the window and simultaneously felt relief and realized that Studio Z isn’t a typical concert hall, nightclub, or theater.</p>
<p>Once I entered the correct building, my next challenge was to find Studio Z. Out of habit, I sought out stairs rather than use the elevator, which created more confusion for me because the lobby of the historic Northwestern Building, built in 1919 and now home of Studio Z, has two sets of stairs <a href="http://images2.loopnet.com/i2/3GaRgykWxF8xdzN79l9w-OqGiJJm4dc4ZkW7fDpq_Ck/112/image.jpg">going in opposite directions</a>. Eventually I found the second floor, a door labeled “Suite 200,” and I walked in late. When I opened the door, I did not expect to find a room full of people sitting on chairs in the dark.</p>
<p>The disorientation I felt walking into Studio Z was perhaps heightened by the fact that I had come from an more familiar situation. I’m an organizer for the <a href="https://2018.tcdrupal.org/">Twin Cities Drupal Camp</a> (since it started in 2011) and this was the weekend. On the Friday of the Studio Z concert I had been in “conference mode,” talking about technology, learning new tools, and writing code. But even though I had been thinking about coding and music – I had given a talk earlier in the day about “<a href="https://2018.tcdrupal.org/session/drupal-and-music-industry-learning-our-success">Drupal and the music industry</a>” – and I was arriving at an event where people would be creating music with code, when I opened that door I felt surprised.</p>
<h2 id="the-performance">The Performance</h2>
<p>At this point, it seems prudent to mention my Ph.D. in music history, and that in graduate school I took exams that required me to be prepared to answer essentially any question about any period in the history of Western music. Additionally, I’ve been performing a variety of music, including “modern music,” since I first learned how to play the violin 37 years ago. For example, in college I performed Steve Reich’s “<a href="https://en.wikipedia.org/wiki/Violin_Phase">Violin Phase</a>” on my violin and computer. I have taught a wide array of music courses and worked for numerous music organizations, including the Minnesota Orchestra. I have begun to learn how to live code, reading about the history and theory of live coding and algorithmic music, and watching live coding videos. And yet this concert still managed to affect me.</p>
<p>Come to think of it, the “stage” in Studio Z didn’t look much different from other <a href="https://www.youtube.com/watch?v=jiNWS2-zYSM">videos of live coding on the internet</a>. Someone dressed in black, in this case <a href="http://michaelmasaruflora.com/">Michael Flora</a>, sat at his laptop in a dark room with a screen behind him. Countless videos <a href="https://www.youtube.com/watch?v=fIuqDKzYBzc&amp;list=RDfIuqDKzYBzc">on the Internet</a> <a href="https://www.youtube.com/watch?v=JY8eNSu6HrU">show</a> <a href="https://www.youtube.com/watch?v=MOWSyygP1O8">people</a> <a href="https://www.youtube.com/watch?v=zlFoKUDB6Y0">live coding</a> <a href="https://www.youtube.com/watch?v=smQOiFt8e4Q">in front</a> <a href="https://vimeo.com/132730904">of screens</a>. Sometimes with people talking in the background. But on this night at Studio Z, the audience of perhaps 20 people sat in chairs and watched performers on a “stage” (the image at the top of this page depicts the “stage” at Studio Z after the lights came on).</p>
<p>Videos of live coding performances usually show code projected on large screens, in keeping with the <a href="https://toplap.org/wiki/ManifestoDraft">TOPLAP manifesto</a>, which states, “code should be seen as well as heard.” I expected to see a lot of code, but in this concert we instead saw a series of intriguing visuals. I recently had read Pelle Dahlstedt’s article about “Action and Perception” in <em>The Oxford Handbook of Algorithmic Music</em> where he discusses “those who think that a visual performance component is unnecessary, and that modern listeners need to get used to the idea of focusing on sound rather than action.” (56) The concert at Studio Z had music and visuals, but no code, which created a fascinating, unfamiliar sensation for me of attending a “music concert” with a desire to see code. I got over it though and enjoyed the visuals.</p>
<p>The five artists – <a href="http://michaelmasaruflora.com/">Michael Flora</a> (ScgPlrAoaCuiEdn), <a href="http://johnkeston.com/">John Keston</a> (Vocalise Sintectica), <a href="http://kindohm.com/">Mike Hodnick</a> (Kindohm), and <a href="https://spednar.bandcamp.com/">Kevin Bednar</a> and <a href="http://www.rew.media/Video">Rachel Wagner</a> (Computational Chiaroscuro) – said little, if anything during the evening. They performed using a mixture of approaches to audiovisual performance using computational processes. Describing the variety of images we saw seems almost comical, but I’ll just say that I recall seeing boxes, squares, glasses of water, industrial equipment, cats with laser vision, people’s faces, and much more. Each artist had their own unique style. I could describe the sounds I heard at various times using words like “musical,” “improvised,” “distorted,” “rhythmic,” “minimalist,” “loud,” “quiet,” “melodic,” etc., but for now I’d rather shy away from the challenge of attempting a meaningful musical analysis. It’s not difficult to imagine people describing the performances simply as “weird.”</p>
<p>During the concert I noticed feelings of excitement simply because I was experiencing live coding <em>live</em>. Before this concert, my live coding experiences consisted of either recorded concerts or live coding by myself (my wife and kids prefer when I use headphones). But at Studio Z I found myself in a room with other people, feeling expectations (or at least I perceived them as expectations) to sit and listen. I could not click pause and wander off when the next thought arose. That’s a more familiar feeling to me, comparable to the experience of meditating alone versus in a group. Sitting still for an hour alone in my home is not the same feeling as sitting still in a room with other people in a remote location on a meditation retreat. In both situations, my desire to remain with the group and not negatively affect other people provides additional motivation to sit still and remain concentrated in the present moment.</p>
<p>Overall, this performance confirmed some ideas about live coding that I previously had only understood on an intellectual level. Live coding encourages us to remain focused in the present moment. It’s not about the past or the future. When my mind wanders, I have new audio and visual objects that can help bring me back to the present moment. After so many years of training my mind to analyze and critique, I noticed numerous moments during the concert where I felt as if I was simply experiencing the moment</p>
<p>In summary, I experienced an event in an unfamiliar location, with strangers, performing in a unfamiliar way, with unfamiliar expectations. The sheer newness of the experience fascinates me. Like listening to the unpredictable sounds of the wind chimes that hang outside my window on a windy day, I enjoyed the unpredictability of my Friday night. The result of the experience feels beneficial, and I look forward to more live coding.</p>
<p>Below are two recordings from the concert. The first is Kindohm’s set and the second is John C.S. Keston:</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/yVYrKPZi0k0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
</iframe>
<p><br /></p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/pz0eJ7pcOBA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
</iframe>
  
  <div class="prev-next">
  
  <a href=".././openvpn-ubuntu-1804/" rel="previous">← Previous Post</a>
  
  
  <a href=".././six-months-of-tidal/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
