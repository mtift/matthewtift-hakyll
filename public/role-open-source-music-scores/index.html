<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - On the Role of Open-Source Music Scores </title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>On the Role of Open-Source Music Scores </h1>
    <div class="post">

  <div class="info">
    Posted on June 10, 2012
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/open-source-music/">open source music</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <p>Few musicians or music scholars would doubt the value of a well-researched, historically-informed scholarly (a.k.a. “historical” or “critical”) edition or music score, such as those produced by <a href="http://www.schirmer.com/">G. Schirmer</a>, <a href="https://www.baerenreiter.com">Bärenreiter</a>, <a href="https://www.areditions.com/">A-R Editions</a>, <a href="http://www.henle.com/">G. Henle Verlag</a>, and others. As a corollary, many people are rightfully suspicious of <a href="http://www.wired.com/wired/archive/14.06/crowds.html?pg=1&amp;topic=crowds&amp;topic_set=">crowdsourcing</a> anything of value, from <a href="http://wikipedia.org">encyclopedias</a> to <a href="http://www.economist.com/node/21556098">medical devices</a> to <a href="http://musescore.com/sheetmusic">music scores</a>. I’m a vocal <a href="public-music-public-media-introduction">proponent of open-source software and music</a>, so here I am going to discuss how I see open-source scores fitting into the matrix of credentialed scores.</p>
<h2 id="the-urtext-model">The “Urtext” model</h2>
<p>The German word “Urtext” means “original text,” so musicians and music scholars sometime use Urtext to denote music which represents the “original” version of a musical work. Publishers such as Bärenreiter and G. Henle Verlag are particularly fond of the word and use it in their marketing materials along words such as “<a href="http://www.henle.com/en/urtext-editions/what-is-urtext.html">authoritative</a>,” “<a href="http://www.henle.com/en/urtext-editions/what-is-urtext.html">undistorted</a>,” and “<a href="https://www.baerenreiter.com/en/about-baerenreiter/baerenreiter-encyclopedia/urtext/">authentic</a>.” While publishers such as these will admit that a modern score cannot be the single, valid representation of a musical work, they would probably like it if we associate their scores rather closely with the truth.</p>
<p>Of course, the notion of an Urtext is rather different in a discussion of 9th-century Gregorian chant not attributed to a particular person, as compared to, for instance, a discussion of the musical works of the 20th-century composer Béla Bartók, who could, at some point in his composition process, declare a musical work “complete and finished.”[1] Nobody really knows what the score should be, but musicians certainly have a sense for what scores are trustworthy. Publishing companies that have been around for decades and employ a team of musicologists are rightfully among those we trust regarding music scores.</p>
<h2 id="the-networked-model">The networked model</h2>
<p>As David Weinberger discusses is his fascinating study, <a href="http://www.toobigtoknow.com/"><em>Too Big to Know</em></a>, the nature of knowledge, in the age of the Internet, is changing. Weinberger makes a compelling case for the growing importance of “networked knowledge” – as opposed to “traditional knowledge” – in our lives. Open-source music scores would be an obvious example of “networked knowledge” in that they open up participation to those without traditional credentials or privileged positions, and create a space for more social interpretations of music scores. When a music score is open source, it is no longer a one-way conversation between a publisher and musician. It allows for us to become more active participants rather than mere consumers.</p>
<p>While a “networked” publisher such as <a href="http://musescore.com/">MuseScore</a> may create and distribute their own version of an Urtext, they intentionally complicate things by applying established, open-source principles to their scores. Using open fomats, such as <a href="http://en.wikipedia.org/wiki/MusicXML">MusicXML</a>, MuseScore makes it trivial for another musician to download a score and make it better, trusting that users will eventually settle on an accurate and useable score. With open-source software, worthy changes have a tendency to become part of the versions that people use.</p>
<p>An open-source score creates a new space for musicologists to embrace the networked world. It also helps to make Bach’s music more inclusive and less exclusive. Furthermore, these scores make me wonder if there need be a wall between musicology-endorsed scores and crowdsourced scores, and what exactly that wall would separate. Any obvious errors in open-source scores that credentialed musicologists would care to point out would likely be changed rather quickly by the people most interested in those scores.</p>
<h2 id="not-necessarily-a-choice">Not necessarily a choice</h2>
<p>While it might be tempting to assume that open-source music competes with traditional music publishing companies, I don’t think this is necessarily the case. I don’t see any reason why they can’t peacefully co-exist. Surely, the primary audience for an Urtext edition is the music historian or professional musician, and those individuals will not likely stop buying editions from businesses and companies they trust.</p>
<p>If open-source music does, in fact, create new audiences for centuries-old music scores, music lovers everywhere would benefit. There are plenty of writers that give away e-books while successfully selling the paper versions, and I think that general principle is applicable here. I suspect that the companies printing editions on high-quality paper <em>in addition to</em> giving away digital versions will come out ahead.</p>
<h2 id="imslp">IMSLP</h2>
<p>For instance, the <a href="http://imslp.org/">International Music Score Library Project</a> (IMSLP), which Edward W. Guo founded in 2006, makes digital music scores available free of charge to anyone with a computer and internet connection. It also offers low-cost, printed versions on demand. By employing the principles that made Wikipedia such a massive success, IMSLP has created a space for an immense collection of music scores to accumulate. And it’s completely crowdsourced.</p>
<p>In what is perhaps the best-known musicological journal, the <a href="http://www.ams-net.org/pubs/jams.php"><em>Journal of the American Musicological Society</em></a>, it has been noted that the legality of these scores is “questionable.”[2] The IMSLP servers are physically located in Canada and subject to Canadian copyright law. In Canada a score is in the <a href="http://en.wikipedia.org/wiki/Public_domain">public domain</a> if the composer died before 1962, and in the United States, anything published before 1923 is in the public domain. Thus, one can be rather certain that a <a href="http://imslp.org/wiki/Goldberg-Variationen,_BWV_988_(Bach,_Johann_Sebastian)">score of Bach’s Goldberg Variations</a> is in the public domain.</p>
<p>When asked about IMSLP, a representative from Universal Edition admitted, “<a href="http://www.nytimes.com/2011/02/22/arts/music/22music-imslp.html?pagewanted=all">there’s room for both of us</a>.” I agree, and I hope that open-source scores can continue to grow in popularity, and that they can extend the audiences and uses for the music that many of us love (and <a href="open-source-music-10-more-reasons-why-it-fits">even more reasons</a>). Music scores are both a representation of sound and an aid to performance, so open-source scores that make music more available enable new interpretations or more performances. I welcome open-source music scores into our audacious, ever-expanding, open-source world.</p>
<h2 id="notes">NOTES</h2>
<p>1. Kenneth Levy, “Charlemagne’s Archetype of Gregorian Chant,” <em>Journal of the American Musicological Society</em> 40 (1987): 1–30; László Somfai, “Manuscript versus Urtext: The Primary Sources of Bartók’s Works,” <em>Studia Musicologica Academiae Scientiarum Hungaricae</em> 23 (1981): 18.<br />
2. Samuel N. Dorf, “Review,” <em>Journal of the American Musicological Society</em>, 65 (2012): 280.</p>
  
  <div class="prev-next">
  
  <a href=".././open-source-music-10-more-reasons-why-it-fits/" rel="previous">← Previous Post</a>
  
  
  <a href=".././bach-meets-high-tech-wisconsin-public-radio-broadcast-experiment/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
