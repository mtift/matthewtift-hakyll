<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon.ico">
    <title>Matthew Tift - Drupal 8: Hello, Configuration Management</title>
    <link rel="stylesheet" href="../css/default.css" />
    <link rel="stylesheet" href="../css/agate.css" />
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </head>

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://piwik.matthewtift.com/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

  <body>
    <header>
      <div class="logo">
        <a href="../">Matthew Tift</a><br />
        <p class="tagline">Free software, musicology, live coding</p>
      </div>
      <nav>
        <a href="../">Home</a>
        <a href="../about">About</a>
        <a href="../cv">Vita</a>
        <a href="../archive">Archive</a>
        <a href="../contact">Contact</a>
      </nav>
    </header>

    <main role="main">
    <h1>Drupal 8: Hello, Configuration Management</h1>
    <div class="post">

  <div class="info">
    Posted on September  6, 2013
    
      by Matthew Tift
    
  </div>

  <div class="tags">
    
      Tags: <a href="../tags/Drupal/">Drupal</a>, <a href="../tags/configuration-management/">configuration management</a>
    
  </div>

  <p align="right">
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
      // Copyright (C) 2016 Nandakumar Edamana and John C. Vernaleo

      dark_theme = false;
      function toggle_dark_theme() {
          dark_theme = !dark_theme;

          links = document.getElementsByTagName('a');

          if(dark_theme) {
              document.body.className = "dark";
              for(i = 0; i < links.length; i ++) {
                links[i].className = "dark";
              }
          } else {
              document.body.className = "light";
              for(i = 0; i < links.length; i ++) {
                  links[i].className = "light";
              }
          }
      }
    // @license-end
    </script>
    <a href="#" onclick="toggle_dark_theme()">Toggle Dark Theme</a>
  </p>

  <hr />

  

  <h5 id="please-note-the-information-in-this-post-is-now-out-of-date.-for-now-you-can-find-some-additional-information-in-the-configuration-system-section-on-drupal.org"><em>PLEASE NOTE: The information in this post is now out-of-date. For now, you can find some additional information in the <a href="https://drupal.org/node/1667894">Configuration System</a> section on Drupal.org</em></h5>
<p>If you have not read Alex Bronstein’s (<a href="https://drupal.org/user/78040">effulgentsia</a>’s) excellent blog post about creating your first Drupal 8 module, go read it: <a href="http://effulgentsia.drupalgardens.com/content/drupal-8-hello-oop-hello-world">Drupal 8: Hello OOP, Hello world!</a> This post will expand on Alex’s discussion and introduce the <a href="https://drupal.org/node/1667894">new configuration API</a> in Drupal 8. Here we will have the modest goal of making the display of the text “Hello, World!” configurable – more specifically, we will give site administrators the ability to make the text UPPERCASE or Title Case. I can tell you are excited.</p>
<p>Fortunately, the <code>.info.yml</code> file does not need to change:</p>
<h2 id="hello.info.yml">hello.info.yml</h2>
<pre><code>name: Hello 
core: 8.x 
type: module </code></pre>
<p>We need to add some code to the routing file, and it will follow a similar organization as before:</p>
<h2 id="hello.routing.yml">hello.routing.yml</h2>
<pre><code>hello: 
  pattern: '/hello' 
  defaults: 
    _content: '\Drupal\hello\Controller\HelloController::content' 
  requirements: 
    _permission: 'access content'

hello.settings_form:
  pattern: '/hello/config'
  defaults:
    _form: 'Drupal\hello\Form\HelloConfigForm'
  requirements:
    _permission: 'administer site configuration'</code></pre>
<p>Notice that instead of adding <code>_content</code>, this route adds a <code>_form</code> so that when a visitor goes to <code>hello/config</code>, the <code>HelloConfigForm</code> will get called. So far so good.</p>
<p>The .module file also should look rather familiar:</p>
<h2 id="hello.module">hello.module</h2>
<pre><code>/**
 * Implements hook_menu().
 */
function hello_menu() {
  $items['hello'] = array(
    'title' =&gt; 'Hello',
    'route_name' =&gt; 'hello',
  );
  $items['hello/config'] = array(
    'title' =&gt; 'Hello Configuration',
    'route_name' =&gt; 'hello.settings_form',
  );
  return $items;
}</code></pre>
<p>The second of the two <code>$items</code> connects the URL <code>hello/config</code> with the route <code>hello.settings_form</code>, which will appear again in the next few code examples. Still seems fairly straightforward.</p>
<p>When the Hello module is installed, we want to have a default case setting configured, so let’s make the text title case by default.</p>
<h2 id="confighello.settings.yml">config/hello.settings.yml</h2>
<pre><code>case: title</code></pre>
<p>Out of the box, Drupal’s new configuration system uses two directories for configuration: active and staging. When the Hello module is enabled, the configuration system will look in the Hello module’s <code>config</code> directory and find <code>hello.settings.yml</code>. Then it will create a new file in the “active” configuration directory, also named <code>hello.settings.yml</code>, with the same single line that reads <code>case: title</code>. The default location for the active and staging directories is <code>sites/default/files/config_xxx</code>, with xxx being a hash that is unique to each Drupal installation for security purposes (security options were discussed at length in this thread: https://groups.drupal.org/node/155559).</p>
<p>Although we could have accomplished the same thing in an install file by implementing <code>hook_install()</code> and calling <code>Drupal::config('hello.settings')-&gt;set('case', 'title')-&gt;save();</code>, the preferred – and easier – method is to provide default configuration for the module in a <code>config</code> directory (as we have done).</p>
<p>The <code>HelloController</code> class needs to be altered so that it will display the correct case:</p>
<h2 id="libdrupalhellocontrollerhellocontroller.php">lib/Drupal/hello/Controller/HelloController.php</h2>
<pre><code>namespace Drupal\hello\Controller;

use Drupal\Core\Controller\ControllerBase;

class HelloController extends ControllerBase {
  public function content() {
    $config = $this-&gt;config('hello.settings');
    $case = $config-&gt;get('case');
    if ($case == 'upper') {
      $markup = $this-&gt;t('HELLO, WORLD!');
    }
    elseif ($case == 'title') {
      $markup = $this-&gt;t('Hello, World!');
    }

    return array(
      '#markup' =&gt; $markup,
    );
  }
}</code></pre>
<p>While <code>hello.install</code> interacted with the configuration system using the <a href="https://api.drupal.org/api/drupal/core%21lib%21Drupal.php/class/Drupal/8">Drupal class</a>, calling <code>Drupal::config()</code>, the Drupal class exists only to support legacy code. In <code>HelloController</code> we take another shortcut of sorts by extending a fairly recent addition to Drupal core, <a href="https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Controller%21ControllerBase.php/class/ControllerBase/8">ControllerBase</a>. This class was created to “<a href="https://drupal.org/node/2060189">stop the boilerplate code madness</a>” by providing access to a number of utility methods, including <code>config()</code>. This class should only be used for “trivial glue code,” and I think our Hello, World! module qualifies.</p>
<p>To give site administrators the ability to change the display from title case to uppercase, we will use a form. Much like putting the controller in <code>lib/Drupal/hello/Controller/HelloController.php</code>, the code for the configuration form goes in <code>lib/Drupal/hello/Form/HelloConfigForm.php</code>. It’s not a requirement, but it is a convention to put forms in a Form directory and use the word Form in the filename. Here is the code for the form:</p>
<h2 id="libdrupalhelloformhelloconfigform.php">lib/Drupal/hello/Form/HelloConfigForm.php</h2>
<pre><code>/**
 * @file
 * Contains \Drupal\hello\Form\HelloConfigForm.
 */

namespace Drupal\hello\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\Context\ContextInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\system\SystemConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure text display settings for this the hello world page.
 */
class HelloConfigForm extends SystemConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Constructs a \Drupal\hello\Form\HelloConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\Context\ContextInterface $context
   *   The configuration context to use.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactory $config_factory, ContextInterface $context, ModuleHandler $module_handler) {
    parent::__construct($config_factory, $context);
    $this-&gt;moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container-&gt;get('config.factory'),
      $container-&gt;get('config.context.free'),
      $container-&gt;get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'hello.settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &amp;$form_state) {
    $config = $this-&gt;configFactory-&gt;get('hello.settings');
    $case = $config-&gt;get('case');
    $form['hello_case'] = array(
      '#type' =&gt; 'radios',
      '#title' =&gt; $this-&gt;t('Configure Hello World Text'),
      '#default_value' =&gt; $case,
      '#options' =&gt; array(
        'upper' =&gt; $this-&gt;t('UPPER'),
        'title' =&gt; $this-&gt;t('Title'),
      ),
      '#description' =&gt; $this-&gt;t('Choose the case of your &quot;Hello, world!&quot; message.'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &amp;$form, array &amp;$form_state) {
    $this-&gt;configFactory-&gt;get('hello.settings')
      -&gt;set('case', $form_state['values']['hello_case'])
      -&gt;save();

    parent::submitForm($form, $form_state);
  }
}</code></pre>
<p>This form contains what was previously described as “<a href="https://drupal.org/node/2049159">boilerplate madness</a>” and illustrates the preferred way of interacting with Drupal’s configuration system: via dependency injection. The “boilerplate” parts include the <code>use</code> statements near the top, the <code>__construct()</code> method, and the <code>create()</code> method.</p>
<p>Dependency injection is the design pattern that all of the cool kids use. It allows developers the ability to <a href="http://symfony.com/doc/current/components/dependency_injection/introduction.html">standardize and centralize the way objects are constructed</a>. If you want to read more about dependency injection, check out Crell’s <a href="https://drupal.org/node/1953342">WSCCI Conversion Guide</a> or <a href="https://drupal.org/node/1539454">this change record</a>. If you want to watch a presentation about it, check out Kat Bailey’s talk <a href="https://portland2013.drupal.org/session/dependency-injection-drupal-8">Dependency Injection in Drupal 8</a>. If you want to explain it to a five year old, read <a href="http://stackoverflow.com/a/1638961">this stackoverflow answer</a>.</p>
<p>For the <code>HelloConfigForm</code>, dependency injection enables code like this: <code>$config = $this-&gt;configFactory-&gt;get('hello.settings')</code>. It’s a lot more code than writing <a href="https://api.drupal.org/api/drupal/core%21lib%21Drupal.php/function/Drupal%3A%3Aconfig/8">Drupal::config()</a> or <a href="https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Controller%21ControllerBase.php/function/ControllerBase%3A%3Aconfig/8">ControllerBase::config()</a> – and we could have dropped three of the four <code>use</code> statements and omitted two of the methods – but I assure you that dependency injection is the preferred method.</p>
<p>The rest of the code should look rather familiar if you already know about writing modules with Drupal 7. You can read more about the <code>{@inheritdoc}</code> in <a href="https://drupal.org/node/1906474">this issue</a>, but the basic idea is that it is less code. (If you want to read more about forms in Drupal 8, check out <a href="http://effulgentsia.drupalgardens.com/content/drupal-8-forms-oop-style">Drupal 8: Forms, OOP style</a>).</p>
<p>This may seem like a fair amount of code just to use the configuration system, but in fact this Hello module takes advantage of multiple new features offered by Drupal 8. The meat of this process could be reduced to four steps, and only the last two access configuration:</p>
<ol type="1">
<li>Implement <code>hook_menu()</code></li>
<li>Add a route in <code>hello.routing.yml</code></li>
<li>Create a controller to display the text</li>
<li>Create a configuration form</li>
</ol>
<p>Of course the most important part of this code is that we’re using Drupal’s new configuration system. This Hello module stores its configuration data in a configuration file (and in the database in the cache_config table), and each time the form is changed <code>sites/default/files/config_xxx/active/hello.settings.yml</code> is updated, thus allowing the ability to transfer configuration between sites. That, however, is a topic for another post.</p>
<p>Until then, you can come to this site to learn more about the Configuration Management Initiative, find links to documentation, issues that need work, and much more. If you would like to get involved, the issues that are the most important for Configuration Management are listed on <a href="http://drupal8cmi.org/focus" class="uri">http://drupal8cmi.org/focus</a>.</p>
<h3 id="acknowledgements">ACKNOWLEDGEMENTS</h3>
<p>A special thanks to <a href="https://drupal.org/user/241634">tim.plunkett</a>, <a href="https://drupal.org/user/65776">xjm</a>, and <a href="https://drupal.org/user/78040">effulgentsia</a>, who each read an earlier draft of this post and helped me improve it.</p>
<p><em>I originally wrote this blog post for the <a href="http://drupal8cmi.org/drupal-8-hello-configuration-management">Drupal 8 Configuration Management Initiative (CMI) site</a>. I’m cross-posting it to my blog.</em></p>
  
  <div class="prev-next">
  
  <a href=".././three-great-ways-get-involved-drupal-core-development-today/" rel="previous">← Previous Post</a>
  
  
  <a href=".././why-you-should-vote/" rel="next" style="float:right">Next Post →</a>
  
</div>


</div>


    </main>

    <footer>
      <div class="footer-left">
        <form class="ddg" name="x" action="//duckduckgo.com/">
            <input type="hidden" value="matthewtift.com" name="sites" />
            <input type="hidden" value="1" name="kh" />
            <input type="hidden" value="1" name="kn" />
            <input type="hidden" value="1" name="kac" />
            <input type="search" id="search" placeholder="Search" name="q" aria-label="Search" />
            <button id="dgg-button" type="submit">Go</button>
        </form>
        <p>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" style="border-width:0" /><br />
          Unless otherwise noted, content on this site is licensed under a<br />
          <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0
            International License</a>.<br />
          Copyright © 2004-2018, Matthew Tift.
          <br />Site built with <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </p>
      </div>
      <div class="footer-right">
        <div class="social">
          <a href="../rss.xml"><img src="../images/rss.png" alt="RSS icon" width="30px" /></a>
          <a href="https://twitter.com/matthewtift"><img src="../images/twitter.png" alt="Twitter icon" width="30px" /></a>
          <a href="https://gitlab.com/mtift"><img src="../images/gitlab.png" alt="GitLab icon" width="30px" /></a>
          <a href="https://post.lurk.org/@mtift"><img src="../images/mastodon.png" alt="Mastodon icon" width="30px" /></a>
        </div>
      </div>
    </footer>
  </body>
</html>
