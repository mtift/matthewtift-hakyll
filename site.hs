--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Control.Applicative ((<$>), Alternative (..))
import           Control.Monad          (forM, forM_, liftM)
import           Data.List              (sortBy, isInfixOf, isSuffixOf, 
                                         intercalate)
import           Data.List.Utils        (replace)
import           Data.Monoid            (mappend, (<>), mconcat)
import           Data.Ord               (comparing)
import           Data.Time.Clock        (UTCTime)
import           Data.Time.Format
import           Hakyll
import           Hakyll.Web.Redirect    (createRedirects)
import           System.Process         (system)
import           System.FilePath.Posix  (takeBaseName, takeDirectory,
                                         (</>), takeFileName, splitPath,
                                         joinPath, replaceExtension,
                                         splitFileName)

--------------------------------------------------------------------------------

main :: IO ()
main = hakyllWith config $ do

    match (fromList ["CNAME"
                    , "404.html"
                    , "keybase.txt"
                    , "favicon.ico"
                    ]) $ do
        route   $ idRoute
        compile $ copyFileCompiler

    -- Create static redirect pages outdated/broken links.
    version "redirects" $ createRedirects brokenLinks

    match "files/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "talks/**" $ do
        route   $ gsubRoute "/talks/" (const "")
        compile copyFileCompiler

    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler

    match "pages/*" $ do
        route   $ flatRoute 0 0 `composeRoutes` cleanRoute
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls
    
    match "contact.md" $ do
        route   $ niceRoute
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/contact.html" defaultContext
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls

    -- Build tags
    tags <- extractTags

    -- Generate tag pages
    tagsRules tags $ \tag pattern -> do
        let title = "Posts tagged \"" ++ tag ++ "\""
        route   $ niceRoute
        compile $ do
            posts <- recentFirst =<< loadAll pattern
            let ctx = constField "title" title
                      `mappend` listField "posts" postCtx (return posts)
                      `mappend` defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/tag.html" ctx
                >>= loadAndApplyTemplate "templates/default.html" ctx
                >>= relativizeUrls
                >>= removeIndexHtml

    match "posts/*" $ do
        route   $ flatRoute 0 17 `composeRoutes` cleanRoute
        compile $ pandocCompiler
            -- save a snapshot to be used later in rss generation
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate "templates/post.html"    (postCtxWithTags tags)
            >>= loadAndApplyTemplate "templates/default.html" (postCtxWithTags tags)
            >>= relativizeUrls
            >>= removeIndexHtml

    create ["archive.html"] $ do
        route   $ niceRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let archiveCtx =
                    listField "posts" teaserCtx (return posts) `mappend`
                    constField "title" "Archives"            `mappend`
                    defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                >>= relativizeUrls
                >>= removeIndexHtml

    match "index.html" $ do
        route idRoute
        compile $ do
            posts <- loadRecentPosts
            let indexCtx =
                    listField "posts" teaserCtx (return posts) `mappend`
                    constField "title" "Home"                `mappend`
                    defaultContext

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls
                >>= removeIndexHtml

    match "templates/*" $ compile templateBodyCompiler
    
    create ["rss.xml"] $ do
      route idRoute
      compile $ do
        -- load all "content" snapshots of all posts
        loadAllSnapshots "posts/*" "content"
        -- take the latest 10
        >>= (fmap (take 10)) . createdFirst
        -- renderAntom feed using some configuration
        >>= renderRss feedConfiguration feedCtx
      where
        feedCtx :: Context String
        feedCtx =  defaultContext <>
                   -- $description$ will render as the post body
                   bodyField "description"

--------------------------------------------------------------------------------

-- ==========================
-- Options and Configurations
-- ==========================

-- Instead of "_site", output to the "public" direcotory for GitHub pages.
config :: Configuration
config = defaultConfiguration {
    destinationDirectory = "public"
}

-- Feed from http://yannesposito.com/Scratch/en/blog/Hakyll-setup/
feedConfiguration :: FeedConfiguration
feedConfiguration = FeedConfiguration
  { feedTitle = "Matthew Tift - Free software, musicology, live coding"
  , feedDescription = "This feed provides information for matthewtift.com"
  , feedAuthorName = "Matthew Tift"
  , feedAuthorEmail = "me@matthewtift.com"
  , feedRoot = "https://matthewtift.com"
  }

-- =================
-- Context functions
-- =================

postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" <>
    field "nextPost" nextPostUrl <>
    field "prevPost" prevPostUrl <>
    defaultContext

postCtxWithTags :: Tags -> Context String
postCtxWithTags tags = tagsField "tags" tags <> postCtx

teaserCtx :: Context String
teaserCtx = teaserField "teaser" "content" <> postCtx

-- ===================
-- Auxiliary Functions
-- ===================

createdFirst :: [Item String] -> Compiler [Item String]
createdFirst items = do
  -- itemsWithTime is a list of couple (date,item)
  itemsWithTime <- forM items $ \item -> do
    -- getItemUTC will look for the metadata "published" or "date"
    -- then it will try to get the date from some standard formats
    utc <- getItemUTC defaultTimeLocale $ itemIdentifier item
    return (utc,item)
  -- we return a sorted item list
  return $ map snd $ reverse $ sortBy (comparing fst) itemsWithTime

-- Allow for cleaan URLs: https://www.rohanjain.in/hakyll-clean-urls/
cleanRoute :: Routes
cleanRoute = customRoute createIndexRoute
  where
    createIndexRoute ident = takeDirectory p </> takeBaseName p </> "index.html"
                             where p = toFilePath ident

-- Flatten directories
flatRoute :: Int    -- ^ Number of directories to flaten to
          -> Int    -- ^ The number of characters to drop from the filename
          -> Routes -- ^ The resulting route
flatRoute x y = customRoute $ createFlatRoute
  where
    createFlatRoute ident = p' </> takeFileName p
                            where p' = joinPath $ take x $ splitPath p
                                  p = replace " " "-" $ drop y $ toFilePath ident

-- replace a foo/bar.md with foo/bar/index.html
-- this way the url looks like: foo/bar in most browsers
niceRoute :: Routes
niceRoute = customRoute createIndexRoute
  where
    createIndexRoute ident =
        takeDirectory p </> takeBaseName p </> "index.html"
          where p = replace " " "-" $ toFilePath ident

-- replace url of the form foo/bar/index.html by foo/bar
removeIndexHtml :: Item String -> Compiler (Item String)
removeIndexHtml item = return $ fmap (withUrls removeIndexStr) item
  where
    removeIndexStr :: String -> String
    removeIndexStr url = case splitFileName url of
        (dir, "index.html") | isLocal dir -> dir
        _                                 -> url
        where isLocal uri = not (isInfixOf "://" uri)

-- Load just the 5 most recent posts
loadRecentPosts :: Compiler [Item String]
loadRecentPosts = liftM (take 5) $
  loadAll ("posts/**")
  >>= recentFirst

-- Inspired by https://javran.github.io/posts/2014-03-01-add-tags-to-your-hakyll-blog.html
extractTags :: Rules Tags
extractTags = do
  tags <- buildTags ("posts/*") $ fromCapture "tags/*.html"
  return $ sortTagsBy caseInsensitiveTags tags


--------------------------------------------------------------------------------
-- Next and previous posts:
-- https://github.com/rgoulter/my-hakyll-blog/commit/a4dd0513553a77f3b819a392078e59f461d884f9
-- these are necessary to make the next and prev buttons work
-- then you also have to add a prev-next html template and then
-- add that template to your post.html template
prevPostUrl :: Item String -> Compiler String
prevPostUrl post = do
  posts <- getMatches postsGlob
  let ident = itemIdentifier post
      sortedPosts = sortIdentifiersByDate posts
      ident' = itemBefore sortedPosts ident
  case ident' of
    Just i -> (fmap (maybe empty toUrl) . getRoute) i
    Nothing -> empty

postsGlob :: Pattern
postsGlob = "posts/*"

nextPostUrl :: Item String -> Compiler String
nextPostUrl post = do
  posts <- getMatches postsGlob
  let ident = itemIdentifier post
      sortedPosts = sortIdentifiersByDate posts
      ident' = itemAfter sortedPosts ident
  case ident' of
    Just i -> (fmap (maybe empty toUrl) . getRoute) i
    Nothing -> empty

itemAfter :: Eq a => [a] -> a -> Maybe a
itemAfter xs x =
  lookup x $ zip xs (tail xs)

itemBefore :: Eq a => [a] -> a -> Maybe a
itemBefore xs x =
  lookup x $ zip (tail xs) xs

urlOfPost :: Item String -> Compiler String
urlOfPost =
  fmap (maybe empty toUrl) . getRoute . itemIdentifier

sortIdentifiersByDate :: [Identifier] -> [Identifier]
sortIdentifiersByDate =
    sortBy byDate
  where
    byDate id1 id2 =
      let fn1 = takeFileName $ toFilePath id1
          fn2 = takeFileName $ toFilePath id2
          parseTime' fn = parseTimeM True defaultTimeLocale "%Y-%m-%d" $ intercalate "-" $ take 3 $ splitAll "-" fn
      in compare (parseTime' fn1 :: Maybe UTCTime) (parseTime' fn2 :: Maybe UTCTime)

-- A list of URLs to redirect.
brokenLinks :: [(Identifier,FilePath)]
brokenLinks = [
   ("blog", "/archive")
 , ("speaking", "/about")
 , ("tags/amp", "/tags/AMP")
 , ("tags/backdrop", "/tags/Backdrop")
 , ("tags/drupal", "/tags/Drupal")
 , ("tags/drupal-association", "/tags/Drupal-Association")
 , ("tags/prx", "/")
 , ("tags/red-hat", "/")
 , ("tags/vpn", "/tags/VPN")
 , ("tags/wordpress", "/tags/Wordpress")
 ]
