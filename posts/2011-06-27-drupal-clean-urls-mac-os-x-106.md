---
title: "Drupal Clean URLs on Mac OS X 10.6"
date: June 27, 2011, 11:44 am
author: Matthew Tift
tags: Drupal
path: drupal-clean-urls-mac-os-x-106
---

Depending on your development environment, there are various things you might have to do to enable clean URLs in Drupal -- this is how I did it using the Apache web server built in to Mac OS X 10.6:

1. In /private/etc/apache2/users/\[username\].conf, change (with sudo) "AllowOverride None" to "AllowOverride All"
2. In /Users/\[username\]/Sites/drupal/.htaccess, change "RewriteBase /" to "RewriteBase/drupal" (or, depending on your setup, "RewriteBase /~\[username\]/drupal")
3. Restart apache: "sudo apachectl restart"
4. Go to http://localhost/drupal/admin/config/search/clean-urls and check "Enable clean URLs"
