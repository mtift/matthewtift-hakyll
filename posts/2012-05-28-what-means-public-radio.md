---
title: 'What &lt;audio&gt; means for public radio'
date: May 28, 2012, 9:43 am
author: Matthew Tift
image: 200px-HTML5-logo.svg_.png
tags: public radio
path: what-means-public-radio
---

On Wednesday this week (May 30), I will be taking part in a "[first of a kind webinar](http://mediaengage.org/webinars/webinar_details.cfm?wbid=159955)" about the state of audio and video on the web. I consider myself very lucky to be speaking beside some esteemed leaders from the public media technology community.

Although the webinar will address some of the technical issues regarding audio (and video, but here I am focusing on audio) on the web, it's a rather complex topic, and on Wednesday we will focus on some specific implementations by showcasing new audio players from [PRX](http://blog.prx.org/2012/03/new-player-new-ways-to-listen/), [NPR](http://www.npr.org/blogs/inside/2011/11/14/142303990/introducing-the-infinite-player), as well as the new HTML5 audio player that we use on [ttbook.org](http://ttbook.org).

The underlying goal with audio players these days is to move away from proprietary plugins like Flash, Silverlight, or QuickTime, and make playing audio with the HTML5 [&lt;audio&gt; tag](http://www.w3.org/TR/html-markup/audio.html) as easy as displaying an image with the [&lt;img&gt; tag](http://www.w3.org/TR/html-markup/img.html). Among other benefits, this gives browsers a more consistent way to stream audio without needing any of the aforementioned plugins and makes it easier for designers to create a consistent look and feel on their sites.

While the &lt;audio&gt; tag offered a good start, it only provided limited functionality and was not nearly as sophisticated as something like Flash, so the [Audio Data API](https://wiki.mozilla.org/Audio_Data_API) and the [Web Audio API](http://www.w3.org/TR/webaudio/) are supposed to help bring the standard more on par with other commercial plugins.

A common phrase used to describe a modern audio player on the web is "HTML5 player with Flash fallback." The "HTML5 player" part of this description refers to using the &lt;audio&gt; tag, and if the browser does not support &lt;audio&gt;, it tries to play the audio with Flash (or Silverlight, depending on the player). One of the first, and most popular, players to offer this functionality was [jPlayer](http://www.jplayer.org/about), which launched in 2009. Other capable player projects that meet this general characterization include [MediaElement.js](http://mediaelementjs.com/), [SoundManager 2](http://www.schillmania.com/projects/soundmanager2/), [Speakker](http://www.speakker.com/), and [audio.js](http://kolber.github.com/audiojs/).

One of the fundamental impediments to using the &lt;audio&gt; tag on the web concerns the browsers used by our listeners. The website [caniuse.com](http://caniuse.com) provides up-to-date information about what [browsers support the &lt;audio&gt; tag](http://caniuse.com/#feat=audio). This highlights a problem at the station where I work because nearly a quarter of our visitors are using IE8, which does NOT support the &lt;audio&gt; tag. So we're hoping they have Flash installed. Thankfully, the majority of our other visitors use browsers that support the &lt;audio&gt; tag.

This is just a taste of the technical issues concerning &lt;audio&gt; on the web.

In the webinar, we'll address issues that reflect how users consume the audio. For example, NPR differentiates between "[engaged listening" and "distracted listening](http://www.npr.org/blogs/inside/2011/11/14/142303990/introducing-the-infinite-player)" to describe why they created their [Infinite Player](http://www.npr.org/sandbox/conplay/), which targets the latter type of interaction. The approach we used when building [ttbook.org](http://ttbook.org) targeted the "engaged listeners," requiring somewhat more effort for our users while allowing them more control. On [Core Publisher](http://core.digitalservices.npr.org/) sites, such as [wprnews.org](http://wprnews.org), NPR advises a more "traditional" approach in that audio players do little more than reliably play audio files related to the story that is currently being viewed.

There exists, to be sure, a wide array of competing technologies in this space, and the knowledgable presenters on the webinar will have useful information to help us all provide the best possible experience for our users and listeners. I hope you will [join us on Wednesday](http://mediaengage.org/webinars/webinar_details.cfm?wbid=159955).
