---
title: " Bach Meets High Tech in a Wisconsin Public Radio Broadcast Experiment"
date: June 24, 2012, 3:43 pm
author: Matthew Tift
tags: open goldberg
path: bach-meets-high-tech-wisconsin-public-radio-broadcast-experiment
---

[Reprinted from opengoldberg.wpr.org]
 
On June 24, 2012 [Wisconsin Public Radio](http://wpr.org/), in partnership with [Open Goldberg Variations](http://www.opengoldbergvariations.org/) and [MuseScore](http://musescore.com), built on its tradition of innovation.  For the first time in history, radio listeners were able to follow the digital score of a broadcast recording as it was broadcast. 
 
If you missed the opportunity to hear on the radio and watch online the new public domain arrangement of Bach's Goldberg Variations, you can still: 
 
* **Listen** to the recording and **follow** the score [online](http://musescore.com/node/16091).
* **Download** [the recording](http://www.opengoldbergvariations.org), [the score](http://musescore.com/opengoldberg/goldberg-variations) or the [iPad app](http://itunes.apple.com/us/app/open-goldberg-variations/id529060306) for free
* **Follow** [@OpenGoldberg](http://twitter.com/OpenGoldberg) and [@WPR](http://twitter.com/WPR) on Twitter
* **Like** [Open Goldberg Variations](https://www.facebook.com/Open.Goldberg.Variations) on Facebook 