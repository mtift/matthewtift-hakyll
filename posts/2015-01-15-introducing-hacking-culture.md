---
title: "Introducing: Hacking Culture"
date: January 15, 2015, 11:00 am
author: Matthew Tift
image: hackingculture_logo.png
tags: Drupal, free software, hacking culture, Backdrop
path: introducing-hacking-culture
---

I worked at a public radio station for eight years and I sometimes thought it would be fun to have my own show. So perhaps there is a bit of irony since I no longer work in public radio and I am finally going to get my own show!

The newest member of Lullabot's podcast family is [Hacking Culture](https://www.lullabot.com/blog/podcasts/hacking-culture), featuring in-depth interviews with free software advocates. Since it's a show about free software it would probably be more appropriate to call it an "oggcast," but I'm afraid that could cause unnecessary confusion.

Hacking Culture will feature stories about the hacking of culture and the culture of hacking. It will be largely based on topics/projects/people that I find interesting. When I listen to podcasts, I appreciate it when both the interviewer and the person being interviewed know their subject, so each interview I have done has been proceeded by a great deal of research.

The intended audience is anyone who is interested in learning about free software. This show will not be about the latest version of Ubuntu, this week's Linux news, or a series of how-tos. The show will not be technical. More than anything, I am interested in hearing stories.

I have already completed two interviews, and I'm starting with people I know. However, it is my great hope that this show will afford me the chance to interview the people who wrote some of my favorite books.

This inaugural episode of Hacking Culture introduces the idea of "software forking," one of the fundamental characteristics of free software. While Drupal will not be the focus of the show, Drupal plays a prominent role in the first episode, in which I talk with [Nate Haug](https://www.lullabot.com/who-we-are/nate-haug) about [Backdrop](https://backdropcms.org), a Drupal fork. This episode explores a variety of topics, including how Nate got involved in free software development, the philosophical and structural differences between Backdrop and Drupal, similarities that Backdrop shares with other well-known software forks, and today's release of Backdrop 1.0.

I've been working on this project almost every day since November and I'm very excited to finally share it. One of the excellent benefits of working at Lullabot is that I don't have to do all of the work by myself and that many of co-workers are intrepid podcasters with a lot of experience. While I've had advice from numerous people, I'd like to extend a special thanks to Kyle Hofmeyer, Justin Harrell, Dave Reid, and Jeff Eaton.

You can learn more about the show and subscribe at [lullabot.com/hackingculture](http://lullabot.com/hackingculture).