---
title: "Install Tidal on Debian Stretch"
date: May 14, 2018, 6:45 am
author: Matthew Tift
image: tidalcycles-logo.png
tags: Tidal
path: install-tidal-debian-stretch
---

[TidalCycles](https://tidalcycles.org) (or just "Tidal") is a programming language used for [live coding](https://en.wikipedia.org/wiki/Live_coding). I'll leave further details about Tidal for another post, but essentially Tidal facilitates creating music with code. While the Tidal community hopes [to make installing Tidal on GNU/Linux easier](https://github.com/tidalcycles/Tidal/issues/284), I struggled to get Tidal working on Debian stable.

<!--more-->

This post is partly "install guide," partly "moderately-tested brain dump" of what I've learned. The information here might prove helpful for someone struggling to get Tidal working on Debian stretch -- potentially me if I break something. While some of what I'm including here comes from the [Tidal install guide](https://tidalcycles.org/getting_started.html#linux), I contradict the instructions on a few key points. Once I got everything working on one laptop, I repeated the steps below on another laptop -- both running Debian stretch. I apologize for any errors.

More importantly, I welcome feedback! I'd like to either contribute back to the Tidal documentation, create an install script that works for Debian (similar to [linux-install.sh](https://github.com/tidalcycles/Tidal/blob/master/doc/install-linux.sh) in the Tidal repo), or figure out some way to package (or [help package](https://packages.debian.org/buster/libghc-tidal-dev)) Tidal for Debian.

But in the meantime, I suggest installing Tidal like this:

### Step 1: Install JACK

The install guide does not mention the [JACK Audio Connection Kit](http://www.jackaudio.org), but I found JACK necessary to get Tidal working on Debian. JACK, PulseAudio, and ALSA can function in a wide variety of configurations, but I prefer running [JACK alongside PulseAudio](http://www.jackaudio.org/faq/pulseaudio_and_jack.html) (which I learned to do, in part, from some [old Fedora docs](https://docs-old.fedoraproject.org/en-US/Fedora/18/html/Musicians_Guide/sect-Musicians_Guide-Integrating_PulseAudio_with_JACK.html)).

* Install JACK and the QjackCtl GUI by running this command in a terminal:

```
sudo apt install qjackctl pulseaudio-module-jack
```

* Open the newly installed QjackCtl
* Choose your sound card under `Setup` > `Parameters` > `Interface` (the "Analog" version works for me)
* Open the PulseAudio configuration file:

```
sudo gedit /etc/pulse/default.pa
```

* Underneath the line that says "#load-module module-alsa-sink" add these two lines:

```
load-module module-jack-sink
load-module module-jack-source
```

* Save and close

These instructions work for me, but occasionally I still have to manually kill PulseAudio (`pulseaudio -k`) when JACK is running. Also, in some scenarios when JACK is running, I can only control the volume in my headphones using alsamixer, which feels clunky.


### Step 2: Install Git

If you don't already have git installed, install it with:

```
sudo apt install git
```


### Step 3: Install Haskell Cabal

Tidal's [installation directions for GNU/Linux](https://tidalcycles.org/getting_started.html#linux) say that Haskell Stack is a requirement and suggest to use the [haskell-stack](https://packages.debian.org/stretch/haskell-stack) included with your distribution. However, I have Tidal working without Haskell Stack. The haskell-stack package from stretch does not include support for upgrading with `stack upgrade --binary`. Further, running `stack upgrade` or `stack install tidal` (the suggested method to install Tidal) both result in this error:

`AesonException "Error in $.packages.cassava.constraints.flags:failed to parse field packages: failed to parse field constraints: failed to parse field flags: Invalid flag name: \"bytestring--lt-0_10_4\""`

The [Haskell Stack documentation regarding Debian](https://docs.haskellstack.org/en/stable/install_and_upgrade/#debian) suggests running their install script (`curl -sSL https://get.haskellstack.org/ | sh`), and I can confirm that it works. But we use Debian stable, so we don't like to install software that way.

The solution to this problem that I prefer is to use [Cabal](https://www.haskell.org/cabal/). You can install it with this command:

```
sudo apt install cabal-install
```


### Step 4: Install Tidal

Rather than use stack to install Tidal, as the Tidal install directions suggest, install Tidal with Cabal: 

```
cabal update; cabal install tidal
```


### Step 5: Install SuperCollider and the plugins

`sudo apt install supercollider sc3-plugins`


### Step 6: Install SuperDirt (once)

* Open SuperCollider and in the text file add the following text:

```
include("SuperDirt")
```

![SuperDirt](../images/include-superdirt.png)

* With your cursor on that line, type `Shift-Enter` and it will install SuperDirt

![SuperDirt Start](../images/superdirt-done.png)

* From the menu, select `File` > `Open startup file`
* In that file, add this line:

```
SuperDirt.start
```

* Save and exit


### Step 7: (Optional) Install Atom

Once again I am going to contradict the install directions, which state "You need to have four pieces of software, Haskell Stack, Atom, SuperCollider and Git." Atom, like Haskell Stack, is not required, and further down the page it says, "If you prefer using another editor (rather installing Atom as described above), you can" use Sublime Text, Emacs, or Vim. It seems that a lot of people using Tidal run it in Atom, and that works well. If you'd rather run Tidal in another text editor, skip to the next step. If you would like to install Atom, follow these directions. 

* Download the deb from the Atom website:

```
wget --output-document=/tmp/atom.deb http://atom.io/download/deb
sudo dpkg -i /tmp/atom.deb
```

* Start Atom
* From the menu select `Edit` > `Preferences` > `Install` and type "tidalcycles" in the search box
* When it shows up, click `Install`


![Install Tidal in Atom](../images/install-tidal.png)


### Step 8: (Optional) Install an alternative text editor

If you did not install Atom, then this step is a requirement. Follow the [directions for your preferred text editor](https://tidalcycles.org/getting_started.html#editors). I did not test all of the options, but I can confirm that the [vim plugin](https://github.com/munshkr/vim-tidal) works great and that it is highly configurable.


### Step 7: Use Tidal

Now that you have everything installed, here are the steps for using Tidal.

#### First, start JACK in QjackCtl
* As I mentioned above, I'm still getting the hand of JACK. It might be helpful to play some random ogg file you have on your computer and make sure that your sound is working after you start JACK and before doing anything with SuperCollider or Tidal. On more than one occasion, I have thought Tidal was broken and in fact none of the sounds were working on my laptop.

#### Second, start SuperCollider
* When it's done, it should say: "SuperDirt: listening to Tidal on port 57120"
* If that text does not appear, try typing `Ctrl-Shift-L`

#### Third, start your text editor
* If using Atom, create a new, empty file and save it as "test.tidal" (or anything that ends in .tidal)
* Add this line in the text editor: `d1 $ sound "bd sn"`
* With your cursor on the line, start Tidal with `Ctrl-Enter` in Atom, `Ctrl-e` in Vim, etc.
* If you hear beeping, do a little celebration dance
* To make the beeping stop, add another line with the word `hush` and type `Ctrl-Enter`/`Ctrl-e` again.

### In case of trouble
* There is a Rocket Chat channel for asking questions: [talk.lurk.org](https://talk.lurk.org). To get my setup working, I got help from the kind folks there, including the [main Tidal developer](https://en.wikipedia.org/wiki/Alex_McLean). Other than searching through the [Tidal website](https://tidalcycles.org) and the [Tidal GitHub repo](https://github.com/tidalcycles/Tidal), that seems to be the best place to find answers.

I hope you enjoy Tidal!
