---
title: Six Months of Tidal
date: November 2018
author: Matthew Tift
tags: live coding, Tidal
path: six-months-of-tidal
---

*(This article was cross-posted on the [TidalCycles blog](http://blog.tidalcycles.org/six-months-of-tidal).)*

I first [attempted to install TidalCycles](https://matthewtift.com/install-tidal-debian-stretch) about six months ago, in May 2018. I've learned a lot in the past six months, and I'd like to share some of the knowledge I've acquired. If you are new to Tidal or you've tried it out in the past and got stuck, then this unnumbered list of observations is for you.

<!--more-->


### Installation can be tricky

When compared to installing software from a package manager or app store, installing TidalCycles can feel frustrating. The [installation instructions are great](https://tidalcycles.org/getting_started.html) and the people in the [Tidal chatroom](https://talk.lurk.org) are friendly, but it took me a few weeks to really comprehend how TidalCycles, SuperCollider, SuperDirt, Haskell, my text editor, and JACK (because I run GNU/Linux) all work together. Tidal is not a standalone app, it's a Haskell library.

While other free software communities repeatedly push people to "read the manual," the Tidal community seems genuinely happy to help out folks who get stuck. They know installing Tidal can be tricky. Of course many people install Tidal without problems, but some people get stuck and the open issues tagged "[Holy grail](https://github.com/tidalcycles/tidal/issues?q=is%3Aissue+is%3Aopen+label%3A%22Holy+grail%22)" in the Tidal code repository are each about improving the installation process. Until the Holy grail is found don't feel afraid to [ask for help](https://talk.lurk.org) if you encounter problems. 


### Learning tidal is fun

Once you've installed it, learning TidalCycles is really fun! You can do all sorts of mind blowing things with Tidal, so jump in however you like. If you like videos, you could start with an [overview](https://www.youtube.com/watch?v=ff7iMNcMnnY), some [basic tutorials](https://www.youtube.com/playlist?list=PLCBw4oQTHsrD_Pe985ABIMKN-z5Ad54D7), get more [in-depth tutorials](https://www.youtube.com/playlist?list=PLKgxw7RG3hcRHyBFsPr5opr1iu8wbNIgP), or just watch [random videos that people upload to the Internet](https://www.youtube.com/results?sp=CAI%253D&search_query=TidalCycles). If you prefer text, then definitely start with the [Creating Patterns](https://tidalcycles.org/patterns.html) and [Reference](https://tidalcycles.org/functions.html) section on [tidalcycles.org](https://tidalcycles.org). For the least frustration, I'd recommend starting with the guide to [Creating Patterns](https://tidalcycles.org/patterns.html)


### The Tidals they are A-changin'

Once you start learning TidalCycles, you will quickly realize that lots of information is incomplete or outdated, and that even though people are sharing their screens and showing their code, they will do things that won't work for you. It will serve you well to embrace the fact that TidalCycles is evolving.

For instance, this fantastic introduction to "[algorithmic dance culture](https://www.youtube.com/watch?v=nAGjTYa95HM)" is as much about TidalCycles as it is about algoraves, and it was one of the early signals to me that making music with Tidal can be distinct from other kinds of musical activities. But when I tried to recreate what happens in the video (around the 8 minute mark), I did not succeed. In the video, I saw code like this:

```haskell
n "d7 a7 [c6 e] e7" # sound "rash"
```

This code won't work even if you have followed the installation instructions perfectly. If you know even a little bit about Tidal, then you know that, [for now at least](https://www.youtube.com/watch?v=6MpG1LW54_k), Tidal starts with nine connections to the SuperDirt synthesiser, named from `d1` to `d9`, and this code does not look like even the most basic Tidal code examples, such as:

```
d1 $ sound "bd"
```

Secondly, the sound "rash" does not come with SuperDirt. About 3 or 4 months into learning Tidal I came across a link in a [tidal workshop](https://github.com/yaxu/tidal-workshop/blob/2afed43de120277a63d51af613da7832b1189350/musichackspace2.tidal#L3) to a repository with [extra-samples](https://github.com/yaxu/spicule/tree/master/extra-samples) that included [rash](https://github.com/yaxu/spicule/tree/master/extra-samples/rash). It also took a few months to figure out that the talk featured [an experimental editor for TidalCycles](https://github.com/yaxu/feedforward), which doesn't require the use of `d1`, `d2`, etc. 

What is more, you might think that the [Reference](https://tidalcycles.org/functions.html) section of the TidalCycles website would be complete, but it's not. Perhaps you watch a video and see someone using a function such as [`fadeOut`](https://github.com/tidalcycles/Tidal/blob/master/src/Sound/Tidal/UI.hs#L310) or [`spreadChoose`](https://github.com/tidalcycles/Tidal/blob/master/src/Sound/Tidal/UI.hs#L417), and you check the Tidal [Reference](https://tidalcycles.org/functions.html), you might not find what you seek. Of course, this isn't that people using Tidal want to confuse you. The TidalCycles live coding environment is evolving and that's OK. If you find something that doesn't make sense, feel free to look through the [codebase](https://github.com/tidalcycles/Tidal/tree/master/src/Sound/Tidal) for it or ask in the [`#tidal` chatroom](https://talk.lurk.org).


### TidalCycles music is unique

To maximize your enjoyment of Tidal, I suggest becoming aware of your preconceptions about music. Trying to use TidalCycles to recreate existing music can prove to be difficult. In one of my early experiments I attempted to reproduce Steve Reich's [Violin Phase](https://en.wikipedia.org/wiki/Violin_Phase), which seemed perfectly suitable for TidalCycles. It was a rather difficult challenge because I was new to Tidal and trying to make it do something specific. However, once I dropped the goal of trying to create some *thing*, I had a lot more fun. Tidal is not designed to make it easy for you to re-create Beethoven's 5th Symphony, your favorite Radiohead song, or pretty much any music from any period in the history of Western art music. While there is no right or wrong way to Tidal, a lot of folks seems to enjoy their experience more when they just play around with sound.


### Haskell is not imperative 

It is absolutely *not* necessary to learn the programming language Haskell to use Tidal. Unlike imperative languages such as C or Java, Haskell is a pure functional programming language created by a committee of academics. Whether you have zero experience with programming or you know more than a dozen programming languages like me, Haskell can be difficult to learn. And even if you follow all of the examples in [Learn You a Haskell for Great Good!](http://learnyouahaskell.com), [watch a bunch of videos](https://www.youtube.com/playlist?list=PLwiOlW12BuPZUxA2gISnWV32mp26gNq56) with the same information, read [Real World Haskell](http://book.realworldhaskell.org), [the Haskell Book](http://haskellbook.com/), and hang out at your local Haskell user group, that might not be enough to grok the [Tidal code](https://github.com/tidalcycles/tidal). If reading about the [theoretical background behind Tidal](https://github.com/tidalcycles/Tidal/blob/master/old/doc/farm/farm.pdf) makes you want to learn more, then by all mean jump in to the Tidal codebase. I just want to warn you the learning curve might be rather steep.


### Tidal like it's 2018

I have greatly enjoyed learning TidalCycles and getting more involved with the live coding community. I've participated in a wide variety of technology and music communities and somehow this live coding group feels different from any of them. But you should not feel like your are required to get involved in the community. Many people just want to install Tidal and play around with it. For these people, and perhaps for you, it does not matter how other people on the Internet use Tidal or how much they know about Haskell. My goal here is to share some of the what I've learned, but ultimately what you do with Tidal is up to you. I encourage  you to [check it out](https://tidalcycles.org/)!
