---
title: Upcoming Live Coding Events in Minnesota and Wisconsin
date: November 2018
author: Matthew Tift
tags: live coding, Tidal, algorave
---

The worldwide live coding community is a diverse group of people that includes programmers, academics, musicians, dancers, visual artists, and others who like to get together in person to share sounds, visuals, and ideas. Compared to some parts of the world where live coding events happen regularly, the [Midwest](https://en.wikipedia.org/wiki/Midwestern_United_States) does not have a lot of opportunities to experience live coding *live*. However, this week we have two live coding events within relatively close proximity.

<!--more-->

First, [Squirrel Haus Arts](http://www.squirrelhausarts.com) (3450 Snelling Ave) in Minneapolis will be the site for a [TidalCycles meetup](https://www.facebook.com/events/1804914972952546/) on Wednesday (November 28, 2018) from 7 to 9pm. This free event welcomes anyone with an interest in using TidalCycles or live coding. Bring your laptop with [TidalCycles already installed](https://tidalcycles.org/getting_started.html) if you'd like to follow along and make some noise! The plan is to spend an hour introducing [TidalCycles](https://tidalcycles.org/) and demonstrating how to use it, followed by some time to jam on your laptop or hang out.

Then on Saturday (December 1) in Madison, Wisconsin there will be a [live-coding/algorave workshop](https://twitter.com/jake_witz/status/1062768889845686274) at 11am on the University of Wisconsin campus, with an [Algorave](https://communicationmadison.com/event-calendar/2018/12/1/algorave-madison) in the evening at 7:30pm at [Communication](https://communicationmadison.com/). The lineup for the algorave includes Midwest locals [kindohm](http://kindohm.com/), [Spednar](https://spednar.bandcamp.com), [17.2m](http://17point2m.fradkin.com/), [1trainwreck](https://www.youtube.com/watch?v=4t522RFneeg&fbclid=IwAR0fLs82DxP-LKbVmquf5y574QYxFm5X2Ep1japXc6ZlUoO36-6Sc7B0o4o), and [Tarek Sabbar](https://closeupoftheserene.bandcamp.com/album/plastic-bags-8-14). 

If you live near either of these locations, and you are even a little curious about live coding, then I highly recommend you check out these events. To find out more about live coding in advance of these events, check out [toplap.org](https://toplap.org), read about my first [six months using TidalCycles](http://blog.tidalcycles.org/six-months-of-tidal/), or listen to my recent interviews on [Hacking Culture](https://www.hackingculture.org/) with some [well-known live coders](https://toplap.org/live-coding-interviews-on-the-hacking-culture-podcast/).
