---
title: "Upcoming presentations"
date: February 1, 2012, 8:45 pm
author: Matthew Tift
tags: Drupal
path: upcoming-presentations
---

In the coming month or so, I will be giving a number of talks concerning Drupal and public media:

**February 2**  
 "Open source software in public media"  
 Wisconsin Public Radio (Madison, WI)

**Febraury 22**  
[Drupal in Public Media](http://groups.drupal.org/node/197058)  
 Twin Cities Drupal User Group (Minneapolis, MN)

**February 27**  
 "How ttbook.org makes use of Drupal"  
 PRI: Public Radio International (Minneapolis, MN)

**March 7**  
[Drupal Developers Unite! Inaugural Pub Media Drupal Developer's Clinic](http://www.integratedmedia.org/conference/schedule#Drupal)  
 Integrated Media Association (Austin, TX)
