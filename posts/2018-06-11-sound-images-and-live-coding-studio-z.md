---
title: "Sound, Images, and Live Coding at Studio Z"
date: June 11, 2018, 7:25 am
author: Matthew Tift
image: studio-z-stage.jpg
image-alt: Empty stage at Studio Z
tags: live coding
path: sound-images-and-live-coding-studio-z
---

On Friday (June 8) I attended my first live coding event, announced as "[Nada presents Spednar, Rew, and Local Artists](http://www.studiozstpaul.com/blog/nada-presents-spednar-rew-and-local-artists)." This concert felt unlike any music concert I had ever attended. I greatly enjoyed the event and I've been investigating why. What follows recounts my experiences at one live coding event, and does not attempt to characterize all live coding events.

<!--more-->

## Venue

Just finding the venue set the stage for a night of surprises.

I had read Alex McLean's [descriptions of live coding performances](https://dl.acm.org/citation.cfm?doid=2633638.2633647): they take place "late at night with a dancing nightclub audience" or "during the day to a seated concert hall audience" or "in more collaborative, long form performance." I think I may have been expecting a dancing nightclub audience.

This was my first event at [Studio Z](http://www.studiozstpaul.com/) and I had some trouble finding the place. A quick glance at my phone told me the general location, essentially across the street from [CHS field](http://saintsbaseball.com/chs-field1/parking) where a [St. Paul Saints](http://saintsbaseball.com/) baseball game was in progress. I've attended Saints games and I know the area, but when I walked down to the appropriate corner I couldn't find Studio Z. At this point, I'm literally standing on a corner, seeing a familiar and conventional entertainment venue, inhabited by thousands of people, but I'm instead seeking out an unknown venue, filled with strangers participating in a more unconventional performance.

I probably should have read the [directions on the website](http://www.studiozstpaul.com/directions--parking.html), which suggest, "Look for the neon Z in the window!" Indeed, I eventually looked up and saw the glowing Z in the window and simultaneously felt relief and realized that Studio Z isn't a typical concert hall, nightclub, or theater.

Once I entered the correct building, my next challenge was to find Studio Z. Out of habit, I sought out stairs rather than use the elevator, which created more confusion for me because the lobby of the historic Northwestern Building, built in 1919 and now home of Studio Z, has two sets of stairs [going in opposite directions](http://images2.loopnet.com/i2/3GaRgykWxF8xdzN79l9w-OqGiJJm4dc4ZkW7fDpq_Ck/112/image.jpg). Eventually I found the second floor, a door labeled "Suite 200," and I walked in late. When I opened the door, I did not expect to find a room full of people sitting on chairs in the dark.

The disorientation I felt walking into Studio Z was perhaps heightened by the fact that I had come from an more familiar situation. I'm an organizer for the [Twin Cities Drupal Camp](https://2018.tcdrupal.org/) (since it started in 2011) and this was the weekend. On the Friday of the Studio Z concert I had been in "conference mode," talking about technology, learning new tools, and writing code. But even though I had been thinking about coding and music -- I had given a talk earlier in the day about "[Drupal and the music industry](https://2018.tcdrupal.org/session/drupal-and-music-industry-learning-our-success)" -- and I was arriving at an event where people would be creating music with code, when I opened that door I felt surprised.


## The Performance

At this point, it seems prudent to mention my Ph.D. in music history, and that in graduate school I took exams that required me to be prepared to answer essentially any question about any period in the history of Western music. Additionally, I've been performing a variety of music, including "modern music," since I first learned how to play the violin 37 years ago. For example, in college I performed Steve Reich's "[Violin Phase](https://en.wikipedia.org/wiki/Violin_Phase)" on my violin and computer. I have taught a wide array of music courses and worked for numerous music organizations, including the Minnesota Orchestra. I have begun to learn how to live code, reading about the history and theory of live coding and algorithmic music, and watching live coding videos. And yet this concert still managed to affect me. 

Come to think of it, the "stage" in Studio Z didn't look much different from other [videos of live coding on the internet](https://www.youtube.com/watch?v=jiNWS2-zYSM). Someone dressed in black, in this case [Michael Flora](http://michaelmasaruflora.com/), sat at his laptop in a dark room with a screen behind him. Countless videos [on the Internet](https://www.youtube.com/watch?v=fIuqDKzYBzc&list=RDfIuqDKzYBzc) [show](https://www.youtube.com/watch?v=JY8eNSu6HrU) [people](https://www.youtube.com/watch?v=MOWSyygP1O8) [live coding](https://www.youtube.com/watch?v=zlFoKUDB6Y0) [in front](https://www.youtube.com/watch?v=smQOiFt8e4Q) [of screens](https://vimeo.com/132730904). Sometimes with people talking in the background. But on this night at Studio Z, the audience of perhaps 20 people sat in chairs and watched performers on a "stage" (the image at the top of this page depicts the "stage" at Studio Z after the lights came on).

Videos of live coding performances usually show code projected on large screens, in keeping with the [TOPLAP manifesto](https://toplap.org/wiki/ManifestoDraft), which states, "code should be seen as well as heard." I expected to see a lot of code, but in this concert we instead saw a series of intriguing visuals. I recently had read Pelle Dahlstedt's article about "Action and Perception" in *The Oxford Handbook of Algorithmic Music* where he discusses "those who think that a visual performance component is unnecessary, and that modern listeners need to get used to the idea of focusing on sound rather than action." (56) The concert at Studio Z had music and visuals, but no code, which created a fascinating, unfamiliar sensation for me of attending a "music concert" with a desire to see code. I got over it though and enjoyed the visuals.

The five artists -- [Michael Flora](http://michaelmasaruflora.com/) (ScgPlrAoaCuiEdn), [John Keston](http://johnkeston.com/) (Vocalise Sintectica), [Mike Hodnick](http://kindohm.com/) (Kindohm), and [Kevin Bednar](https://spednar.bandcamp.com/) and [Rachel Wagner](http://www.rew.media/Video) (Computational Chiaroscuro) -- said little, if anything during the evening. They performed using a mixture of approaches to audiovisual performance using computational processes. Describing the variety of images we saw seems almost comical, but I'll just say that I recall seeing boxes, squares, glasses of water, industrial equipment, cats with laser vision, people's faces, and much more. Each artist had their own unique style. I could describe the sounds I heard at various times using words like "musical," "improvised," "distorted," "rhythmic," "minimalist," "loud," "quiet," "melodic," etc., but for now I'd rather shy away from the challenge of attempting a meaningful musical analysis. It's not difficult to imagine people describing the performances simply as "weird."

During the concert I noticed feelings of excitement simply because I was experiencing live coding *live*. Before this concert, my live coding experiences consisted of either recorded concerts or live coding by myself (my wife and kids prefer when I use headphones). But at Studio Z I found myself in a room with other people, feeling expectations (or at least I perceived them as expectations) to sit and listen. I could not click pause and wander off when the next thought arose. That's a more familiar feeling to me, comparable to the experience of meditating alone versus in a group. Sitting still for an hour alone in my home is not the same feeling as sitting still in a room with other people in a remote location on a meditation retreat. In both situations, my desire to remain with the group and not negatively affect other people provides additional motivation to sit still and remain concentrated in the present moment.

Overall, this performance confirmed some ideas about live coding that I previously had only understood on an intellectual level. Live coding encourages us to remain focused in the present moment. It's not about the past or the future. When my mind wanders, I have new audio and visual objects that can help bring me back to the present moment. After so many years of training my mind to analyze and critique, I noticed numerous moments during the concert where I felt as if I was simply experiencing the moment

In summary, I experienced an event in an unfamiliar location, with strangers, performing in a unfamiliar way, with unfamiliar expectations. The sheer newness of the experience fascinates me. Like listening to the unpredictable sounds of the wind chimes that hang outside my window on a windy day, I enjoyed the unpredictability of my Friday night. The result of the experience feels beneficial, and I look forward to more live coding.

Below are two recordings from the concert. The first is Kindohm's set and the second is John C.S. Keston:

<iframe width="560" height="315" src="https://www.youtube.com/embed/yVYrKPZi0k0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<br/>

<iframe width="560" height="315" src="https://www.youtube.com/embed/pz0eJ7pcOBA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
