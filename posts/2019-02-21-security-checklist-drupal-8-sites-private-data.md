---
title: A Security Checklist for Drupal 8 Sites with Private Data
date: February 21, 2019 
author: Matthew Tift
tags: Drupal, security
---

*(This article was cross-posted on [lullabot.com](https://www.lullabot.com/articles/security-checklist-drupal-8-sites-private-data).)*

Drupal has a great reputation as a CMS with excellent security standards and a 30+ member [security team](https://security.drupal.org/team-members) to back it up. For some Drupal sites, we must do more than just keep up-to-date with each and every security release. A Drupal site with private and confidential data brings with it some unique risks. Not only do you want to keep your site accessible to you and the site’s users, but you also cannot afford to have private data stolen. This article provides a checklist to ensure the sensitive data on your site is secure.

<!--more-->

Keeping a Drupal 8 site secure requires balancing various needs, such as performance, convenience, accessibility, and resources. No matter how many precautions we take, we can never guarantee a 100% secure site. For instance, this article does not cover vulnerabilities beyond Drupal, such as physical vulnerabilities or team members using [unsafe hardware, software, or networks](https://www.lullabot.com/articles/eight-reasons-why-security-matters-for-distributed-agencies). These suggestions also do not ensure [General Data Protection Regulation (GDPR) compliance](https://www.drupal.org/project/drupal_gdpr_team), a complex subject that is both important to consider and beyond the scope of this checklist.

Rather than systems administrators well-versed in the minutia of securing a web server, the list below targets Drupal developers who want to advise their clients or employers with a reasonable list of recommendations and implement common-sense precautions. The checklist below should be understood as a thorough, though not exhaustive, guide to securing your Drupal 8 site that contains private data, from development to launch and after.

The information below comes from various sources, including the security recommendations on Drupal.org, articles from [Drupal Planet](https://www.drupal.org/planet/) that discuss security, a Drupal security class I took with Gregg Knaddison and his 2009 book [*Cracking Drupal*](http://crackingdrupal.com/), a review a numerous internal Lullabot documents regarding security, and other sources. This list was reviewed by Lullabot's security team. That said, for more comprehensive content and considerations, I recommend learning more about [securing your site](https://www.drupal.org/security/secure-configuration) on Drupal.org and other security sources.

## Infrastructure
* Configure all servers and environments to use [HTTPS](https://www.lullabot.com/articles/https-everywhere-security-is-not-just-for-banks).
* Install the site following the Drupal guidelines to [secure file permissions and ownership](https://www.drupal.org/node/244924) (using the `fix-permissions.sh` included with the guidelines is a quick way to do this).
* Hide important core files that may allow attackers to identify the installed version (the point release) of Drupal, install new sites, update existing sites, or perform maintenance tasks. For example, when using Apache add something like this to your .htaccess configuration file:
```bash
<FilesMatch "(MAINTAINERS|INSTALL|INSTALL.mysql|CHANGELOG).txt">
  Order deny,allow
  deny from all
  Allow from 127.0.0.1  <-- your domain goes here
</FilesMatch>
<FilesMatch "(authorize|cron|install|update).php">
  Order deny,allow
  deny from all
  Allow from 127.0.0.1  <-- your domain goes here
</FilesMatch>
```
* In `settings.php`:
    * Configure a hard-to-guess table name prefix (e.g. `5d3R_`) in the `$databases` array to deter SQL injections (this suggestion comes from [KeyCDN](https://www.keycdn.com/blog/drupal-security)).
    * Check that `$update_free_access = FALSE;` to further prevent using the `update.php` script.
    * Configure trusted host patterns (e.g. `$settings['trusted_host_patterns'] = ['^www\.example\.com$'];`).
    * In each environment (development, staging, and production) configure a [private files directory](https://www.drupal.org/docs/8/core/modules/file/overview#content-accessing-private-files) located outside of the web root.
* If you are running MariaDB 10.1.4 or greater with a XtraDB or InnoDB storage engine on your own hardware (as opposed to a hosting provider such as Linode or AWS), enable [data-at-rest encryption](https://mariadb.com/kb/en/library/encryption/).
* Establish a regular process for code, files, and database off site backup using an encryption tool such as [GPG](https://gnupg.org) or a service such as [NodeSquirrel](http://www.nodesquirrel.com).
* If a VPN is available, restrict access to the administrative parts of the site to only local network/VPN users by blocking access to any pages with “admin” in the URL.
* Note that these recommendations also apply to Docker containers, which often run everything as root out of the box. Please be aware that the Docker Community -- not the Drupal Community or the Drupal Security Team -- maintains the [official Docker images for Drupal](https://github.com/docker-library/drupal).

## Development: Pre-Launch 
* Before launching the site, calibrate security expectations and effort. Consider questions such as, who are the past attackers? Who are the most vulnerable users of the site? Where are the weaknesses in the system design that are "by design"? What are the costs of a breach? This discussion might be a good project kickoff topic.
* Establish a policy to stay up-to-date with [Drupal security advisories and announcements](https://www.drupal.org/security). Subscribe via [RSS](https://groups.drupal.org/node/15254/feed) or email, follow [@drupalsecurity](https://twitter.com/drupalsecurity) on Twitter, or create an [IRC bot](https://en.wikipedia.org/wiki/IRC_bot) that automatically posts security updates. Use whatever method works best for your team.
* Create a process for keeping modules up to date, how and when security updates are applied, and considers the [security risk level](https://www.drupal.org/drupal-security-team/security-risk-levels-defined). [Drupal security advisories](https://www.mydropwizard.com/blog/understanding-drupal-security-advisories-risk-calculator) are based on the [NIST Common Misuse Scoring System (NISTIR 7864)](http://www.nist.gov/itl/csd/cmss-072512.cfm), so advisories marked "Confidentiality impact" (CI) are especially crucial for sites with private and confidential data.
* Establish a process for handling [zero-day vulnerabilities](https://en.wikipedia.org/wiki/Zero-day_(computing)).
* Review content types to ensure that all sensitive data, such as user photos and other sensitive files, are uploaded as private files.
* Implement one or more of the [Drupal modules for password management](https://www.drupal.org/node/598562). The United States National Institute of Standards and Technology (NIST) offers [detailed guidelines about passwords](https://pages.nist.gov/800-63-3/sp800-63b.html). Here are some policies that are easy to implement using submodules of the [Password Policy](https://www.drupal.org/project/password_policy) module:
    * At least 8 characters (Password Character Length Policy module)
    * Maximum of 2 consecutive identical characters (Password Consecutive Characters Policy module)
    * Password cannot match username (Password Username Policy)
    * Password cannot be reused (Password Policy History)
* Consider having your organization [standardize on a password manager](https://www.troyhunt.com/only-secure-password-is-one-you-cant/) and implement training for all employees.
* Consider using the [Two-Factor Authentication (TFA) module](https://www.drupal.org/project/tfa).
* Change the admin username to something other than admin.
* Enable the [Login Security](https://www.drupal.org/project/login_security) module, which detects password-guessing and brute-force attacks performed against the Drupal login form.
* Enable [Security Review](https://www.drupal.org/project/security_review) module, run the Security Review checklist, and make sure all tests pass. Disable this module before site launch, but use this module to perform security reviews each time core or contributed modules are updated.
* Carefully review Drupal user permissions and lock down restricted areas of the site.

## Development: Ongoing
* Perform a thorough and recurring analysis of your site using online tools such as [Qualys SSL Server Test](https://www.ssllabs.com/ssltest/), [Sucuri SiteCheck scanner](https://sitecheck.sucuri.net/), [Unmask Parasites](http://www.unmaskparasites.com), and [securityheaders.com](https://securityheaders.com).
* Carefully review contributed modules to ensure they contain secure code. Prioritize stable releases of modules that are covered by Drupal's [security advisory policy](https://www.drupal.org/node/475848).
* For custom modules, follow advice for [writing secure code](https://www.drupal.org/docs/8/security/writing-secure-code-for-drupal-8).
* Follow guidelines to [preserve user input](https://www.drupal.org/docs/develop/security/why-does-drupal-filter-on-output) and ensure that all [output is sanitized](https://www.drupal.org/docs/8/security/drupal-8-sanitizing-output).
* Build forms using Drupal's [Form API](https://www.drupal.org/docs/8/api/form-api) to help [protect against cross-site request forgeries (CSRF)](https://www.drupal.org/node/178896).
* Check code integrity before each deployment with the [Coder module](https://www.drupal.org/project/coder).
* Use [ESLint](https://eslint.org/) to scan JavaScript for obvious errors.
* Use the [drush](http://www.drush.org./) tools provided by [Database Sanitize](https://www.drupal.org/project/database_sanitize) to sanitize database data for local development.
