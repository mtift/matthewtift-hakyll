---
title: "AMPing up Drupal"
date: February 26, 2016, 4:43 pm
author: Matthew Tift
image: night-lights.jpg
tags: AMP, Drupal
path: amping-drupal
---

*(This article was cross-posted on [lullabot.com](https://www.lullabot.com/articles/amping-up-drupal) and the [AMP project blog](https://amphtml.wordpress.com/2016/03/02/amping-up-drupal/)).*

Today we are proud to announce a [new Drupal 8 module](https://www.drupal.org/project/amp) that provides support for the Accelerated Mobile Pages (AMP) Project. The AMP Project is a new open source initiative which drastically improves the performance of the mobile web. In January 2016 Lullabot and Google started working together to create the Drupal [AMP module](https://www.drupal.org/project/amp). A beta version of AMP module for Drupal 8 is available immediately, and we are starting work on a Drupal 7 version of the module that will be available in mid-March.

In many cases, the mobile web is a slow and frustrating experience. The AMP Project is an open source initiative that embodies the vision that publishers can create mobile optimized content once and have it load instantly everywhere. When AMP was first introduced last October, many commentators immediately compared it to Facebook's Instant Articles and Apple's News app. One of the biggest differentiators between AMP and other solutions is the fact that AMP is open source.

[AMP HTML](https://github.com/ampproject/amphtml) is, essentially, a subset of HTML. And it really [makes the web fast](http://adage.com/article/digital/google-amp-faster-mobile-web/302837/). AMP HTML is designed to support smart caching, predictable performance, and modern, beautiful mobile content. Since AMP HTML is built on existing web technologies, publishers continue to host their own content, craft their own user experiences, and flexibly integrate their advertising and business models -- all using familiar tools, which will now include Drupal!

One of the most touted features of Drupal is its flexibility, so making Drupal produce AMP HTML has required a lot of careful consideration of the design approach. To make Drupal output AMP HTML, we have created an [AMP module](https://www.drupal.org/project/amp), [AMP theme](https://www.drupal.org/project/amptheme), and a [PHP Library](https://github.com/Lullabot/amp-library).

When the [AMP module](https://www.drupal.org/project/amp) is installed, AMP can be enabled for any content type. At that point, a new AMP view mode is created for that content type, and AMP content becomes available on URLs such as node/1/amp or node/article-title/amp. We also created special AMP formatters for text, image, and video fields.

The [AMP theme](https://www.drupal.org/project/amptheme) is designed to produce the very specific markup that the [AMP HTML standard](https://www.ampproject.org/) requires. The AMP theme is triggered for any node delivered on an /amp path. As with any Drupal theme, the AMP theme can be extended using a subtheme, allowing publishers as much flexibility as they need to customize how AMP pages are displayed. This also makes it possible to do things like place AMP ad blocks on the AMP page using Drupal's block system.

The PHP Library analyzes HTML entered by users into rich text fields and reports issues that might make the HTML non-compliant with the AMP standard. The library does its best to make corrections to the HTML, where possible, and automatically converts images and iframes into their AMP HTML equivalents. More automatic conversions will be available in the future. The PHP Library is CMS agnostic, designed so that it can be used by both the Drupal 8 and Drupal 7 versions of the Drupal module, as well as by non-Drupal PHP projects.

We have done our best to make this solution as turnkey as possible, but the module, in its current state, is not feature complete. At this point only node pages can be converted to AMP HTML. The initial module supports AMP HTML tags such as [amp-ad](https://www.ampproject.org/docs/reference/amp-ad.html), [amp-pixel](https://www.ampproject.org/docs/reference/amp-pixel.html), [amp-img](https://www.ampproject.org/docs/reference/amp-img.html), [amp-video](https://www.ampproject.org/docs/reference/amp-video.html), [amp-analytics](https://www.ampproject.org/docs/reference/extended/amp-analytics.html), and [amp-iframe](https://www.ampproject.org/docs/reference/extended/amp-iframe.html), but we plan to add support for more of the [extended components](https://www.ampproject.org/docs/reference/extended.html) in the near future. For now the module supports Google Analytics, AdSense, and DoubleClick for Publisher ad networks, but additional network support is forthcoming.

While AMP HTML is already being served by some of the biggest publishers and platforms in the world — such as The New York Times, The Economist, The Guardian, BBC, Vox Media, LinkedIn, Pinterest, and many more! — you don't have to be a large publisher to take advantage of AMP. Today, any Drupal 8 site can output AMP HTML using the [AMP module](https://www.drupal.org/project/amp). We invite you to try it out and let us know what you think!

Finally, if you are interested in this topic and want to learn more about publishing AMP with Drupal, please leave a comment on our [DrupalCon New Orleans proposal](https://events.drupal.org/neworleans2016/sessions/amping-drupal).