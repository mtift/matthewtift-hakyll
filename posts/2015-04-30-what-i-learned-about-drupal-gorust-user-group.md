---
title: "What I Learned about Drupal at a Go/Rust User Group"
date: April 30, 2015, 12:11 pm
author: Matthew Tift
tags: Drupal, drupal planet, go, rust, community
path: what-i-learned-about-drupal-gorust-user-group
---

Last night I had the pleasure of attending the first joint meeting of [Minnesota Go and Rust users](http://www.meetup.com/golangmn/events/221703765), and arrived home inspired -- not just by Go and Rust, but also about Drupal and its community.

Attending a wide variety of local meetups is something I really enjoy. Since I moved back to the Twin Cities area nearly seven years ago, I have attended meetings related to [PHP](http://www.meetup.com/mn-php), [Linux](http://www.mn-linux.org), [Concrete5](http://www.meetup.com/Concrete5-Minnesota), [NodeJS](https://www.joyent.com/noderoad/cities/minneapolis-6-17-2014), [.NET](http://www.meetup.com/tcdnug), [JavaScript](http://www.meetup.com/JavaScriptMN), [Flash](processing-processing), and more. Each of these groups has a unique community and way of doing things, although just about every one of them involves pizza, beer, and predominantly white, male participants.

Given the fact that Rust and Go are both C-like low-level languages, it might not come as a surprise that this Go/Rust user group meeting was quite technical. There were three presentations and lots of terminals on the big screen. The first speaker introduced Rust by illustrating the process for generating the Fibonacci sequence. The end result looked something like this:

 ```
struct Fibonacci {
    curr: u32,
    next: u32,
}

impl Iterator for Fibonacci {
    type Item = u32;
    fn next(&mut self) -> Option {
        let new_next = self.curr + self.next;

        self.curr = self.next;
        self.next = new_next;

        Some(self.curr)
    }
}

fn fibonacci() -> Fibonacci {
    Fibonacci { curr: 1, next: 1 }
}

fn main() {
    for i in fibonacci().take(30) {
        println!("{}", i);
    }
}
```

At other local meetups I have attended, this could have made for a dry, boring talk. However, at this Go/Rust meetup, each aspect of this Fibonacci program was followed by remarkably sophisticated audience questions and answers about language architecture, Rust's underlying assumptions, and frequent references to how Rust compared to C, Python, Go, and other languages. I learned a lot and found it very entertaining. (Incidentally, while I personally prefer Go, I think this is a great article comparing [Go and Rust](http://jaredly.github.io/2014/03/22/rust-vs-go/index.html) -- the author likes Rust).

I have attended a lot of DrupalCon and DrupalCamp sessions over the past five years or so, and I don't recall any of them feeling like this Go/Rust meetup. Perhaps there are a lot of more technical sessions and I just avoided them. Perhaps sessions like the (Drupal) Core Conversations are very technical, but I just don't notice it as much. Whatever the case, these Rust and Go talks got me thinking about Drupal and its community.

Drupal meetings, especially in the [Twin Cities](https://groups.drupal.org/twin-cities), tend to be focused on bringing in new users and not leaving anyone out of the conversations. That often means less technical presentations. Furthermore, every year at the Twin Cities DrupalCamp we have made it an explicit goal to welcome new users into our community.

[Getting off the Drupal island](http://www.garfieldtech.com/blog/off-the-island-2013) to go to this Go/Rust group was a nice reminder of just how much Drupal lets me do without ever having to think about these low-level issues. For comparison, in PHP (Drupal is written in PHP), we might do something more simpler looking to create the Fibonacci series:

 ```
$fib = [1,0];
for ($i = 0; $i < 30; $i++) {
  $next = array_sum($fib);
  array_shift($fib);
  array_push($fib,$next);
  echo $next.', ';
}
```

Or perhaps more relevant is the fact that I can just spin up a Drupal blog for one of my friends or one of my kids, easily download themes to radically change the appearance, and quickly add sophisticated integrations without writing code. And at that point get on with my hacking, or not. My code, my data, my content. Sometimes I lose track of that when I'm working on a project with teams of people I've never met, testing my code in ways I never would have anticipated, making spectacularly complex, cool websites.

The Go/Rust meetup had the unexpected effect of reminding me that Drupal can be very laid-back and non-technical, and that the web can be the same. Not all websites will need the sophistication of Drupal 8's [fancy new configuration system](http://drupal8cmi.org), and its ability to facilitate development, testing, and production environments. Drupal allows me to come home, emboldened by a meetup, and quickly jot down my unfiltered, half-baked thoughts for all the world to see, without having to think about the complexities of immutable references or garbage collection. This Drupal site lets me spill out these ideas in short order.

As Drupal becomes an increasingly capable and accommodating entrance onto the wonderfully diverse information superhighway, it's nice to be reminded once in a while of the fact that Drupal also can do a darn good job of hiding complexity and letting us get on with creating and managing our content of all shapes, sizes, and qualities.