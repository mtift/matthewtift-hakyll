---
title: "Public Music for Public Media: An Introduction"
date: June 1, 2012, 9:44 am
author: Matthew Tift
image: score.jpg
tags: public media
path: public-music-public-media-introduction
---

Open-source music is a [hot](http://boingboing.net/2012/05/28/opengoldberg.html) [topic](http://entertainment.slashdot.org/story/12/05/29/0015231/open-source-bach-project-completed-score-and-recording-now-online) right now, and it is in the best interest of public media stations to help promote it. This post is meant to serve as an overview of open-source music, and in the coming weeks -- leading up to a [special broadcast at Wisconsin Public Radio on June 24](http://opengoldberg.wpr.org/) -- I will explore this topic in more depth.

### Consider the history of music distribution

In 1498, the Venetian government granted Ottaviano Petrucci the exclusive rights to print and sell music, for 20 years, in the Venetian Republic. In his application Petrucci said that he had "at great expense and with most watchful care, executed what many, not only in Italy but also outside of Italy, have long attempted in vain, which is, with the utmost convenience, to print Figured Music."<sup>1 </sup>

Half a century after Gutenberg's press, Petrucci's editions would rather suddenly expose music to a much larger section of the population, as musical scores were previously an expensive undertaking, with each piece of music copied by hand, and mostly the purview of the nobility and church. Petrucci's editions signaled a monumental change in the history and distribution of music.

### The work has already begun

More than 500 years later, and much in the manner of Petrucci, there are numerous attempts to expose classical music to new audiences. For example, the [International Music Score Library Project](http://imslp.org/) (IMSLP) -- also known as the Petrucci Music Library -- is building a sizeable online library of public domain music scores. The [Mutopia Project](http://www.mutopiaproject.org/), whose slogan is "free sheet music for everyone," has similar goals. Another related example is [Musopen](http://musopen.org/), which works to improve "access and exposure to music by creating free resources and educational materials."

Like Petrucci, the people working on these projects are creating new audiences for music by making music more available. Before Petrucci, music scores were essentially unavailable to the public. And before projects like IMSLP, music scores cost money.

If the above projects are the equivalent to giving away the sheets printed by Petrucci, then there are other efforts that are more akin to giving away Petrucci's press, as well as his templates. Sheet music on [MuseScore.com](http://MuseScore.com) is available not only as a PDF, but also in various open-source formats. This approach allows visitors to do much more than look at scores -- they can, for example, download the scores in the open [MusicXML](http://en.wikipedia.org/wiki/MusicXML) form and change them using the free and open-source [MuseScore](http://musescore.org/) music composition and notion program.

These open formats -- especially when combined -- allow listeners to create new relationships with the music, to see as well as hear, to create as well as consume. These technologies allow listeners to become more than listeners by enabling wholly new manners of interaction. These are new kinds of "[musicking](http://books.google.com/books/about/Musicking.html?id=1lOx9nr0aHkC)."

### So what does this mean for public media?

These technologies could benefit our listeners in numerous ways. For example, many classical music stations publish their playlists online. What if we could take this a step further? Imagine that you are listening to your favorite classical radio station and you hear a string quartet playing something especially beautiful. It is one thing to find out the name of the quartet and composer of the piece. It would be quite another thing if you could go to that station's website, download or stream the world-class recording you hear (for free) and download or view the score (for free). That is already starting to sound rather special.

Better yet, let's say you knew that the particularly beautiful moment occurred a few minutes near the end of the work, so you don't just find the name of the piece, you listen to the recording online and you watch as the score follows along (a bit like [this](http://musescore.com/opengoldberg/goldberg-variations)). So you jump near the end of the piece, and you not only hear that beautiful moment again, but you get to see the notes. Even if you do not read music, you would suddenly be able to see the music like never before. And as a bonus you would have a copyright-free soundtrack to download for your next cat video on YouTube.

And yet, that is only the beginning. These technologies would allow public media stations to form new (or strengthen existing) partnerships. What music teacher would not love to have a free catalog of scores and recordings available to their students? The scores their students downloaded would not merely be for study because they could open the file and make changes to the score in the music notion program of their choice, such as [Finale](http://www.finalemusic.com/finale/features/sharingyourmusic/musicxml.aspx), [Sibelius](http://www.us.sibelius.com/products/sibeliusedu/faq/index.html#8), or [MuseScore](http://musescore.org/en/handbook/file-format).

Because the scores are available in MusicXML, there is nothing that would prevent someone writing software that allows real-time, group editing, a la [Apache Wave](http://en.wikipedia.org/wiki/Apache_Wave) or [Etherpad](http://etherpad.org/). The open-source nature of these music-related projects imbues them with special powers that public media can amplify.

The influential American historian Lawrence Levine wrote of a "shared public culture that once characterized the United States."<sup>2</sup> Promoting open-source music scores and recordings, and creating new sites of collaboration, might help bring together people that would not otherwise wake up in the morning and decide they want to contribute to an open-source project.

### Please Listen on June 24

Here at Wisconsin Public Radio, in partnership with the [Open Goldberg project](http://www.opengoldbergvariations.org/) and [MuseScore](http://musescore.com), we will be conducting a bit of an experiment later this month on June 24. We will be [broadcasting the Open Goldberg Variations](http://opengoldberg.wpr.org/) and we think it will be the first time in history that radio listeners will be able to follow the digital score of a broadcast recording as it is being played on air.

If it is a success, I will make the case that WPR should do even more, such as sponsor another open-source recording or bring our listeners together to help create a new open-source score. I have so many ideas I can hardly contain my excitement. But first this broadcast needs to demonstrate interest.

I invite you to please tune in -- on air or [online](http://opengoldberg.wpr.org/) -- on June 24 and take part in radio history.

**Notes**  
 1. Gustave Reese, "The First Printed Collection of Part-Music," *The Musical Quarterly* 20 (1934): 40.  
 2. Lawrence Levine, *Highbrow/Lowbrow: The Emergence of Cultural Hierarchy in America* (Cambridge: Harvard University Press, 1988), 9.
