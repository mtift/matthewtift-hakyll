---
title: "Exploring IMSLP"
date: August 21, 2012, 8:54 pm
author: Matthew Tift
image: Eroica_Beethoven_title.jpg
tags: open source music, IMSLP
path: exploring-imslp
---

Over the weekend, I was sitting in my office watching an episode about [Beethoven's 3rd ("Eroica") symphony](http://video.pbs.org/video/1295282213) from the excellent PBS series [Keeping Score](http://www.keepingscore.org/), when Michael Tilson Thomas said something about the symphony that intrigued me. I could not easily locate my copy of the orchestral score, so rather than continue my search in another room, I did something that I find myself doing more and more these days -- searching on the International Music Score Library Project ([IMSLP](http://imslp.org/)) website. As often happens on IMSLP, which is also known as the Petrucci Library, I found [exactly what I wanted](http://imslp.org/wiki/Symphony_No.3,_Op.55_(Beethoven,_Ludwig_van)).

Don't get me wrong. The Keeping Score project is excellent. The site has some fancy tools for exploring excerpts from the score of the Eroica, complete with audio and visual guides. It's quite impressive.

But if you want to listen to a complete recording of the Eroica and follow along with a score, IMSLP has all that you need. Not only does IMSLP have a score of the Eroica, it has [seven different versions](http://imslp.org/wiki/Symphony_No.3,_Op.55_(Beethoven,_Ludwig_van)) of the full score (from as early as 1809), as well as various parts, arrangements, and transcriptions. It also has a number of recordings, not from Leonard Bernstein conducting the New York Philharmonic, but from the Czech National Symphony Orchestra.

The scores and recordings on IMSLP are free and legal. This Wikipedia-inspired site allows users to upload [public domain scores](http://imslp.org/wiki/Public_domain), which can be freely distributed. While copyright laws differ from country to country, in the United States works published before 1923 are public domain. In other words, IMSLP hosts more than 100,000 scores by composers such as [Bach](http://imslp.org/wiki/Category:Bach,_Johann_Sebastian), [Beethoven](http://imslp.org/wiki/Category:Beethoven,_Ludwig_van), and [Brahms](http://imslp.org/wiki/Category:Brahms,_Johannes), rather than copyrighted works by contemporary composers. If you are looking for more information about the legality of this project, the New York Times published [a helpful article](http://www.nytimes.com/2011/02/22/arts/music/22music-imslp.html) that explains more.

I can think of at least five different uses for IMSLP that might not be readily apparent:

1. **Find a score**. In addition to searching for a score, IMSLP also provides a [forum](http://imslpforums.org/) where users can request a score be added to the collection, request new features on the IMSLP website, and much more. One person (self-described as "not a musician") shared [on the forum](http://imslpforums.org/viewtopic.php?f=35&t=6332) how seeing the scores from IMSLP facilitated a new appreciation of Mozart's music.
2. **Remix a recording**: For those who were inspired by TTBOOK's excellent episode on [remix culture](http://ttbook.org/book/remix-culture) or Radiolab's [remix contest](http://www.indabamusic.com/opportunities/wnyc-radiolab-remix-contest) that would like to try remixing, but would prefer to use legal recordings, IMSLP is a great place to find all kinds of sounds.
3. **Remix a score**: If you want to be like Jad Abumrad, and do your own [remix of Terry Riley's *In C*](http://www.in-c-remixed.com/), you can find a copy of the score for [*In C* on IMSLP](http://imslp.org/wiki/In_C_(Riley,_Terry)) to get started.
4. **Learn**. In addition to hosting scores, recordings, and a forum, there is also a free [IMSLP Journal](http://imslpjournal.org/), which contains articles intended for a general audience, and without too much music jargon.
5. **Be romantic**: For those less musical, a sweet way to say "I love you" is to print out an original score of someone's favorite music and use it as wrapping paper for a small gift.

In the final decades of the 20th century, music scholars grew increasingly incredulous toward grand narratives that featured "great works by great men." With the advent of the Web -- and tools such as IMSLP -- the work of disrupting these grand narratives can be opened up to those without the academic credentials of a music scholar. Access to online [music libraries](http://imslp.org/wiki), [journals](http://www.doaj.org/doaj?func=subject&cpid=6), [books](http://www.digitalculture.org/hacking-the-academy/), and other information is increasing. While it was possible in the past, websites such as IMSLP make it a lot easier and invite everyone to participate.

If you do happen to find something interesting on IMSLP, and you create a free blog on a site such as [Drupal Gardens](http://www.drupalgardens.com/) or [Wordpress](http://wordpress.org/), IMSLP [wants to know about it](http://imslpjournal.org/are-you-a-classical-music-or-culture-blogger/) and might even feature it [on their website](http://imslpjournal.org/).
