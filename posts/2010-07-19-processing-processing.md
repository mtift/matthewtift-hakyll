---
title: "Processing Processing"
date: July 19, 2010, 5:43 pm
author: Matthew Tift
tags: processing
path: processing-processing
---

In June 2010 I attended [Flashbelt](http://www.flashbelt.com/) in Minneapolis, Minnesota. This annual conference focuses “[around Adobe Flash and related technologies](http://www.flashbelt.com/#/about/)." Two of the speakers that I found most informative, [Jeremy Thorpe](http://www.blprnt.com/) and [Wes Grubbs](http://www.devedeset.com/), seemed to focus not so much on Flash, but on their use of a “related technology” called [Processing](http://processing.org/).

Just a few weeks later I am finding Processing useful to help me understand the large amounts of data to which I have access: membership data, underwriting data, major giving data, pledge data, web analytics data, etc. Sure, I could create graphs with a spreadsheet or other simple interfaces. However, I would like to see data to help gain a different perspective. We have so much data that sometimes it would be nice to pick out certain pieces and look more closely.

I have examined a few other data visualization tools such as [Gephi](http://gephi.org/), [Protovis](http://vis.stanford.edu/protovis/), and [Parallel Sets](http://code.google.com/p/parsets/). While these tools look quite promising, Processing seems to be backed by a much larger community, and the [literature about Processing](http://processing.org/learning/) seems to be much more vast (which suits me).

One of my first forays into Processing started with a question about comparing data from two different sources, our website and our membership database. This certainly did not mark the first time that I wanted to compare data from disparate sources. I wanted to know how a particular web initiative affected the number of pledges at a certain level -- to help my organization make informed choices. These days I’m reading more and more blogs -- such as [datavisualization.ch](http://datavisualization.ch/), [eagereyes.org](http://eagereyes.org/), [infosthetics.com](http://infosthetics.com/) -- and books about visualizing data, and I am well aware that there may be holes in my methodology.

That said, in this case I used Processing to display two types of data over a ten-year period (in this example I am using fake data). Although I’ve done a fair amount of programming, it still took me quite a while -- even following along with Ben Fry's excellent book, [Visualizing Data: Exploring and Explaining Data with the Processing Environment](http://amzn.com/0596514557) -- to get this first project working how I wanted it. I am hoping that time invested now will prove useful for my next question to be answered with Processing. In other words, I didn't do this project just to answer the question, I also did this project to help learn Processing.

Part of what I like about using Processing is that I can control every element: fonts, colors, width, height, labels, etc. Processing can create Windows apps, Mac apps, Linux apps, and interactive web apps. The app I shared with my coworkers is interactive and they can use their mouse to hover over any data point. The image above shows my (scrambled) results.

I realize this may not be mind blowing – I made a graph with two lines. Yippie. In the coming days, I plan to show other data that I have “visualized” with Processing. It probably won't be nearly as cool as what Jer Thorpe did with Processing for his recent visualization for [Wired UK](http://blog.blprnt.com/blog/blprnt/wired-uk-barabasi-lab-and-big-data) or any of the other [exhibitions on Processing.org](http://processing.org/exhibition/). Instead, I hope to show a few uses for Processing that might be relevant for people working in public radio or fundraising.
