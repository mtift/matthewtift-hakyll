---
title: "From Public Radio to Public Media"
date: July 30, 2010, 2:31 am
author: Matthew Tift
tags: public media, public radio
path: public-radio-public-media
---

The trend in public radio is to move away from the term “public radio” and, in many cases, embrace the term “public media.” For instance: the Public Radio Development and Marketing Conference recently became the Public Media Development and Marketing Conference; Louisville's Public Radio Partnership [became Louisville Public Media](http://prpd-news.blogspot.com/2008/03/name-change-in-louisville.html); American Public Media calls itself "the second largest producer and distributor of public radio programming"; and, of course, National Public Radio [changed its name](http://www.washingtonpost.com/wp-dyn/content/article/2010/07/07/AR2010070704578.html) to NPR.

<div>While there must be countless private discussions, this name change seems to be occurring without much public discussion. I have not found many thorough investigations into this change of terminology, although I can imagine they exist somewhere. Then again, my point here will not be to discuss the existing literature, but rather to offer a few of my own observations.

To some extent, this name change makes a lot of sense. What used to be public radio "programming" has now become "content" delivered on the Internet, our phones, music players, and many other devices that we would not think of as radios. We are embracing these new media, so we wonder why we would we hang on to the term radio.

Radio, as I understand it, is a single medium. Listeners listen to a radio station. Public media, on the other hand, consist of multiple mediums. Increasingly we deliver our public radio content on any device. With the new media, we interact. In addition to having listeners and members, we are now engaging with consumers, visitors, viewers, readers, tweeters, etc. This new terminology creates some fascinating, sometimes awkward, discussions in the hallways and webinars of a “public radio station” (e.g., it is not just our listeners that visit our websites). Moving from public radio to public media is moving from a single, identifiable thing to a bunch of other things that are not necessarily related.

This situation presents both challenges and opportunities for public radio corporate development representatives (the sales team). It creates new staffing questions because some people know radio really well and they might not know a heck of a lot about bit rates, JPEGs, animation, impressions, click through rates, and all of the new ways to sponsor these new media.

I don't want to suggest that this is necessarily a bad thing. In his book, [Convergence Culture: Where Old and New Media Collide](http://amzn.com/0814742815), Henry Jenkins points out that “old media never die -- and they don't even necessarily fade away” (13). Jenkins reminds us that old media coexist with new media, using his theory of “convergence” to describe the recent changes in media, technology, delivery mechanisms, etc. He describes these changes not just as technological, but also as changes in relationships. While Jenkins does not focus on public media specifically, his comments have helped me understand and embrace these changes, reminding me once again that my job as an information technology professional in a public radio station is not merely to understand and deploy new technologies -- I need to continually investigate these new relationships as well as these areas of convergence.

 </div>
