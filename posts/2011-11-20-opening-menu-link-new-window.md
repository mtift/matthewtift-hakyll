---
title: "Opening a menu link in a new window"
date: November 20, 2011, 7:16 pm
author: Matthew Tift
tags: Drupal
path: opening-menu-link-new-window
---

In Drupal, it seems like it should be easy to configure external menu links to open in a new window, but I could not find clear instructions for doing so in Drupal 7. If this needs to be done in numerous places, the easiest way to get this done might be to install the External Links module. However, for a single link (menu item) I found the simplest and quickest way to get this done was to implement hook\_menu\_link\_alter():

```
  function mymodule_menu_link_alter(&$item) {
    if ($item['link_path'] == 'http://example.com') {
      $item['options']['attributes']['target'] = '_blank';
    }
  }
```

That's it. I hope someone finds this helpful.
