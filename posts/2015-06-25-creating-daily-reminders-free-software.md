---
title: "Creating Daily Reminders with Free Software"
date: June 25, 2015, 8:52 am
author: Matthew Tift
tags: zenity, free software
path: creating-daily-reminders-free-software
---

Many of us who build websites for a living need to log the hours we work each day. It can be easy to occasionally forget a day. There are some interesting solutions to this problem, such as time trackers like [Toggl](https://toggl.com/) or [Freckle](https://letsfreckle.com/) or email reminder applications such as [Boomerang for Gmail](http://www.boomeranggmail.com).

But what about a free/libre solution? There are a variety of tools that could be used for such a purpose, such as [remind](https://packages.debian.org/jessie/remind) or [gxmessage](https://packages.debian.org/jessie/gxmessage), or even simply setting up daily emails with [Sendmail](http://en.wikipedia.org/wiki/Sendmail).

My preferred solution to this problem is to use a program called [Zenity](http://en.wikipedia.org/wiki/Zenity). Zenity is a [GNOME program](https://wiki.gnome.org/action/show/Projects/Zenity) that enables a shell script to create GTK+ dialog boxes. A Zenity Hello, World! program, run from the command line, might look something like this:

 ```
zenity --info --text "Hello, World!"
```

In this example, the `--info` flag is a signal to display an info dialog and `--text` sets the dialog text to “Hello, World!”.

<div class="figure"> ![](../images/hello-world.png)</div>A gentle reminder about logging hours might look like this:

 ```
zenity --question --text="Did you enter your hours?" --ok-label="Yes" --cancel-label="No"
```

Here the `--question` flag creates a question dialog with the `--text` value. The other two flags change the labels of “OK” and “Cancel” to “Yes” and “No”.

<div class="figure"> ![](../images/enter-hours.png)</div>Combining Zenity with cron and bash allows for all kinds of possibilities. To create a daily reminder, the steps might go something like this:

**1. Create a bash script somewhere in your $PATH (I like to keep my personal scripts in ~/bin):**

 ```
#!/bin/bash

CONTINUE=1

while [ $CONTINUE -eq 1 ]; do

  # Wait 10 minutes.
  sleep 10m

  # The response to a zenity question is in the $? variable.
  CONTINUE=`zenity --question --text="Did you enter your hours?" --ok-label="Yes" --cancel-label="No" --display=:0.0; echo $?`;

done
```

This script will display this message, and then display it again 10 minutes later until “Yes” is clicked. This starts by setting a variable called “CONTINUE” to 1, and as long as CONTINUE equals (`-eq`) 1, it will wait 10 minutes (`10m`) and then display the question. If “Yes” is clicked, CONTINUE will be set to zero and the program will complete. If “No” is click, CONTINUE will still be 1 and the program will wait ten minutes before displaying the question again. When using `--question` zenity stores the response in a `$?` variable, which is why we assign the value of `$?` to CONTINUE (`echo $?`).

**2. Add line to .bashrc:**

 ```
# Allow use of zenity in cron jobs (xhost controls user access to the X server)
xhost local:USERNAME > /dev/null
```

There are some [issues with Zenity and cron](http://promberger.info/linux/2009/01/02/running-x-apps-like-zenity-from-crontab-solving-cannot-open-display-problem), so this line is needed for the cron job to work. Replace USERNAME with your username.

**2. Create a cron job to run the script twice a day:**

 ```
40 8,16 * * 1-5 /home/USERNAME/bin/daily-reminder.sh
```

For most Linux (and Mac) systems, you can just type `crontab -e` and then enter this line. This will display the reminder every week day (`1-5`) at 8:50am and 4:50pm (`40 8,16`). Remember that the bash script starts by waiting 10 minutes, so in this case set the time for 8:40 and 16:40.

I have been using this system for quite a while, and it works very well for me. It could also be modified to be used as a simple replacement for programs like [RSIBreak](https://userbase.kde.org/RSIBreak), for example displaying a popup message every 20 minutes for 20 seconds reminding yourself to take a break, look away from your screen, and move around a little bit.
