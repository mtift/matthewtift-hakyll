---
title: "Drupal Core Development and Fundraising"
date: February 17, 2014, 8:39 am
author: Matthew Tift
image: development-masterplan.jpg
tags: Drupal, fundraising, drupal planet
path: drupal-core-development-and-fundraising
---

Over the past couple of years, the Drupal community has been hard at work trying to confront a variety of challenges concerning [Drupal core development](https://drupal.org/contribute/development), especially issues like [burnout](http://london2011.drupal.org/coreconversation/1700-1730-burnout) and [funding](https://www.lullabot.com/blog/podcasts/drupalizeme-podcast/17-funding-drupal-core-development). People like [heyrocker](http://heyrocker.com/funding-cmi), [YesCT](http://yesct.net/targeting), and [Alex Pott](http://alex.vit-al.com/node/16) have been especially outspoken. I have been paying close attention to these discussions as well as engaging in conversations with some of the key individuals involved.

[Until last week](broadcasting-bot), I worked in the nonprofit sector and I have been struck by the similarities between code contributors to Drupal core and the monetary contributions to nonprofit organizations. I do not think it is a coincidence that that nonprofit fundraising departments are also called *development* departments or that both Drupal and nonprofits require *contributions*.

What follows is a broad discussion of Drupal core development through the lens of current fundraising practices. It will unfortunately gloss over the important contributions of countless individuals and organizations, especially in the contributed module space. It is my hope that through this rubric we might develop an increased appreciation of the current state of [Drupal core development](https://drupal.org/node/717162).

Membership
----------

For many nonprofits, individual gifts are the lifeblood of the organization and collectively are often the largest single source of revenue.

Drupal's "membership department" (my phrase, not theirs) consists of individuals like [YesCT](https://drupal.org/user/258568), [Cottser](https://drupal.org/user/1167326), [xjm](https://drupal.org/user/65776), and [ZenDoodles](https://drupal.org/user/226976). These people take it upon themselves to help other people contribute to Drupal core by organizing frequent code sprints, helping folks climb the [Drupal ladder](http://drupalladder.org/), answering questions and offering assistance during [core office hours](https://drupal.org/core-office-hours), and much more. Like many of the people I know that work in nonprofit membership, these people often project a seemingly endless supply of patience.

Much like many nonprofits that are shifting their strategy from getting one-time contributors to attracting sustainers -- that contribute monthly donations -- the concerted efforts to grow the Drupal community of core contributors is succeeding. Not only is the number of [contributors to Drupal 8](http://ericduran.github.io/drupalcores/index.html) already more than twice the number that [contributed code to Drupal 7](http://www.knaddison.com/drupal/contributors-drupal-7-final-numbers), the number of people who have contributed more than one patch to Drupal 8 has also grown considerably.

Corporate Support
-----------------

For nonprofits, corporate support can sometimes mean getting nice things, such a new building or an endowed professorship. Corporations like to be recognized for their contributions.

In Drupal, it would seem that we are rather healthy in this area, too. We have created a situation in which companies want to contribute back. In the contributed module space, for example, we have nice things like [Drupal Commerce](https://drupal.org/project/commerce), [Workbench](https://drupal.org/project/workbench), [Backup and Migrate](https://drupal.org/project/backup_migrate), and so much more. We also have numerous companies that encourage their employees to work on Drupal for a certain percentage of their time each week. Further, we have a somewhat unique situation with Acquia's [Office of the CTO (OCTO)](http://www.acquia.com/blog/next-steps-how-acquia-plans-give-back-drupal- 8-q4), which, with the involvement of [Drupal's project lead](http://buytaert.net), helps tremendously in pushing Drupal core development forward.

I would guess that much of this corporate support in code comes from the Drupal Association's excellent work in [securing financial contributions](https://association.drupal.org/advertising). However, because the Drupal Association [has no authority over the planning, functionality and development of the Drupal software](https://association.drupal.org/about), their involvement in this area is necessarily limited.

Major Giving
------------

Nowadays, nonprofits are increasingly involved in cultivating relationships with major donors. They are called "major gifts" because they have a major influence on an organization's ability to succeed (or not). For example, when I worked for the Minnesota Orchestra, I was shocked to find out that a single donor would pay for all of the costs to send the orchestra on a tour of Europe.

In Drupal, we have people like [Daniel Wehner](https://drupal.org/user/99340), [Tim Plunkett](https://drupal.org/user/241634), and [many others](http://ericduran.github.io/drupalcores) that devote significant time and code to Drupal core. We have [Alex Pott](https://drupal.org/user/157725), who will forever hold a place in Drupal lore as the guy who [quit his job to work full time on Drupal](http://buytaert.net/alex-pott).

In many nonprofits the role of the top executive is increasingly becoming that of chief fundraiser. Presidents, Executive Directors, and General Managers are just as likely, if not more likely, to be experts in finance or fundraising as they are experts in their organizational subject matter.

This is one of the areas where my analytical tool begins to break down. For Drupal, the equivalent would be for Dries to be spending his time flying around the world, taking top contributors out to lunch to talk about the future of the Drupal project. While that model proves successful for nonprofits, it hardly seems like a good use of Dries's time. Alex Pott, for example, is a visionary leader in his own right and does not need to be cultivated. Alex needs people to help him reach his [goal on Gitttip](https://www.gittip.com/alexpott/).

Overall, it seems like the Drupal community recognizes the contributions of its major givers and does its best to take care of them. When [yched's](https://drupal.org/user/39567) laptop started melting, the community [bought him a new one](https://www.drupalfund.us/project/funding-yched-complete-d8-entity-field-api). DrupalCons and numerous DrupalCamps, such as BADCamp and [Twin Cities Drupal Camp](http://tcdrupal.org) (TCDC), bring core contributors together in real life through scholarships and other support. (I like to brag that Views in Drupal Core was [born at TCDC](http://xjm.drupalgardens.com/blog/drupalcamp-twin-cities-views-drupal-core).) We are also starting to see some success with [Drupalfund](https://www.drupalfund.us/) and the [Drupal community on Gittip](https://www.gittip.com/for/drupal).

Parting Thoughts
----------------

While my goal here has been to re-imagine Drupal contributions rather than to solve some of the very real problems we face, I cannot help but wonder if some folks have already figured out the magic formula: patronage. This formula allows organizations to get what they need while supporting Drupal core development. While not perfect, the patronage system worked for the [Esterházy family](http://en.wikipedia.org/wiki/House_of_Esterh%C3%A1zy) and [Haydn](http://en.wikipedia.org/wiki/Joseph_Haydn), the [Medici's](http://en.wikipedia.org/wiki/House_of_Medici) and [Da Vinci](http://en.wikipedia.org/wiki/Davinci), and countless others. Nowadays, we have Stanford Univeristy and Tim Plunckett. Tim's employer requires him to work on Drupal sites, while also giving him a generous amount of time to work on Drupal core. His employer benefits, Drupal benefits, and Tim benefits.

Even if patronage is not the answer, then I think we will benefit by thinking about Drupal core like nonprofits approach fundraising. We need to grow contributors and do whatever it takes to keep folks coming back for more. We need to make it beneficial for businesses to support work on Drupal core and recognize those contributions. And we need to keep our major givers happy.

*P.S. If you found this article helpful, please consider looking at these two issues that [I'm responsible for](https://drupal.org/node/2187339) that will help get CMI to beta: <https://drupal.org/node/2108813> and <https://drupal.org/node/2148199>.*