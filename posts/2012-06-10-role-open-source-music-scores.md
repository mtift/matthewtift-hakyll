---
title: "On the Role of Open-Source Music Scores "
date: June 10, 2012, 8:32 pm
author: Matthew Tift
tags: open source music
path: role-open-source-music-scores
---

Few musicians or music scholars would doubt the value of a well-researched, historically-informed scholarly (a.k.a. "historical" or "critical") edition or music score, such as those produced by [G. Schirmer](http://www.schirmer.com/), [Bärenreiter](https://www.baerenreiter.com), [A-R Editions](https://www.areditions.com/), [G. Henle Verlag](http://www.henle.com/), and others. As a corollary, many people are rightfully suspicious of [crowdsourcing](http://www.wired.com/wired/archive/14.06/crowds.html?pg=1&topic=crowds&topic_set=) anything of value, from [encyclopedias](http://wikipedia.org) to [medical devices](http://www.economist.com/node/21556098) to [music scores](http://musescore.com/sheetmusic). I'm a vocal [proponent of open-source software and music](public-music-public-media-introduction), so here I am going to discuss how I see open-source scores fitting into the matrix of credentialed scores.

The "Urtext" model
------------------

The German word "Urtext" means "original text," so musicians and music scholars sometime use Urtext to denote music which represents the "original" version of a musical work. Publishers such as Bärenreiter and G. Henle Verlag are particularly fond of the word and use it in their marketing materials along words such as "[authoritative](http://www.henle.com/en/urtext-editions/what-is-urtext.html)," "[undistorted](http://www.henle.com/en/urtext-editions/what-is-urtext.html)," and "[authentic](https://www.baerenreiter.com/en/about-baerenreiter/baerenreiter-encyclopedia/urtext/)." While publishers such as these will admit that a modern score cannot be the single, valid representation of a musical work, they would probably like it if we associate their scores rather closely with the truth.

Of course, the notion of an Urtext is rather different in a discussion of 9th-century Gregorian chant not attributed to a particular person, as compared to, for instance, a discussion of the musical works of the 20th-century composer Béla Bartók, who could, at some point in his composition process, declare a musical work "complete and finished."\[1\] Nobody really knows what the score should be, but musicians certainly have a sense for what scores are trustworthy. Publishing companies that have been around for decades and employ a team of musicologists are rightfully among those we trust regarding music scores.

The networked model
-------------------

As David Weinberger discusses is his fascinating study, [*Too Big to Know*](http://www.toobigtoknow.com/), the nature of knowledge, in the age of the Internet, is changing. Weinberger makes a compelling case for the growing importance of "networked knowledge" -- as opposed to "traditional knowledge" -- in our lives. Open-source music scores would be an obvious example of "networked knowledge" in that they open up participation to those without traditional credentials or privileged positions, and create a space for more social interpretations of music scores. When a music score is open source, it is no longer a one-way conversation between a publisher and musician. It allows for us to become more active participants rather than mere consumers.

While a "networked" publisher such as [MuseScore](http://musescore.com/) may create and distribute their own version of an Urtext, they intentionally complicate things by applying established, open-source principles to their scores. Using open fomats, such as [MusicXML](http://en.wikipedia.org/wiki/MusicXML), MuseScore makes it trivial for another musician to download a score and make it better, trusting that users will eventually settle on an accurate and useable score. With open-source software, worthy changes have a tendency to become part of the versions that people use.

An open-source score creates a new space for musicologists to embrace the networked world. It also helps to make Bach's music more inclusive and less exclusive. Furthermore, these scores make me wonder if there need be a wall between musicology-endorsed scores and crowdsourced scores, and what exactly that wall would separate. Any obvious errors in open-source scores that credentialed musicologists would care to point out would likely be changed rather quickly by the people most interested in those scores.

Not necessarily a choice
------------------------

While it might be tempting to assume that open-source music competes with traditional music publishing companies, I don't think this is necessarily the case. I don't see any reason why they can't peacefully co-exist. Surely, the primary audience for an Urtext edition is the music historian or professional musician, and those individuals will not likely stop buying editions from businesses and companies they trust.

If open-source music does, in fact, create new audiences for centuries-old music scores, music lovers everywhere would benefit. There are plenty of writers that give away e-books while successfully selling the paper versions, and I think that general principle is applicable here. I suspect that the companies printing editions on high-quality paper *in addition to* giving away digital versions will come out ahead.

IMSLP
-----

For instance, the [International Music Score Library Project](http://imslp.org/) (IMSLP), which Edward W. Guo founded in 2006, makes digital music scores available free of charge to anyone with a computer and internet connection. It also offers low-cost, printed versions on demand. By employing the principles that made Wikipedia such a massive success, IMSLP has created a space for an immense collection of music scores to accumulate. And it's completely crowdsourced.

In what is perhaps the best-known musicological journal, the [*Journal of the American Musicological Society*](http://www.ams-net.org/pubs/jams.php), it has been noted that the legality of these scores is "questionable."\[2\] The IMSLP servers are physically located in Canada and subject to Canadian copyright law. In Canada a score is in the [public domain](http://en.wikipedia.org/wiki/Public_domain) if the composer died before 1962, and in the United States, anything published before 1923 is in the public domain. Thus, one can be rather certain that a [score of Bach's Goldberg Variations](http://imslp.org/wiki/Goldberg-Variationen,_BWV_988_(Bach,_Johann_Sebastian)) is in the public domain.

When asked about IMSLP, a representative from Universal Edition admitted, “[there’s room for both of us](http://www.nytimes.com/2011/02/22/arts/music/22music-imslp.html?pagewanted=all).” I agree, and I hope that open-source scores can continue to grow in popularity, and that they can extend the audiences and uses for the music that many of us love (and [even more reasons](open-source-music-10-more-reasons-why-it-fits)). Music scores are both a representation of sound and an aid to performance, so open-source scores that make music more available enable new interpretations or more performances. I welcome open-source music scores into our audacious, ever-expanding, open-source world.

NOTES
-----

1\. Kenneth Levy, "Charlemagne's Archetype of Gregorian Chant," *Journal of the American Musicological Society* 40 (1987): 1–30; László Somfai, "Manuscript versus Urtext: The Primary Sources of Bartók's Works," *Studia Musicologica Academiae Scientiarum Hungaricae* 23 (1981): 18.  
 2. Samuel N. Dorf, "Review," *Journal of the American Musicological Society*, 65 (2012): 280.
