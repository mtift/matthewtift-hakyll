---
title: "Why You Should Vote "
date: September 16, 2013, 3:23 pm
author: Matthew Tift
tags: Drupal, drupal planet
path: why-you-should-vote
---

I hope that you see a lot of reminders to vote this week. Of course, I am referring to the vote for the Drupal Association (DA) Board of Directors. I have already seen a number of blog posts and tweets that link to the [candidate profiles](https://association.drupal.org/election/3/candidates), the [first](https://association.drupal.org/node/18458) and [second](https://association.drupal.org/node/18508) "meet the candidate" sessions, and the URL where you [cast your vote](https://association.drupal.org/vote2014). Elections are open until midnight on Thursday, September 19. But you may be wondering, "Why should I care?" You may be thinking that those candidate profiles are quite *detailed* or that the "meet the candidate" sessions are especially *comprehensive*. I have first hand knowledge of this because I am [one of the candidates](https://association.drupal.org/election-candidate/68). So let me offer up a few reasons *why* you should vote.

Vote for your friends
---------------------

First off, I don't think there is any problem if you just want to vote for your friends or vote for people you know. That just seems like good karma. If you've been around the Drupal community for any time, you've probably met one or more of the candidates at a party, code sprint, IRC channel, issue queue, user group, camp, con, or elsewhere. So [show your support](https://association.drupal.org/vote2014). After all, [that's what friends are for](http://www.youtube.com/watch?v=xGbnua2kSa8).

Vote your conscience
--------------------

While it seems like there is quite a bit of agreement among the candidates, we each answered questions differently. For example, [Greg Lind](https://association.drupal.org/election-candidate/8) mentioned "expanding the adoption of Drupal for use in developing countries." [Ben Manning](https://association.drupal.org/election-candidate/63) wants to "highlight the use and development of Drupal in private education." [Pedja Grujic](https://association.drupal.org/election-candidate/53)<a> said he wants to "help new users get involved and learn Drupal." These goals all sound pretty admirable to me -- do any of them strike a chord with you? Then </a>[go vote](https://association.drupal.org/vote2014).

Vote for the mission
--------------------

Perhaps you are hard core into the DA and you know that its mission is to foster and support the Drupal software project, the community, and its growth. Perhaps you have read the [about page](https://association.drupal.org/about) on the DA website and you know how it works to realize its mission statement. So another reason to vote is that you want to choose candidates whose stated goals align most closely with the mission of the DA. For example, [Morten Birch Heide-Jørgensen](https://association.drupal.org/election-candidate/38) ([mortendk](https://association.drupal.org/user/662) to most of you) has done an enormous amount of work in the community organizing camps, cons, and other Drupal events, and he would like to help the DA become an even better resource for people organizing Drupal events. That is a fantastic goal and it aligns quite nicely with one the DA's key activities: "organizing and promoting worldwide events." [Josh Simmons](https://association.drupal.org/election-candidate/23) wants to "drive growth of the Drupal developer community" which matches up with this DA activity: "Empowering the Drupal community to participate in and contribute to the project." To be fair, most of the candidates have goals that mirror DA goals, so [check out the candidates](https://association.drupal.org/election/3/candidates) and [vote](https://association.drupal.org/vote2014).

Vote because you care
---------------------

This election is not about the number of candy bars in the cafeteria vending machine or the length of recess. The DA helps the community with funding, infrastructure, education, and more. The DA maintains the hardware and software infrastructure of drupal.org and runs DrupalCons. The Board of Directors of the DA are responsible for financial oversight and setting the strategic direction of the DA. Quite simply, if you are part of the Drupal community, you should vote. Read the [candidate profiles](https://association.drupal.org/election/3/candidates), find something that moves you, and [vote](https://association.drupal.org/vote2014).