---
title: "Drupal Development on OSX with MAMP and Solr "
date: October 6, 2012, 11:33 am
author: Matthew Tift
tags: Drupal, mamp, mysql, solr
path: drupal-development-osx-mamp-and-solr
---

These days I find myself splitting time between OSX and Linux, but when I'm on a Mac I like my development environment ready to go without having to do much clicking around. While I have tried a few times to get away from using MAMP, I find that it is just more convenient than configuring everything manually. However, I prefer to use the command line and have it run in the background without having to see it there in my dock and making me feel guilty for not configuring everything myself. To do this, I mostly followed the instructions from a old post on stringfoo.com and now I have a setup that I like.

First, make a script to start apache on startup:

`sudo vi /Library/LaunchDaemons/info.mamp.start.apache.plist`Then add this code to the file:

`<?xml version="1.0" encoding="UTF-8"??><plist version="1.0"><dict><key>Label</key><string>info.mamp.start.apache</string><key>ProgramArguments</key><array><string>/Applications/MAMP/Library/bin/apachectl</string><string>start</string></array><key>RunAtLoad</key><true></true></dict></plist>`Second, make a script to start mysql on startup:

`sudo vi /Library/LaunchDaemons/info.mamp.start.mysql.plist`Add this code to the file:

`<?xml version="1.0" encoding="UTF-8"??><plist version="1.0"><dict><key>Label</key><string>info.mamp.start.mysql</string><key>ProgramArguments</key><array><string>/Applications/MAMP/Library/bin/mysqld_safe</string><string>--port=3306</string><string>--socket=/Applications/MAMP/tmp/mysql/mysql.sock</string><string>--lower_case_table_names=0</string><string>--pid-file=/Applications/MAMP/tmp/mysql/mysql.pid</string><string>--log-error=/Applications/MAMP/logs/mysql_error_log</string></array><key>RunAtLoad</key><true></true><key>UserName</key><string>YOUR_USERNAME</string></dict></plist>`Be sure to replace `YOUR_USERNAME` with, well, your username.

Third, run a couple of commands to make sure that root is starting MAMP, and you should be good to go on startup:

`sudo chown root:wheel /Library/LaunchDaemons/info.mamp.start.apache.plistsudo chown root:wheel /Library/LaunchDaemons/info.mamp.start.mysql.plist`If I need to restart apache without using the MAMP UI, I type this command (actually, I have an alias in my .bashrc):

`sudo /Applications/MAMP/bin/apache2/bin/apachectl restart`Lastly, most of the sites I work on use solr for search, so I like to have that running without needing to open a separate window, type java -jar start.jar, and have it just sit there. It turns out that this is even easier to have start on startup. I have Solr sitting my my Applications folder, so I just went to System Preferences > Users & Groups > Login Items > hit the plus sign > and add /Applications/apache/example/start.jar to the startup items. Done.

This is mostly for my reference, so I realize that it is a bit short on details. If you've gotten this far, I hope it was helpful to you.