---
title: "From Broadcasting to 'Bot"
date: January 30, 2014, 10:42 am
author: Matthew Tift
image: officially-lullabot.jpg
tags: Lullabot, Wisconsin Public Radio
path: broadcasting-bot
---

***"You're always you, and that don't change, and you're always changing, and there's nothing you can do about it.”***

 ―Neil Gaiman, *The Graveyard Book*

- - - - - -

I am writing to announce that I have made the difficult decision to resign from Wisconsin Public Radio. My last day will be Friday, February 14. I have accepted a job offer to be a Senior Developer at [Lullabot](http://www.lullabot.com/).

When I started working at Wisconsin Public Radio nearly eight years ago, I had not yet finished my Ph.D. in musicology. I had been a stay-at-home dad and my first-born daughter was about 18 months old. I had decided that I needed to start supporting my family and to give my wife the opportunity to be a stay-at-home mom. It was one of the best decisions I have ever made.

WPR provided me with an awesome opportunity to grow professionally. During my first year, I quickly moved from traffic (radio data systems), to application development, to web development. WPR gave me the opportunity to geek out like never before. They sent me to week-long training courses in VB6, C#.NET, ColdFusion, and more. I learned PHP, SQL, ActionScript, Java, C, C++, Python, and, of course, [Drupal](https://drupal.org/). Over the past eight years, I have built countless custom applications for the wonderful people in WPR's membership, major giving, and underwriting departments.

In 2007, I launched WPR's online underwriting program. In addition to the original goal of setting up the technology for delivery, I created sell sheets (that looked like the work of a back-end developer), forged numerous partnerships, trained our salespeople and traffic department, and established multiple new revenue streams. Then I was invited to describe our online fundraising program at various conferences, [webinars](https://events.meetingbridge.com/Register/Default.aspx?06123161196), in a public media [toolkit for online fundraising](http://spiblog.pbs.org/2012/04/dei-toolkit-just-got-even-better.html), and on public media trade websites. While I am very proud of the success we have achieved in this area, what I really like to do is build websites.

During my time at WPR, the web team has grown from one person to seven people, and we converted most of our major web properties to Drupal, including [wpr.org](http://wpr.org), [wisconsinvote.org](http://wisconsinvote.org), [notmuch.com](http://notmuch.com), [wpra.org](http://wpar.org), and [ttbook.org](http://ttbook.org). We made [radio history](http://www.matthewtift.com/content/wpr-partners-open-goldberg) with [our broadcast](http://boingboing.net/2012/05/28/opengoldberg.html) of the [Open Goldberg Variations](http://www.opengoldbergvariations.org/score-following-wisconsin-public-radio-success). Additionally, over the past few years I have done a fair amount of freelance Drupal development and consulting for a variety of organizations, such as Harvest Public Media, Gorton Studios, TEN7, Public Radio International, Nevada Public Radio, and others.

When I built my first Drupal site, I followed along with the book [Using Drupal](http://amzn.com/0596515804), which was written by Lullabots. I first learned how to build Drupal modules watching the [Lullabot Learning Series DVDs](https://www.lullabot.com/news/20080228/lullabot-announces-new-dvd-training-series).

Over the past few years, I have had the pleasure of getting to know various Lullabots (at least 15 people, by my count) at numerous DrupalCons, DrupalCamps, code sprints, organizing the [Twin Cities Drupal Camp](http://tcdrupal.org), and working on the [Drupal 8 Configuration Management Initiative](http://drupal8cmi.org/).

When someone from Lullabot asked me to apply more than a year ago, I was definitely not looking for a job. But today I am ready for a new adventure. I have greatly enjoyed my time working at WPR and with my co-workers. I am surely going to miss a lot of people at WPR, especially people in the fundraising department, web team, and my boss, Mary Kay Dadisman, who has been an exceptional mentor and advocate.

Since Lullabot is a [fully distributed company](https://www.lullabot.com/blog/article/how-working-home-works-us), my day-to-day life will change only slightly. I was thrilled five years ago when WPR was willing to let me start working remotely, and now I'm perhaps even more excited about working at a company where all of the employees are remote. I will continue working in the same home office. My wife and dog will still be the co-workers that I see in the hallway. I'll continue to build websites using the same, or very similar, tools. Instead of working on public radio websites, now I will get to work on an [exceptionally talented team](https://www.lullabot.com/who-we-are) that has a long history of building some [awesome, high profile websites](http://www.lullabot.com/work).

Although I will miss my dedicated colleagues at Wisconsin Public Radio, and I am a bit nervous about leaving a stable job that I still greatly enjoy, I am excited to start this new phase of my career.
