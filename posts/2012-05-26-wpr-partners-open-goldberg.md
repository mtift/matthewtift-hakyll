---
title: "WPR partners with Open Goldberg"
date: May 26, 2012, 10:59 pm
author: Matthew Tift
tags: Wisconsin Public Radio, public radio
path: wpr-partners-open-goldberg
---

I met [Robert Douglass](http://www.robshouse.net/) in person for the first time at [DrupalCon Denver](http://denver2012.drupal.org/) this year. We immediately hit it off due to our shared interests and background in both music and Drupal.

When Robert told me about [Open Goldberg Variations](http://www.opengoldbergvariations.org), a project to create a public domain score and studio recording of Bach's Goldberg Variations, and said he was looking for a public radio station to broadcast the piece, I told him I would do everything I could to help make his dream a reality.

So I am very proud to say that on June 24, for the first time in history, radio listeners will be able to follow the digital score of a broadcast recording as it is being played on air. The open source score, and recording by award-winning pianist Kimiko Ishizaka, will be available on May 28.

Learn more about this exciting event at [opengoldberg.wpr.org](http://wpr.org).
