---
title: "OpenVPN on Ubuntu 18.04"
date: May 30, 2018, 10:40 am
author: Matthew Tift
tags: VPN
path: openvpn-ubuntu-1804
---

I needed to install Ubuntu 18.04 recently and I ran into some problems with [OpenVPN 2.4.4](https://packages.ubuntu.com/bionic/openvpn) not working (it had worked fine with [OpenVPN 2.4.0](https://packages.debian.org/stretch/openvpn) from Debian stretch/stable). The errors pointed to `ns-cert-type` being the problem, which [is indeed a deprecated option](https://community.openvpn.net/openvpn/wiki/DeprecatedOptions). But that led me to find suggestions to instead use `remote-cert-tls server` that did not help me get things working.

<!--more-->

Rather, the problem ended up being with these options that I'm sometimes required to add to get openvpn working:

```
up /etc/openvpn/update-resolv-conf
down /etc/openvpn/update-resolv-conf
```

Following the advice from [this answer](https://askubuntu.com/questions/1032476/ubuntu-18-04-no-dns-resolution-when-connected-to-openvpn/1036209#1036209) in the Ubuntu forums, I had to make just a few tweaks to get things working.

#### First, install openvpn-systemd-resolved:

```
sudo apt install openvpn-systemd-resolved
```

#### Second, add these four lines (yes, with `script-security 2`) at the end of client.ovpn:

```
script-security 2
up /etc/openvpn/update-systemd-resolved
down /etc/openvpn/update-systemd-resolved
down-pre
```

And now OpenVPN works again:

```
sudo openvpn --config /path/to/client.ovpn --auth-user-pass /path/to/my.auth
```
