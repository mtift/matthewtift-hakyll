## EDUCATION

2007 &emsp; Ph.D., Musicology, University of Wisconsin-Madison<br />
&emsp; &emsp; &emsp; Dissertation: *[Musical AIDS: Music, Musicians, and the Cultural Construction of HIV/AIDS](/files/tift-dissertation.pdf)*

2001 &emsp; M.A., Musicology, University of Wisconsin-Madison

1998 &emsp; B.A., Music, *magna cum laude*, Bemidji State University (Minnesota)

<br /> 

## EMPLOYMENT

2014-present, Senior Web Developer, Lullabot (Minneapolis)

2009, Visiting Assistant Professor of Musicology, University of Iowa (Iowa City)

2006-2014, Web Developer, Wisconsin Public Radio (Madison and Minneapolis)

1999-2006, Sea Grant Project Assistant, University of Wisconsin-Madison

1999, Office Manager, Madeline Island Music Camp (Minneapolis)

1998, Artistic Planning Intern, Minnesota Orchestra (Minneapolis)

1998, Substitute Music Instructor, Aspen Country Day School (Aspen, Colorado)

1997-1998, Teaching Associate, History and Literature of Music, Bemidji State University (Minnesota)

<br /> 

## PUBLICATIONS

#### Edited Volumes

2007 &emsp; "[Grateful Dead Musicking](https://www.worldcat.org/title/all-graceful-instruments-the-contexts-of-the-grateful-dead-phenomenon/oclc/123903223)" in *All Graceful Instruments: The Contexts of the Grateful Dead Phenomenon*, edited by Nicholas Meriwether (Cambridge Scholars Press).

<br />

#### Conference Proceedings

2019 &emsp; "[Meditative Live Coding and Musicological Hindrances](http://iclc.livecodenetwork.org/2019/papers/paper12.pdf)," International Conference on Live Coding (Madrid, Spain), January 18

<br />

## CONFERENCE ACTIVITY

#### Papers Presented

2019 &emsp; "[The Imaginary Band of Drupal Rock Stars](https://events.drupal.org/seattle2019/sessions/imaginary-band-drupal-rock-stars)," DrupalCon Seattle, April 11

2018 &emsp; "[Drupal and the Music Industry](https://2018.tcdrupal.org/session/drupal-and-music-industry-learning-our-success)," Twin Cities Drupal Camp, June 8

2017 &emsp; "[Drupal in the Public Sphere](http://2017.tcdrupal.org/session/drupal-public-sphere)," Twin Cities Drupal Camp, June 23

2017 &emsp; "[Drupal in the Public Sphere](https://events.drupal.org/baltimore2017/sessions/drupal-public-sphere)," DrupalCon Baltimore, April 27

2016 &emsp; "[AMPing up Drupal](https://events.drupal.org/neworleans2016/sessions/amping-drupal)" (with Karen Stevenson and Marc Drummond), DrupalCon New Orleans, May 11

2016 &emsp; "[Configuration Management for Developers in Drupal 8](https://events.drupal.org/neworleans2016/sessions/configuration-management-developers-drupal-8)" (the most attended session at DrupalCon), DrupalCon New Orleans, May 10

2015 &emsp; "[Configuration Management for Developers in Drupal 8](https://2015.badcamp.net/event/summit/configuration-management-and-devops-summit)" (with Alex Pott), BADCamp Configuration Management and DevOps Summit, October 23

2015 &emsp; "[Introducing Drupal 8](/talks/osn)," Open Source North, October 3

2015 &emsp; "[The Cultural Construction of Drupal](http://2015.nyccamp.org/session/teaching-kids-code)," NYC Camp, July 18

2015 &emsp; "[Teaching Kids to Code](http://2015.tcdrupal.org/session/teaching-kids-code)," Twin Cities Drupal Camp, June 27

2015 &emsp; **Keynote**, "[Teaching Kids to Code](https://2015.drupalcampcolorado.org/session/teaching-kids-code-next-gen-drupalers)," DrupalCamp Colorado, June 21

2014 &emsp; "[Introducing the Drupal 8 Configuration System](http://2014.tcdrupal.org/node/238)," Twin Cities Drupal Camp, August 8

2014 &emsp; "[Introducing the Drupal 8 Configuration System](https://austin2014.drupal.org/session/introducing-drupal-8-configuration-system.html)," DrupalCon Austin, June 3

2014 &emsp; "[Introducing the Drupal 8 Configuration System](https://groups.drupal.org/node/416043)," Twin Cities Drupal User Group, April 23

2013 &emsp; "[Introducing the Drupal 8 Configuration System](http://2013.badcamp.net/sessions/introducing-drupal-8-configuration-system)," BADCamp, October 26


2013 &emsp; **Keynote**, "[Drupal 8: What You Need To Know](http://mtift.github.io/drupalcamp-wi-2013-keynote)," DrupalCamp Wisconsin, July 26

2013 &emsp; [Drupal 8: What You Need To Know](http://2013.tcdrupal.org/session/drupal-8-what-you-need-know)," Twin Cities Drupal Camp, "June 19

2013 &emsp; "[WordPress, Drupal, and APIs, Oh My!](https://web.archive.org/web/20130626040423/http://www.integratedmedia.org:80/conference/schedule#APIs)" Integrated Media Association, March 7

2012 &emsp; "[Drupal in Public Media](https://groups.drupal.org/node/197058)," Twin Cities Drupal User Group, February 22

2005 &emsp; "[Positive Music: Women, Music, and AIDS](/files/2005-04-16-womens-studies-interactions-positive-music.pdf)," [Women's and Gender Studies Conference](https://consortium.gws.wisc.edu/conference/) -- [InterACTIONS: Activism, Research, Scholarship, Teaching](/files/womens-studies-interactions-program-2005.pdf) (Madison, Wisconsin), April 16

2005 &emsp; "['Don't Go Chasing Waterfalls': Music, Black Women, and HIV/AIDS](/files/2005-02-19-society-for-american-music-music-black-women-aids.pdf)," [Society for American Music](/files/society-american-music-program-2005.pdf) (Eugene, Oregon), February 19

2004 &emsp; "[Musical AIDS: On the Popularity of American Musical Responses to HIV/AIDS in the 1990s](/files/2004-02-28-mgmc-musical-aids.pdf)," Midwest Graduate Music Consortium (Chicago, Illinois), February 28

2002 &emsp; "[Excuse Me, Where Can I Find Mozart's Clarinet Concerto?](/files/2002-02-23-excuse-me-where-can-i-find-mozarts-clarinet-concerto.pdf)" Midwest Graduate Music Consortium (Madison, Wisconsin), February 23

2001 &emsp; "[How Do the Grateful Dead and Deadheads 'Mean'?](/files/2001-03-09-southwest-texas-popular-culture-association-how-do-the-grateful-dead-and-deadheads-mean.pdf)" Southwest/Texas Popular Culture Association (Albuquerque, New Mexico), March 9

2000 &emsp; "[Grateful Dead Musicking](/files/2000-11-06-sam-grateful-dead-musicking.pdf)," Society for American Music, "Toronto 2000: Musical Intersections," November 6

2000 &emsp; "[Playing in the Band: The Grateful Dead and Musicking](/files/2000-02-11-southwest-texas-popular-culture-association-playing-in-the-band-the-grateful-dead-and-musicking.pdf)," Southwest/Texas Popular Culture Association (Albuquerque, New Mexico), February 11

<br />

## INTERVIEWS

2018 &emsp; "[Episode 36](https://ten7.com/blog/post/episode-036-matthew-tift)," TEN7 podcast, August 8

2017 &emsp; "[Drupalcon Baltimore Edition](https://www.lullabot.com/podcasts/drupalizeme-podcast/drupalcon-baltimore-edition)," Lullabot Podcast, April 20

2016 &emsp; "[Past, Present, and Future of Drupal with Dries Buytaert and Matthew Tift](https://www.lullabot.com/podcasts/drupalizeme-podcast/past-present-and-future-of-drupal)," Lullabot Podcast, November 24

2016 &emsp; "[Selling Drupal Modules and Distributions](https://www.lullabot.com/podcasts/drupalizeme-podcast/selling-drupal-modules-and-distributions)," Lullabot Podcast, April 7

2016 &emsp; "[Drupal AMP](http://www.talkingdrupal.com/115)," Talking Drupal Podcast, March 9

2014 &emsp; "[Coding in Schools](https://www.lullabot.com/blog/podcasts/drupalizeme-podcast/53-coding-schools)," interviewed along with Eric Schneider (Assistant Superintendent for Instruction, Minnetonka Public Schools), Drupalize.Me Podcast, December 12

2012 &emsp; "[What `<audio`> Means for Public Radio](http://www.current.org/2012/05/ncme-and-ima-sponsoring-public-media-innovators-audio-webinar-on-Wednesday/)," Public Media Innovators Webinar, May 30

2008 &emsp; KBOO (Portland, Oregon), interviewed by Andrew Geller about "Grateful Dead Musicking" chapter in *All Graceful Instruments*,  November 8

2004 &emsp; Interviewed on the "Morning Show" on WKOW (Madison) about [Positive Music: Musical Responses to HIV/AIDS](/positive-music-musical-responses-hivaids) benefit concert, December 3

<br />

## SERVICE

2016-present, Host, [Hacking Culture: Free Software and the Art of Hacking](https://www.hackingculture.org/) (podcast)

2019, Program Committee Member, [International Conference on Live Coding](http://iclc.livecodenetwork.org/2019/) (Madrid, Spain)

2011-2018, Organizer, [Twin Cities Drupal Camp](http://tcdrupal.org) (Minneapolis, Minnesota)

2014-2016, Instructor, Minnewashta Elementary Coding Club (Shorewood, Minnesota)

2014, Mentor, Tonka Coder Dojo (Minnetonka, Minnesota)

2014, Member, [Tonka `<Codes>` Design Team](https://www.minnetonkaschools.org/academics/specialty-programs/tonka-codes) (Minnetonka School District, Minnesota)

2013, Instructor, [Drupal Developers Unite! Inaugural Pub Media Drupal Developer's Clinic](https://web.archive.org/web/20120603023545/http://www.integratedmedia.org:80/conference/schedule#Drupal), Integrated Media Association Conference (Austin, Texas), March 6

2013, Invited Speaker, “How ttbook.org Uses Drupal,” PRI: Public Radio International, February 27

2004-2006, Member, State of Wisconsin HIV Prevention Community Planning Council

2005, Invited Speaker, "Music, Musicians, and AIDS," State of Wisconsin AIDS/HIV Program (Madison)

2004, Producer, [Positive Music: an AIDS Benefit Concert](/positive-music-musical-responses-hivaids) (Madison)

2002, Founder & Coordinator, Wisconsin AIDS Ride (formerly AIDS Network Cycles Together)
