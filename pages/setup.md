---
title: 'Current Setup'
---

* Laptop: Libreboot X200
* Laptop OS: Debian stable
* Text editor: vim
* Email: mutt
* IRC: HexChat
* Phone: Moto X4
* Live Coding: Tidal


## Changelog
* May 2018: Started live coding with Tidal
* Apr 2018: Purchased Moxo X4
* Jun 2017: Started using mutt for email
* Mar 2016: Purchased ThinkPad T450 at MicroCenter
* Sep 2015: Switched phone to Nexus 6 (it's too big)
* Jun 2015: Switched from XChat to HexChat for IRC
* Jan 2015: Purchased Libreboot X200 at LibrePlanet
* Feb 2014: Started using Debian stable for my main desktop
* Feb 2014: Purchased Nexus 5 (it's the perfect size)
* Sept 2010: Vim became my text editor (sometime around here)
