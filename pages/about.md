Dr. Matthew Tift is a senior developer at [Lullabot](https://www.lullabot.com/about/matthew-tift) and host of [Hacking Culture](https://www.hackingculture.org), a show about free software and the art of hacking.

An active member of the [Drupal community](https://drupal.org/user/751908), Matthew is a [Drupal 8 configuration system](https://cgit.drupalcode.org/drupal/tree/core/MAINTAINERS.txt#n124) co-maintainer, organizer of the [Twin Cities DrupalCamp](http://tcdrupal.org) since 2011, Drupal core contributor since 2010, and regular speaker at various conferences and user groups. He teaches coding with free software at various clubs and organizations to K-12 students in his local community.

Matthew has spent most of his life hacking and musicking. He started playing violin at age 5 and learned to code (in BASIC) at age 8. For his undergraduate senior recital in 1998, Matthew performed Steve Reich's [violin phase](https://en.wikipedia.org/wiki/Violin_Phase) with his violin and computer. Matthew first started building websites professionally in 1999 and earned a Ph.D. in musicology from the University of Wisconsin-Madison in 2007. He has taught at the University of Iowa and Bemidji State University.

In 2012, while working at Wisconsin Public Radio, he helped organize the [first radio broadcast in history](/opengoldberg/) where radio listeners could follow the digital score of a musical work online as it was broadcast over the airwaves.

Matthew lives near Minneapolis, Minnesota, where he enjoys cycling, running, walking, meditation, and playing string quartets with his wife (viola) and two daughters (cello and violin).
