### Permission denied errors on settings.php
```
chmod ug+w sites/default
```

### Change files in a directory
```
rename 's/ /-/g' * && rename 'y/A-Z/a-z/' *
```
(replace spaces with hyphens and upper case with lower case)

### Add a .doc file extension to all files in a directory
```
rename 's/(.*)/$1.doc/' *
```

### Fix quotation marks
```
sed -i 's/“/"/g' file && sed -i 's/”/"/g' file && sed -i "s/’/'/g" file && sed -i "s/‘/'/g" file
```

### Upload ssh keys to a server
```
ssh-copy-id -i ~/.ssh/id_rsa.pub user@server.org
```

### Resize an image
```
convert logo.png -resize 300 logo.png
```

### Get image info
```
file logo.png
```

### Creat an ico from a image file
```
icotool -o favicon.ico -c input.png
```

### Drupal core workflow
```
wget http://drupal.org/issue.patch
git apply --index issue.patch
[hack]
git diff > interdiff.txt
git diff HEAD > new.patch
```

### Log arrays with logger in Drupal 8
```
\Drupal::logger('Matthew')->notice('<pre><code>' . print_r($variable, TRUE) . '</code></pre>');
```

### Update default browser in Debian
```
sudo update-alternatives --config x-www-browser
```

### Convert Unix timestamp on the command line
```
date -d @$TIMESTAMP
```

### Find and edit
```
grep -rl 'hook_expire_cache_alter' . | xargs gvim -p
```

### Find and replace
```
grep -rl 'OLD_TEXT' . | xargs sed -i 's/OLD_TEXT/NEW_TEXT/g'
```

### Pass array as command line argument
```
php -r "print json_encode(array('node/*/edit' =&gt;
'128M', 'node/add/type' =&gt; '128M'));" |
drush vset --format=json page_memory_limit -
```

### Count words in vim
```
g > CTRL-g
```

### Delete blank lines in vim
```
:g/^$/d
```

### Debug JS in PHP code
```
echo "&lt;script type='text/javascript'&gt; alert(" . print_r($var) . "); &lt;/script&gt;";
```

### Download high quality audio from YouTube (best quality is 0)
```
youtube-dl --extract-audio --audio-format wav --audio-quality 0 URL
```

### Get "Date Taken" times of JPGs in a directory
```
exiv2 *.JPG | grep "Image timestamp"
```

### Change "Date Taken" times of JPGs in a directory
```
exiftool "-AllDates=2002:01:01 04:00:00" *.JPG
```

### Vim special characters ([didigraphs](http://vimdoc.sourceforge.net/htmldoc/digraph.html#digraph-table))
```
CTRL-k a - (ā)
CTRL-k a ! (à)
CTRL-k n . (ṅ)
CTRL-k e ' (é)
CTRL-k u - (ū)
CTRL-k u : (ü)
```

### Paste into the Vim command line
```
CTRL-r +
```

### Find Files 100-999M
```
du -h | grep '[0-9]\{3\}M'
```

### Make PDFs searchable
```
ocrmypdf file.pdf file.pdf
```

### Get Size of All MySQL Databases
```
SELECT table_schema "DB Name",
Round(Sum(data_length + index_length) / 1024 / 1024, 1) "DB Size in MB"
FROM information_schema.tables
GROUP BY table_schema;
```
