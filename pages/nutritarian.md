* allspice
* almond butter
* almond milk
* amaranth
* anise
* apples
* apricots
* artichoke hearts
* artichokes
* arugula!
* asparagus
* bamboo shoots
* banana
* bananas
* barley
* basil
* bay leaf
* bean sprouts
* beet greens
* beets!
* berries
* bibb lettuce!
* black beans!
* black pepper
* black-eyed peas
* blackberries!
* blood oranges
* blueberries!
* bok choy!
* boston lettuce!
* broccoli rabe
* broccoli!
* brown rice
* brussels sprouts
* buckwheat
* cabbage!
* cajun seasoning
* cannellini beans!
* cantaloupe
* carageenan
* caraway seeds
* cardamom
* carrots!
* cauliflower!
* cayenne pepper
* celery
* celery root (celeriac)
* celery seeds
* cherimoyas
* cherries
* cherry tomatoes
* chervil
* chia seeds!
* chickpeas!
* chicory
* chili pepper
* chili powder
* chives!
* cilantro
* cinnamon
* clementine
* clementines
* cloves
* coffee
* collard greens
* collards!
* corn
* cowpeas
* cranberries
* cucumbers!
* cumin
* curly endive
* curry powder
* daikon radish
* dandelion greens
* dates!
* dill
* dragon fruit
* edamame
* eggplant
* endive
* escarole
* farro
* fennel
* fennel bulb
* firm tofu
* flaxseeds!
* fresh figs
* frisée
* garam masala
* garlic cloves
* garlic powder
* garlic!
* ginger
* goji berries
* grapefruit
* grapes
* great northern beans
* green beans
* green bell peppers
* green onions!
* green peas
* hemp seeds!
* herbes de Provence
* honeydew melon
* horseradish
* iceberg lettuce (low nutrient density)
* jackfruit
* jicama
* kale!
* kidney beans
* kiwi
* kohlrabi
* kumquats
* leaf lettuce!
* leeks!
* lemongrass
* lemons
* lima beans
* lime juice
* limes
* lntils
* mangoes
* marjoram
* melons
* mesclun
* millet
* mint
* miso
* mung bean sprouts
* mushrooms!
* mustard
* mustard greens!
* mâche!
* nectarines
* nutmeg
* oak leaf lettuce
* oatmeal
* oats
* okra
* olives
* onion powder
* onions!
* oranges
* oregano
* papayas
* paprika
* parsley
* parsnips
* peaches
* peanut butter
* pears
* peas
* pectin
* pepper
* pepper flakes
* peppers (all types including bell peppers and hot peppers)
* persimmons
* pickle
* pigeon peas
* pine nuts
* pineapple
* pineapples
* pinto beans!
* plums
* pomegranates
* popcorn
* porridge
* potato
* prickly pear fruit
* pumpkin
* pumpkin seeds
* quinoa
* radicchio
* radishes!
* rambutan
* raspberries!
* raw carrots
* red bell pepper!
* red cabbage!
* red kidney beans!
* red leaf lettuce!
* red wine
* rhubarb
* rice
* romaine lettuce!
* rosemary
* rutabaga
* rutabaga leaf
* saffron
* sage
* savory
* scallions!
* sesame seeds
* shallots!
* snow peas!
* soybeans
* spinach!
* split peas!
* starfruit/carambola
* steel cut oats
* strawberries!
* string beans
* sugar snap peas
* summer squash
* sunflower seeds
* sweet potato
* swiss chard
* tangerines
* tarragon
* tempeh
* thyme
* tofu
* tomatoes!
* turmeric
* turnip greens
* walnuts!
* water chestnuts
* watercress!
* watermelons
* wheat berries
* white beans
* wild rice
* winter squash
* zucchini!

! = Eat lots!
